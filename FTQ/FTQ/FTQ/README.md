How to get tests locally:
* install JAVA v1.8
* install Maven
* install Git
* install IntelliJ IDEA https://www.jetbrains.com/idea/download/
* in browser log in to Bitbucket and goto url https://bitbucket.org/ftq360/ftq_automation/src/master/
* click Clone button and copy project url
* open terminal and navigate to folder where you want to place project codebase 
* in terminal paste clone command like ```git clone https://dkovtn@bitbucket.org/ftq360/ftq_automation.git``` and hit Enter


How to run test suite and get results locally:
* start IntelliJ IDEA 
* click Open
* select recently downloaded project
* open project
* to run tests open terminal into IntelliJ IDEA and run following command in terminal ```mvn test```
* test result could be found on ```target/cucumber-html-report/index.html```


How to run test suite on Bamboo:
* log in to Bamboo
* click Cucumber Automation plan
* select a branch in dropdown click Run plan dropdown menu item

How to get test results on Bamboo:
* log in to Bamboo
* click Cucumber Automation plan
* click needed build number in the list
* click Artifacts tab
* click Cucumber Reports > FTQ > target >  cucumber-html-report