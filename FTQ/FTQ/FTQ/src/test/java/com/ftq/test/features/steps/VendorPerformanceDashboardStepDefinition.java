package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.VendorPerformanceDashboardPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class VendorPerformanceDashboardStepDefinition {

    @Then("^I 'should' see '/Dashboards/VendorPerformanceDashboard' page$")
    public void iShouldSeeChecklistsVendorPerformanceDashboardPage() {
        VendorPerformanceDashboardPage vendorPerformanceDashboardPage = page(VendorPerformanceDashboardPage.class);

        vendorPerformanceDashboardPage.getTitle().shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("% FTQ Trend").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("% FTQ Checkpoints By Category").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("% FTQ Checkpoints By Responsible Party").shouldBe(Condition.visible);

        vendorPerformanceDashboardPage.getChartTitle("# Deficiencies Trend").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("# Deficiencies By Responsible Party").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("Avg. Days To R4R By Responsible Party").shouldBe(Condition.visible);

        vendorPerformanceDashboardPage.getChartTitle("Recurring Deficiencies By Checkpoint").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("Total R Exposure By Responsible Party").shouldBe(Condition.visible);
        vendorPerformanceDashboardPage.getChartTitle("Stats").shouldBe(Condition.visible);
    }
}
