package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.User;
import com.ftq.test.pages.LoginPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import sun.rmi.runtime.Log;

import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.getUniqueValue;

public class LoginStepDefinitions {

    private String username, password;

    @Given("^login with following credentials:$")
    public void loginWithFollowingCredentials(List<User> users) {
        LoginPage loginPage = page(LoginPage.class);

        for (User user : users) {
            username  = user.getUsername();
            password = user.getPassword();
        }

        loginPage.submitForm(getUniqueValue(username), password);
    }

    @Given("^I login as admin user$")
    public void iLoginAsAdminUser() {
        LoginPage loginPage = page(LoginPage.class);
        loginPage.submitForm(
                Common.getConfigValue("username"),
                Common.getConfigValue("password"));
    }

    @Given("^I login as admin user using url '(.*)'$")
    public void iLoginAsAdminUserUsingUrl(String url) {
        open(url);
        LoginPage loginPage = page(LoginPage.class);
        loginPage.submitForm(
                Common.getConfigValue("username"),
                Common.getConfigValue("password"));
    }


    @Given("^I login as restricted user$")
    public void iLoginAsRestrictedUser() {
        LoginPage loginPage = page(LoginPage.class);
        loginPage.submitForm(
          Common.getConfigValue("restrictedUsername"),
          Common.getConfigValue("restrictedPassword")
        );
    }

    @Given("^I login as performance user$")
    public void iLoginAsPerformanceUser() {
        Common.printCurrentTime();
        LoginPage loginPage = page(LoginPage.class);
        loginPage.submitForm(
                Common.getConfigValue("performanceUsername"),
                Common.getConfigValue("password")
        );
    }

    @Then("^I should see login error on login page$")
    public void iShouldSeeLoginErrorOnLoginPage() {
        LoginPage loginPage = page(LoginPage.class);
        loginPage.getLoginError().shouldBe(Condition.visible);
    }

    @Then("^I should see 'Forgot password' link under Login form$")
    public void iShouldSeeForgotPasswordLinkUnderLoginForm(){
        LoginPage loginPage = page(LoginPage.class);
        loginPage.getForgotPasswordLink().shouldBe(Condition.visible);
    }

    @When("^I click on 'Forgot password' link under Login form$")
    public void iClickOnForgotPasswordLinkUnderLoginForm(){
        LoginPage loginPage = page(LoginPage.class);
        loginPage.getForgotPasswordLink().click();
    }

}
