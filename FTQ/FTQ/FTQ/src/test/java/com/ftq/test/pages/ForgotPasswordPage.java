package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ForgotPasswordPage {

    private SelenideElement getForgotPasswordModalTextLocator(String text) { return $(By.xpath(String.format("//*[@id='login-form']/*[contains(@action, 'ForgotPassword')]/p[contains(text(), \"%s\")]", text))); }
    private SelenideElement getUserNameFieldLocator() { return $("#username"); }
    private SelenideElement getResetBtnLocator() { return $(".submit [value='Reset']"); }
    private SelenideElement getConfirmationSendingResetPasswordTextLocator(String text) { return $(By.xpath(String.format("//*[@id='login-form']/p[contains(text(), \"%s\")]", text))); }

    public SelenideElement getForgotPasswordModalText(String text) {
        return getForgotPasswordModalTextLocator(text);
    }

    public SelenideElement getUserNameField() {
        return getUserNameFieldLocator();
    }

    public SelenideElement getResetBtnL() {
        return getResetBtnLocator();
    }

    public SelenideElement getConfirmationSendingResetPasswordText(String text) {
        return getConfirmationSendingResetPasswordTextLocator(text);
    }
}
