package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class CommunitySetupPage {

    //Elements on the page

    private SelenideElement getAddButtonLocator() { return $(".th-plus-icon"); }
    private SelenideElement getSaveButtonLocator() { return $("[data-bind*='save']"); }
    private SelenideElement getNewItemLocator() { return $("tbody tr[data-bind*=isFreshFlag]"); }
    private SelenideElement getNewProjectCodeFieldLocator() { return $(".has-changes [data-bind*='CommunityCode']"); }
    private SelenideElement getNewProjectDescriptionFieldLocator() { return $(".has-changes [data-bind*='CommunityLabel']"); }
    private SelenideElement getNewProjectEmailFieldLocator() { return $(".has-changes [data-bind*='Email']"); }
    private SelenideElement getProjectDescriptionFieldLocator() { return $("[data-bind*='CommunityLabel']"); }
    private SelenideElement getProjectEmailFieldLocator() { return $("[data-bind*='Email']"); }
    private SelenideElement getSearchedProjectSelectLocator() { return $("[name='rowselect']"); }
    private SelenideElement getNewPhaseCodeFieldLocator() { return $(".has-changes [data-bind*=JobsCode]"); }
    private SelenideElement getNewPhaseNameFieldLocator() { return $(".has-changes [data-bind*=LotNumber]"); }
    private SelenideElement getNewLocationCodeFieldLocator() { return $(".has-changes [data-bind*='parent.Code']"); }
    private SelenideElement getNewLocationNameFieldLocator() { return $(".has-changes [data-bind*='parent.Description']"); }
    private SelenideElement getNewEquipmentCodeFieldLocator() { return $(".has-changes [data-bind*='parent.Code']"); }
    private SelenideElement getNewEquipmentNameFieldLocator() { return $(".has-changes [data-bind*='parent.Description']"); }
    private SelenideElement getPhaseNameFieldLocator() { return $("[data-bind*='LotNumber']"); }
    private SelenideElement getSelectAllCheckboxLocator() { return $("[for=Selected]"); }
    private SelenideElement getSelectAllConfirmOkLocator() { return $(".modal.active > .actions > div.ok.button"); }
    private SelenideElement getFilterCheckboxLocator(String selectedCheckbox) { return $(By.xpath(String.format("//table[@class='primary-table']/tbody/tr[%s]//label", selectedCheckbox))); }
    private SelenideElement getBottomMenuButtonLocator(String buttonName) { return $(By.xpath(String.format("//span[.='%s']", buttonName))); }
    private SelenideElement getInspectorChecklistsLocator(String selectedChecklist) { return $(By.xpath(String.format("//li[%s]//label", selectedChecklist))); }
    private SelenideElement getDeleteButtonLocator() { return $(".delete-icon"); }
    private SelenideElement getDeleteModalButtonLocator(String buttonName) { return $(By.xpath((String.format("//div[text()='%s']", buttonName)))); }
    private SelenideElement getMarkedToDeleteItemLocator() { return $(".marked-for-delete"); }
    private SelenideElement getNewPhaseNameLocator() { return $(By.xpath("//*[contains(@data-bind,'LotNumber')]")); }
    private SelenideElement getCancelButtonLocator() { return $("[data-bind*='discardChanges']"); }
    private SelenideElement getNextButtonLocator() { return $("[data-bind*='goNext']"); }
    private SelenideElement getAttachColumnLocator() { return $(By.xpath("//th[text()='Attach']")); }
    private SelenideElement getChecklistDropDownMenuLocator() { return $("select#selJ1J"); }
    private SelenideElement getPlanItemCodeLocator() { return $(".has-changes [data-bind*=PlanItemCode]"); }
    private SelenideElement getItpChecklistsDropdownLocator() { return $(By.xpath("//span[text()='Select Checklist']")); }
    private SelenideElement getItpChecklistDropdownInputLocator() { return $(By.xpath("//span[text()='Select Checklist']/parent::a//following-sibling::div//input")); }
    private SelenideElement getItpChecklistDropdownMenuLocator(String checklist) { return $(By.xpath(String.format("//*[@class='active-result'][contains(text(), '%s')]", checklist))); }

    private SelenideElement getCreatePermissionLocator(){return $(By.xpath("//table[@class='primary-table']/tbody/tr//label[contains(@for, '.Create')]"));}

    //Actions on the page

    public SelenideElement getNewPhaseName() {
        return getNewPhaseNameLocator();
    }

    public SelenideElement getAddButton() {
        return getAddButtonLocator();
    }

    public CommunitySetupPage clickUnselectAllButton() {
        getBottomMenuButtonLocator("Unselect All").click();
        while(!getBottomMenuButtonLocator("Select All").isDisplayed()) {
            Common.pause(100);
        }
        return this;
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public SelenideElement getNewProjectCodeField() {
        return getNewProjectCodeFieldLocator();
    }

    public SelenideElement getNewProjectDescriptionField() {
        return getNewProjectDescriptionFieldLocator();
    }

    public SelenideElement getNewProjectEmailField() {
        return getNewProjectEmailFieldLocator();
    }

    public SelenideElement getNewItem() {
        executeJavaScript("$('.table-header').get(0).scrollIntoView()");
        return getNewItemLocator();
    }

    public SelenideElement getProjectDescriptionField() { return getProjectDescriptionFieldLocator(); }

    public SelenideElement getProjectEmailField() {
        return getProjectEmailFieldLocator();
    }

    public SelenideElement getSearchedItemSelect() {
        return getSearchedProjectSelectLocator();
    }

    public SelenideElement getNewPhaseCodeField() {
        return getNewPhaseCodeFieldLocator();
    }

    public SelenideElement getNewLocationCodeField() {
        return getNewLocationCodeFieldLocator();
    }

    public SelenideElement getNewEquipmentCodeField() {
        return getNewEquipmentCodeFieldLocator();
    }

    public SelenideElement getNewEquipmentNameField() {
        return getNewEquipmentNameFieldLocator();
    }

    public SelenideElement getNewPhaseNameField() {
        return getNewPhaseNameFieldLocator();
    }

    public SelenideElement getNewLocationNameField() {
        return getNewLocationNameFieldLocator();
    }

    public SelenideElement getPhaseNameField() { return getPhaseNameFieldLocator(); }

    public void selectPhaseChecklists(String number) {
        getSelectAllCheckboxLocator().click();
        Common.pause(200);
        getSelectAllConfirmOkLocator().click();

        for (int i=1; i<=Integer.parseInt(number); i++) {
            getFilterCheckboxLocator(String.valueOf(i)).click();
        }
    }

    public void selectResponsiblePartyPerson(String number) {
        getSelectAllCheckboxLocator().click();
        Common.pause(200);
        getSelectAllConfirmOkLocator().click();
        for (int i=1; i<=Integer.parseInt(number); i++) {
            getFilterCheckboxLocator(String.valueOf(i)).click();
        }
        Common.pause(5000);
    }

    public void selectInspectorChecklists(String number) {
        getSelectAllCheckboxLocator().click();
        for (int i=1; i<=Integer.parseInt(number); i++) {
            getInspectorChecklistsLocator(String.valueOf(i)).click();
        }
    }

    public SelenideElement getDeleteButton() { return getDeleteButtonLocator(); }

    public SelenideElement getMarkedToDeleteItem() { return getMarkedToDeleteItemLocator(); }

    public SelenideElement getDeleteModalButton(String buttonName) { return getDeleteModalButtonLocator(buttonName); }

    public SelenideElement getCancelButton() {
        return getCancelButtonLocator();
    }

    public SelenideElement getNextButton() {
        return getNextButtonLocator();
    }

    public SelenideElement getAttachColumn() {
        return getAttachColumnLocator();
    }

    public SelenideElement getChecklistDropDownMenu() {
        return getChecklistDropDownMenuLocator();
    }

    public SelenideElement getPlanItemCode() {
        return getPlanItemCodeLocator();
    }

    public SelenideElement getItpChecklistDropdown() {
        return getItpChecklistsDropdownLocator();
    }

    public SelenideElement getItpChecklistDropdownInput() {
        return getItpChecklistDropdownInputLocator();
    }

    public SelenideElement getItpChecklistDropdownMenu(String checklist) {
        return getItpChecklistDropdownMenuLocator(checklist);
    }

    public void selectCreatePermission() {
        getCreatePermissionLocator().click();
    }
}


