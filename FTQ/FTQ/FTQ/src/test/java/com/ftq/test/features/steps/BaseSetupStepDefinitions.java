package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.AccountSetupPage;
import com.ftq.test.pages.BaseSetupPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class BaseSetupStepDefinitions {

    @Then("^I 'should' see '/Settings/BaseSetup' page$")
    public void iShouldSeeBaseSetupPage() {
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);
        BaseSetupPage baseSetupPage = page(BaseSetupPage.class);

        baseSetupPage.getTitle().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
        baseSetupPage.getButton("Save").shouldBe(Condition.visible);
        baseSetupPage.getButton("Cancel").shouldBe(Condition.visible);
    }
}
