package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.User;
import com.ftq.test.pages.CommunitySetupPage;
import com.ftq.test.pages.ResponsiblePartySetupPage;
import com.ftq.test.pages.UserSetupPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.ftq.test.utils.Common.getUniqueEmail;
import static com.ftq.test.utils.Common.getUniqueValue;

public class ResponsiblePartyStepDefinitions {

    private String firstName, inspectionType, lastName, email, username;

    @When("^I select '(.*)' responsible party$")
    public void iSelectXXXResponsibleParty(String responsibleParty) {
        ResponsiblePartySetupPage responsiblePartySetupPage = page(ResponsiblePartySetupPage.class);
        Common.pause(5000);
        responsiblePartySetupPage.selectResposibleParty(responsibleParty);
    }

    @Then("^I '(.*)' see user with following data on 'Crew' tab on 'Responsible Party' setup page:$")
    public void iConditionSeeUserWithFollowingDataOnUserSetupPage(String condition, List<User> userList) {
        ResponsiblePartySetupPage responsiblePartySetupPage = page(ResponsiblePartySetupPage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        Condition should = getVisibleCondition(condition);

        if(condition.equalsIgnoreCase("should")) {

            for (User user : userList) {
                username = user.getUsername();
                firstName = user.getFirstName();
                inspectionType = user.getInspectionType();
                lastName = user.getLastName();
                email = user.getNewUserEmail();

                responsiblePartySetupPage.getUsernameField().shouldHave(text(getUniqueValue(username)));
                responsiblePartySetupPage.getFirstNameField().shouldHave(attribute("value", firstName));
                responsiblePartySetupPage.getLastNameField().shouldHave(attribute("value", lastName));
                responsiblePartySetupPage.getEmailField().shouldHave(attribute("value", getUniqueEmail(email)));
                responsiblePartySetupPage.getInspType().shouldHave(text(inspectionType));
            }
        } else {
            responsiblePartySetupPage.getNoListedUsersText().shouldHave(text("There are no users listed"));
        }
    }
}
