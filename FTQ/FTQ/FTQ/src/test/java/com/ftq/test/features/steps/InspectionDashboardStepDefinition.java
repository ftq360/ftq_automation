package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.InspectionDashboardPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class InspectionDashboardStepDefinition {

    @Then("^I 'should' see '/Dashboards/InspectionDashboard' page$")
    public void iShouldSeeChecklistsInspectionDashboardPage() {
        InspectionDashboardPage inspectionDashboardPage = page(InspectionDashboardPage.class);

        inspectionDashboardPage.getChartTitle("Time Period").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Inspections").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Stats").shouldBe(Condition.visible);

        inspectionDashboardPage.getChartTitle("Project").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Responsible Party").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Inspector").shouldBe(Condition.visible);

        inspectionDashboardPage.getChartTitle("Checklist").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Inspector Type").shouldBe(Condition.visible);
        inspectionDashboardPage.getChartTitle("Status").shouldBe(Condition.visible);
    }
}
