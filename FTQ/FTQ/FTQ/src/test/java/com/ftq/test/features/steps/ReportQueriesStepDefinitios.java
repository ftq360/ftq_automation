package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ReportQueriesPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ReportQueriesStepDefinitios {

    @Then("^I 'should' see '/Reports/Queries' page$")
    public void iShouldSeeReportQueriesPage() {
        ReportQueriesPage reportQueriesPage = page(ReportQueriesPage.class);

        reportQueriesPage.getTitle().get(0).shouldBe(Condition.visible);
        reportQueriesPage.getQueriesList().get(0).shouldBe(Condition.visible);
        reportQueriesPage.getQueryIdInputField().get(0).shouldBe(Condition.visible);
        reportQueriesPage.getRunButton().get(0).shouldBe(Condition.visible);
    }
}
