package com.ftq.test.objects;

public class Phase {

    private String phaseName;
    private String code;
    private String checklist;

    public String getPhaseName() { return phaseName; }

    public String getPhaseCode() { return code; }

    public String getChecklist() {
        return checklist;
    }
}
