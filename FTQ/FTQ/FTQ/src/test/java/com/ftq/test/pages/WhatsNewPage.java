package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WhatsNewPage {

    private SelenideElement getTitleLocator() { return $("h1[title=\"What's New in 2018\"]");}

    public SelenideElement getTitle() {
        return getTitleLocator();
    }
}
