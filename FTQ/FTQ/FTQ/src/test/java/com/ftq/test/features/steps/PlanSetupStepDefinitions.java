package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.*;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class PlanSetupStepDefinitions {

    @Then("^I 'should' see '/Settings/PlanSetup' page$")
    public void iShouldSeePlanSetupPage() {
        UserBasePage userBasePage = page(UserBasePage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);
        CommonPage commonPage = page(CommonPage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        userBasePage.getSearchField().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
        commonPage.getColumnTitle("Code").shouldBe(Condition.visible);
        commonPage.getColumnTitle("Name").shouldBe(Condition.visible);
        commonPage.getColumnTitle("Description").shouldBe(Condition.visible);
        communitySetupPage.getSaveButton().shouldBe(Condition.visible);
        communitySetupPage.getNextButton().shouldBe(Condition.visible);
        communitySetupPage.getCancelButton().shouldBe(Condition.visible);
    }
}
