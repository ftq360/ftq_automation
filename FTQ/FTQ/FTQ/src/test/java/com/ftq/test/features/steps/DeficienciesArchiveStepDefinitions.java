package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.DeficienciesArchivePage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class DeficienciesArchiveStepDefinitions {

    @Then("^I 'should' see '/Deficiencies/Archive' page$")
    public void iShouldSeedeficienciesArchivePage() {
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);

        deficienciesArchivePage.getColumnTitle("Insp. ID").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Def. ID").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Created").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Fixed").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Status").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("R4R").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Checklist/No").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Chkpt./Notes").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Resp. Party/Crew").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Proj").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Phase/Location/Ref#").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Equip.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Chkpt. Insp.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("R").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Last Chkpt. Insp.").shouldBe(Condition.visible);
    }
}
