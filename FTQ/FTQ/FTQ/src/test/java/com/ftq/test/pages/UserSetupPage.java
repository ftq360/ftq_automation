package com.ftq.test.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.ftq.test.utils.Common.scrollToBottomOfElementByXpath;

public class UserSetupPage {

    //Elements on the page

    private SelenideElement getAddButtonLocator() { return $(".th-plus-icon"); }
    private SelenideElement getSaveButtonLocator() { return $("[data-bind*='save']"); }
    private SelenideElement getNewUserFirstNameFieldLocator() { return $(".has-changes [data-bind*=FirstName]"); }
    private SelenideElement getNewUserInspectionTypeFieldLocator() { return $(".has-changes [data-bind*=InspectorType]~div"); }
    private SelenideElement getInspectionTypeInputFieldLocator() { return $(By.xpath("//*[@class='has-changes']//*[contains(@data-bind,'InspectorType')]/..//input")); }
    private SelenideElement getNewUserLastNameFieldLocator() { return $(".has-changes [data-bind*=LastName]"); }
    private SelenideElement getNewUserPasswordLocator() { return $(".has-changes [data-bind*=Password]"); }
    private SelenideElement getConfirmNewPasswordLocator() { return $(".has-changes [data-bind*=Confirmation]") ; }
    private SelenideElement getNewUserEmailFieldLocator() { return $(".has-changes [data-bind*=Email]"); }
    private SelenideElement getNewUserUserNameLocator() { return $(".has-changes [data-bind*=UserName]"); }
    private SelenideElement getSearchedUserLocator() { return $("[data-bind*=UserName]"); }
    private SelenideElement getSearchedUserRadioButtonLocator() { return $("[name='rowselect']"); }
    private SelenideElement getDeleteButtonLocator() { return $(".delete-icon"); }
    private SelenideElement getMarkedToDeleteItemLocator() { return $(".marked-for-delete"); }
    private SelenideElement getInspTypeLocator() { return $(By.xpath("//select[contains(@data-bind,'InspectorType')]/..//span")); }
    private SelenideElement getEmptyMessageLocator(){return $("[data-bind=\"text: emptyText \"]"); }
    private SelenideElement getUsernameLocator() { return $(By.xpath("//*[@placeholder='Enter Username']"));}
    private SelenideElement getFirstNameLocator() { return $(By.xpath("//*[contains(@data-bind,'FirstName')]"));}
    private SelenideElement getLastNameLocator() { return $(By.xpath("//*[contains(@data-bind,'LastName')]"));}
    private SelenideElement getEmailLocator() { return $(By.xpath("//*[@placeholder='Enter Email']"));}
    private SelenideElement getNoListedUsersTextLocator() { return $("[data-bind*='emptyText']"); }
    private SelenideElement getUserPasswordLocator() { return $("[data-bind*=Password]"); }
    private SelenideElement getConfirmPasswordLocator() { return $("[data-bind*=Confirmation]") ; }
    private String getSaveButtonLocatorPath() { return "//*[contains(@data-bind,'save')]"; }
    private ElementsCollection getNewUserLinesListLocator() { return $$(".has-changes"); }
    private SelenideElement getCancelButtonLocator() { return $("[data-bind*='discardChanges']"); }
    private SelenideElement getSendInvitationButtonLocator() { return $("span>.big-default-btn"); }
    private SelenideElement getStepItemLocator(int indx){return  $(By.xpath(String.format("//ul[contains(@data-bind,'steps')]//li[%d]",indx)));}
    private SelenideElement getProcessAreaDivLocator(){ return $(By.xpath("//div[@class='process-area']/div"));}
    private SelenideElement getAffiliationLocator() { return $(".has-changes [data-bind*=VendorID]~div");}
    private SelenideElement getAffiliationLocator2() { return $(By.xpath("(//select[contains(@data-bind,'VendorID')]/parent::td//a[@class='chzn-single'])[1]")); }
    private SelenideElement getAffiliationInputFiledLocator() { return $(By.xpath("(//select[contains(@data-bind,'VendorID')]/parent::td//a[@class='chzn-single'])[1]/..//input")); }
    //private SelenideElement getAffiliationDDItemLocator(String affiliation) { return $(String.format(".chzn-container-active li:contains('%s')",affiliation)); }

    //Next locators are the same and can be refactored to active dd selection
    private SelenideElement getAffiliationItemLocator(String affiliation) { return $(By.xpath(String.format("//div[contains(concat(' ',normalize-space(@class),' '),' chzn-with-drop ')]//li[contains(text(),'%s')]", affiliation))); }
    private SelenideElement getNewUserInspectionTypeItemLocator(String inspectionType) { return $(By.xpath(String.format("//div[contains(concat(' ',normalize-space(@class),' '),' chzn-with-drop ')]//li[contains(text(),'%s')]", inspectionType))); }

    //Actions on the page

    public SelenideElement getAddButton() {
        return getAddButtonLocator();
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public void clickSaveButton() {
        scrollToBottomOfElementByXpath(getSaveButtonLocatorPath());
        getSaveButtonLocator().click();
    }

    public SelenideElement getNewUserFirstNameField() { return getNewUserFirstNameFieldLocator(); }

    public void selectNewUserInspectionType(String inspectionType) {
        getNewUserInspectionTypeFieldLocator().click();
        getNewUserInspectionTypeFieldLocator().waitUntil(cssClass("chzn-with-drop"),3000);
        getNewUserInspectionTypeItemLocator(inspectionType).click();
        //getNewUserInspectionTypeItemLocator(inspectionType).click();
//        getInspectionTypeInputFieldLocator().waitUntil(visible, 60000);
//        getInspectionTypeInputFieldLocator().click();
//        getInspectionTypeInputFieldLocator().setValue(inspectionType);
//        getInspectionTypeInputFieldLocator().sendKeys(Keys.ENTER);
    }

    public void selectAffiliation(String affiliation) {
        getAffiliationLocator().click();
        getAffiliationLocator().waitUntil(cssClass("chzn-with-drop"),3000);
        getAffiliationItemLocator(affiliation).click();
//        getAffiliationInputFiledLocator().waitUntil(visible, 60000);
//        getAffiliationInputFiledLocator().click();
//        getAffiliationInputFiledLocator().setValue(affiliation);
//        getAffiliationInputFiledLocator().sendKeys(Keys.ENTER);
    }

    public SelenideElement getNewUserLastNameField() { return getNewUserLastNameFieldLocator(); }

    public SelenideElement getNewUserPassword() { return getNewUserPasswordLocator(); }

    public SelenideElement getConfirmNewPassword() { return getConfirmNewPasswordLocator(); }

    public SelenideElement getNewUserEmailField() { return getNewUserEmailFieldLocator(); }

    public SelenideElement getNewUserUserName() { return getNewUserUserNameLocator(); }

    public SelenideElement getSearchedUser() { return  getSearchedUserLocator();}

    public static Condition getVisibleConditionUser(String condition) {
        if (condition.equalsIgnoreCase("should")) {
            return visible;
        } else {
            return hidden;
        }
    }

    public SelenideElement getSearchedUserRadioButton() { return getSearchedUserRadioButtonLocator(); }

    public SelenideElement getDeleteButton() { return getDeleteButtonLocator(); }

    public SelenideElement getMarkedToDeleteItem() { return getMarkedToDeleteItemLocator(); }

    public SelenideElement getInspType() {
        return getInspTypeLocator();
    }

    public SelenideElement getEmptyMessage() { return getEmptyMessageLocator() ;}

    public SelenideElement getUsernameField() {
        return getUsernameLocator();
    }

    public SelenideElement getFirstNameField() {
        return getFirstNameLocator();
    }

    public SelenideElement getLastNameField() {
        return getLastNameLocator();
    }

    public SelenideElement getEmailField() {
        return getEmailLocator();
    }

    public SelenideElement getNoListedUsersText() {
        return getNoListedUsersTextLocator();
    }

    public SelenideElement getUserPassword() {
        return getUserPasswordLocator();
    }

    public SelenideElement getConfirmPassword() {
        return getConfirmPasswordLocator();
    }

    public ElementsCollection getNewUserLinesList() {
        return getNewUserLinesListLocator();
    }

    public SelenideElement getCancelButton() {
        return getCancelButtonLocator();
    }

    public SelenideElement getSendInvitationButton() {
        return getSendInvitationButtonLocator();
    }

    public SelenideElement getStepItem(String stepName){
        int indx=0;
        switch (stepName){
            case "Users":indx=1; break;
            case "Checklists":indx=2; break;
            case "Proj. Access":indx=3; break;
            case "Permissions":indx=4; break;
            case "Linked Accounts":indx=5; break;
            default: indx=-1;
        }

        return getStepItemLocator(indx);
    }

    // If visible -> data is loaded
    public SelenideElement getRightPanelData(){
        return getProcessAreaDivLocator();
    }
}
