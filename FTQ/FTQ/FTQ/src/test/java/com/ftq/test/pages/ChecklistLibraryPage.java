package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ChecklistLibraryPage {

    private SelenideElement getAuthorColumnLocator() { return $(By.xpath("//th[text()='Author']")); }
    private SelenideElement getAllTradesDropdownLocator() { return $(By.xpath("//span[text()='All Trades']")); }
    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Checklist Library']")); }

    public SelenideElement getAuthorColumn() {
        return getAuthorColumnLocator();
    }

    public SelenideElement getAllTradesDropdown() {
        return getAllTradesDropdownLocator();
    }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }
}
