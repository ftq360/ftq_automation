package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
//import com.sun.org.apache.bcel.internal.generic.RET;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class CreateInspectionPage {

    private SelenideElement getDivisionLocator(String divisionName) { return $(By.xpath(String.format("//div[@class='item-text' and contains(.,'%s')]", divisionName))); }
    private SelenideElement getShowRecentInspectionsButtonLocator() { return $(By.xpath("//span[contains(text(),'Show Recent Inspections')]")); }
    private SelenideElement getInspectionIdLocator() { return $("tr:first-of-type>td>span[data-bind*='/Inspection#']>a"); }
    private SelenideElement getLastActivityLocator() { return $("tr:first-of-type>td>span[data-bind*='DateInspectionMod']"); }
    private SelenideElement getStatusLocator() { return $("tr:first-of-type>td>span[data-bind*='StatusText']"); }
    private SelenideElement getChecklistLocator() { return $("tr:first-of-type>td>span[data-bind*='TradeAndTaskSupplier']"); }
    private SelenideElement getCrewLocator() { return $("tr:first-of-type>td>span[data-bind*='VendorDescriptionSupplier']"); }
    private SelenideElement getProjectLocator() { return $("tr:first-of-type>td>span[data-bind*='Misc']"); }
    private SelenideElement getLastInspectionLocator() { return $("tr:first-of-type>td>span[data-bind*='CrewLabelCustomer']"); }
    private SelenideElement getInspectionGUIDLocator(String guid) { return $(String.format("[href*='%s']", guid)); }
    private ElementsCollection getInspectionsListLocator() { return $$(By.xpath("//a/div[@class='item-text']")); }
    private SelenideElement getPlanModeCheckboxLocator() { return $("div.checkbox[data-bind*='setPlanMode']"); }
    private SelenideElement getInspectionSelectionStepTitleLocator(String title) {return $(By.xpath(String.format("//*[@id='select-phase'][contains(text(), '%s')]", title))); }
    private SelenideElement getTitleLocator(String titleText) { return $(By.xpath(String.format("//*[@class='item-text'][contains(text(), '%s')]", titleText))); }
    private SelenideElement getTitleByNameLocator(String titleName) { return $(By.xpath(String.format("//div[contains(text(),'%s')]", titleName))); }
    private SelenideElement getChecklistCodeLocator(String code) { return $(By.xpath(String.format("//div[contains(text(),'%s')]", code))); }

    public SelenideElement selectDivision(String divisionName) {
       return getDivisionLocator(divisionName);
    }

    public SelenideElement getDivision(String divisionName) {
        return getDivisionLocator(divisionName);
    }

    public SelenideElement getShowRecentInspectionsButton() {
        return getShowRecentInspectionsButtonLocator();
    }

    public SelenideElement getInspectionId() {
        return getInspectionIdLocator();
    }

    public SelenideElement getLastActivity() {
        return getLastActivityLocator();
    }

    public SelenideElement getStatus() {
        return getStatusLocator();
    }

    public SelenideElement getChecklist() {
        return getChecklistLocator();
    }

    public SelenideElement getCrew() {
        return getCrewLocator();
    }

    public SelenideElement getProject() {
        return getProjectLocator();
    }

    public SelenideElement getLastInspection() {
        return getLastInspectionLocator();
    }

    public SelenideElement getInspectionGUID(String guid) {
        return getInspectionGUIDLocator(guid);
    }

    public ElementsCollection getInspectionsList() {
        return getInspectionsListLocator();
    }

    public SelenideElement getPlanModeCheckbox() {
        return getPlanModeCheckboxLocator();
    }

    public SelenideElement getTitle(String titleText) {
        return getTitleLocator(titleText);
    }
    public SelenideElement getInspectionSelectionStepTitle(String title){
        return getInspectionSelectionStepTitleLocator(title);
    }

    public SelenideElement getTitleByName(String titleName) {
        return getTitleByNameLocator(titleName);
    }

    public SelenideElement getChecklistCode(String code) {
        return getChecklistCodeLocator(code);
    }

    public void setPlanMode(boolean isSet){
        executeJavaScript(String.format("app.vm.setPlanMode(%s)",isSet));
    }
}