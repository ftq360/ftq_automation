package com.ftq.test.objects;

public class Equipment {

    private String code;
    private String equipmentName;

    public String getEquipmentCode() { return code; }

    public String getEquipmentName() { return equipmentName; }
}
