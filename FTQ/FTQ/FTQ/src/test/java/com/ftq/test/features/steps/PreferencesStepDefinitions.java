package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.*;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class PreferencesStepDefinitions {

    @Then("^I 'should' see '/Settings/Preferences' page$")
    public void iShouldSeePreferencesPage() {
        PreferencesPage preferencesPage = page(PreferencesPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        preferencesPage.getTitle().shouldBe(Condition.visible);
        communitySetupPage.getSaveButton().shouldBe(Condition.visible);
        communitySetupPage.getCancelButton().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Term").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Abbreviation").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Plural").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("FTQ360 Default").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("How term is used").shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
    }
}
