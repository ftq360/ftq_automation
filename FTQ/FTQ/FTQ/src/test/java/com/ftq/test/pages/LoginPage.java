package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    //Elements on the page

    private SelenideElement getUsernameFieldElement() { return $("#user_login.input"); }
    private SelenideElement getPasswordFieldElement() { return $("#user_pass.input"); }
    private SelenideElement getLoginButtonElement() { return $("#wp-submit"); }
    private SelenideElement getLoginErrorLocator() { return $(".validation-summary-errors"); }
    private SelenideElement getForgotPasswordLinkLocator() { return $("[href='/Account/ForgotPassword'][title='Reset forgotten password']"); }

    //Actions on the page

    /**
     * Fill login form
     * @param username username
     * @param password user password
     * @return Login page object
     */
    public LoginPage fillForm(String username, String password) {
        getUsernameFieldElement().setValue(username);
        getPasswordFieldElement().setValue(password);
        return this;
    }

    /**
     * Click on login button
     * @return Login page object
     */
    public LoginPage clickLoginButton() {
        getLoginButtonElement().click();
        return this;
    }

    /**
     * Submit login form
     * @param username username
     * @param password user password
     */
    public LoginPage submitForm(String username, String password) {
        fillForm(username, password);
        clickLoginButton();
        return this;
    }

    public SelenideElement getLoginError() {
        return getLoginErrorLocator();
    }

    public SelenideElement getForgotPasswordLink() {
        return getForgotPasswordLinkLocator();
    }
}


