package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UserLicenseAgreementPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//div[text()='SOFTWARE LICENSE AGREEMENT']")); }
    private SelenideElement getLicenseAgreementFieldLocator() { return $(By.xpath("//textarea")); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getLicenseAgreementField() {
        return getLicenseAgreementFieldLocator();
    }
}
