package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DeficienciesArchivePage {

    private SelenideElement getColumnTitleLocator(String title) { return $(By.xpath(String.format("//th[contains(.,'%s')]", title)));}

    public SelenideElement getColumnTitle(String title) {
        return getColumnTitleLocator(title);
    }
}
