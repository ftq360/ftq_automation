package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ReportsHistoryPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Report Processing']"));}
    private SelenideElement getRefreshButtonLocator() { return $(".ui.button .refresh.icon"); }
    private SelenideElement getReportNameLocator(String name) { return $(By.xpath(String.format("(//span[text()='%s'])[1]", name))); }
    private SelenideElement getRequestDateLocator(String date) { return $(By.xpath(String.format("//td[text()='%s']", date))); }
    private SelenideElement getCompletedReportStatusLocator() { return $(By.xpath("(//span[text()='Completed'])[1]")); }
    private SelenideElement getEmailButtonLocator() { return $(By.xpath("(//button[contains(@data-bind, 'openSendDialog')])[1]")); }
    private ElementsCollection getClearButtonsListLocator() { return $$(".delete.icon"); }
    private SelenideElement getEmailFieldLocator(String fieldType) { return $(By.xpath(String.format("//label[text()='%s']/following-sibling::autocomplete//input[@class='search']", fieldType))); }
    private SelenideElement getFormatRadiobuttonLocator(String format) { return $(String.format("[value='%s']", format)); }
    private SelenideElement getSendButtonLocator() { return $(By.xpath(String.format("//button[text()='Send']"))); }
    private ElementsCollection getReportDeleteButtonsListLocator() { return $$(".trash.icon"); }
    private  SelenideElement getYesModalButtonLocator() { return  $("[data-bind='text:$data.okButtonText']"); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getRefreshButton() {
        return getRefreshButtonLocator();
    }

    public SelenideElement getReportName(String name) {
        return getReportNameLocator(name);
    }

    public SelenideElement getRequestDate(String date) {
        return getRequestDateLocator(date);
    }

    public void clickRefreshButtonUntilReportStatusBecomesCompleted() {
        int counter = 0;
        while (!getCompletedReportStatusLocator().isDisplayed() && counter < 60) {
            counter++;
            getRefreshButton().click();
            Common.pause(2000);
        }
    }

    public SelenideElement getEmailButton() {
        return getEmailButtonLocator();
    }

    public ElementsCollection getClearButtonsList() {
        return getClearButtonsListLocator();
    }

    public SelenideElement getEmailField(String fieldType) {
        return getEmailFieldLocator(fieldType);
    }

    public SelenideElement getFormatRadioButton(String format) {
        return getFormatRadiobuttonLocator(format);
    }

    public SelenideElement getSendButton() {
        return getSendButtonLocator();
    }

    public ElementsCollection getReportDeleteButtonsList() {
        return getReportDeleteButtonsListLocator();
    }

    public SelenideElement getYesModalButton() {
        return getYesModalButtonLocator();
    }
}
