package com.ftq.test.objects;

public class User {

    private String username;
    private String password;
    private String inspectionType;
    private String firstName;
    private String lastName;
    private String confirmPassword;
    private String email;
    private String inspect;
    private String editInsp;
    private String dataOverrideDelete;
    private String affiliation;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() { return firstName; }

    public String getInspectionType() { return inspectionType; }

    public String getLastName() { return lastName; }

    public String getConfirmPassword() { return confirmPassword; }

    public String getNewUserEmail() { return email; }

    public String getInspect() {
        return inspect;
    }

    public String getEditInsp() { return editInsp; }

    public String getDataOverrideDelete() { return dataOverrideDelete; }

    public String getAffiliation() {
        return affiliation;
    }
}
