package com.ftq.test.objects;

public class SendReport {

    private String to;
    private String format;

    public String getTo() {
        return to;
    }

    public String getFormat() {
        return format;
    }
}
