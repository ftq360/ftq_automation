package com.ftq.test.utils;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.*;

import java.util.List;

public class TestRailsApi {

    private final String endPoint = Common.getConfigValue("test.rails.url");
    private final String username = Common.getConfigValue("test.rails.username");
    private final String password = Common.getConfigValue("test.rails.password");
    private TestRail testRail;
    private final String projectName = Common.getConfigValue("test.rails.project.name");
    private final String runName = Common.getConfigValue("test.rails.run.name");

    public TestRailsApi() {
        testRail = TestRail.builder(endPoint, username, password).build();
    }

    private Project getProject(String projectName) {
        List<Project> projects = testRail.projects().list().execute();
        Project project = null;

        for (Project project_tmp : projects) {
            String tmp = project_tmp.getName();
            if (tmp.equals(projectName)) {
                project = project_tmp;
                break;
            }
        }
        return project;
    }

    private int getProjectId(Project project) {
        return project.getId();
    }

    private Run getRun(int projectId, String runName) {
        List<Run> runs = testRail.runs().list(projectId).execute();
        Run run = null;

        for (Run run_tmp : runs) {
            String tmp = run_tmp.getName();
            if (tmp.equals(runName)) {
                run = run_tmp;
                break;
            }
        }
        return run;
    }

    private int getRunId(Run run) {
        return run.getId();
    }

    private int getTestId(int runId, String testName) {
        List<Test> tests = testRail.tests().list(runId).execute();
        int testId = 0;

        for (Test test_tmp : tests) {
            String tmp = test_tmp.getTitle();
            if (tmp.equals(testName)) {
                testId = test_tmp.getId();
                break;
            }
        }
        return testId;
    }

    public void setStatus(String testName, int status) {

        Project project = getProject(projectName);
        int projectId = getProjectId(project);

        Run run = getRun(projectId, runName);
        int runId = getRunId(run)
                ;

        int testId = getTestId(runId, testName);

        if (testId == 0) {
            System.out.println("There is no scenario on Test Rail: " + testName);
        }
        else {
            List<ResultField> customResultFields = testRail.resultFields().list().execute();
            testRail.results().add(testId, new Result().setStatusId(status), customResultFields).execute();
        }
    }
}
