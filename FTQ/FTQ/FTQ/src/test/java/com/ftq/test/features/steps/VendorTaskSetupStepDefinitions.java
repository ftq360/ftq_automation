package com.ftq.test.features.steps;

import com.ftq.test.objects.VendorTaskSetup;
import com.ftq.test.pages.*;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.*;

public class VendorTaskSetupStepDefinitions {

    private String description, email;
    private Boolean isActive;

    @When("^I add following data for new responsible party on VendorTaskSetup page:$")
    public void iAddFollowingDataForNewResponsiblePartyOnVendorTaskSetupPage(List<VendorTaskSetup> vendorTaskSetupList) {
        VendorTaskSetupPage vendorTaskSetupPage = page(VendorTaskSetupPage.class);

        for(VendorTaskSetup vendorTaskSetup : vendorTaskSetupList) {
            description = vendorTaskSetup.getDescription();
            email = vendorTaskSetup.getEmail();
            isActive = vendorTaskSetup.isActive();

            vendorTaskSetupPage.getCodeField().waitUntil(exist, 30000);
            vendorTaskSetupPage.getCodeField().waitUntil(visible, 30000);

            if (StringUtils.isNotEmpty(description)) {
                vendorTaskSetupPage.getDescriptionField().setValue(getUniqueValue(description));
            }
            if (StringUtils.isNotEmpty(description)) {
                vendorTaskSetupPage.getEmaiField().setValue(getUniqueEmail(email));
            }
            if (StringUtils.isNotEmpty(description)) {
                vendorTaskSetupPage.getActiveCheckbox().setSelected(isActive);
            }
            vendorTaskSetupPage.getCodeField().waitUntil(visible, 30000);
            pause(2000);
        }
    }

    @Then("^I 'should' see '/Settings/VendorTasksSetup' page$")
    public void iShouldSeeVendorTasksSetupPage() {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        taskSetupPage.getSaveButton().shouldBe(visible);
        taskSetupPage.getAddButton().shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Code").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Org. Type").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Description").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Reports").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Email").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Notes").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Category").shouldBe(visible);
        //deficienciesArchivePage.getColumnTitle("Is Partner").shouldBe(visible);
        communitySetupPage.getAttachColumn().shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Active").shouldBe(visible);
        communitySetupPage.getCancelButton().shouldBe(visible);
        userBasePage.getSearchField().shouldBe(visible);
        accountSetupPage.getProcessMenu().shouldBe(visible);
    }
}
