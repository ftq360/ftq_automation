package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.AccountSetupPage;
import com.ftq.test.pages.CommonPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class AccountSetupStepDefinitions {

    @Then("^I 'should' see '/Settings/AccountSetup' page$")
    public void iShouldSeeAccountSetupPage() {
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);

        accountSetupPage.getTitle().shouldBe(Condition.visible);
        accountSetupPage.getAccountDetailsTable().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
        accountSetupPage.getSaveButton().shouldBe(Condition.visible);
        accountSetupPage.getCancelButton().shouldBe(Condition.visible);
    }

    @Then("^I 'should' see '(.*)' legend on the Account Setup page$")
    public void iShouldSeeXXXLegendOnTheAccountSetupPage(String legend) {
        CommonPage commonPage = page(CommonPage.class);

        commonPage.getLegendTitle(legend).shouldBe(Condition.visible);
    }
}
