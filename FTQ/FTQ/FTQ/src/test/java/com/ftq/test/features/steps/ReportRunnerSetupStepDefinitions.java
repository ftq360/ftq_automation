package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.*;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ReportRunnerSetupStepDefinitions {

    @Then("^I 'should' see '/Settings/ReportRunnerSetup' page$")
    public void iShouldSeeReportRunnerSetupPage() {
        ReportRunnerSetupPage reportRunnerSetupPage = page(ReportRunnerSetupPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        reportRunnerSetupPage.getTitle().shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Code").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Description").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Show Online").shouldBe(Condition.visible);
        reportRunnerSetupPage.getRunOnScheduleColumn().shouldBe(Condition.visible);
        reportRunnerSetupPage.getScheduleColumn().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Loop").shouldBe(Condition.visible);
        communitySetupPage.getSaveButton().shouldBe(Condition.visible);
        communitySetupPage.getCancelButton().shouldBe(Condition.visible);
    }
}
