package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.Report;
import com.ftq.test.pages.AccountSetupPage;
import com.ftq.test.pages.ReportsPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.*;

public class ReportsStepDefinitions {

    @When("^I select '(.*)' date range on reports page")
    public void iSelectXXXDateRangeOnReportsPage(String dateRange) {
        ReportsPage reportsPage = page(ReportsPage.class);
        reportsPage.getDateRange(dateRange).waitUntil(Condition.exist, 2000);
        pause(1000);
        reportsPage.getDateRange(dateRange).hover();
        reportsPage.getDateRange(dateRange).click();
    }

    @When("^I click on '(.*)' button on reports page$")
    public void iClickOnXXXButtonOnReportsPage(String button) {
        ReportsPage reportsPage = page(ReportsPage.class);
        if(button.equalsIgnoreCase("Select")) {
            reportsPage.getSelectButton().click();
        } else if(button.equalsIgnoreCase("Run Report(interactive)")) {
            waitPageLoading();
            reportsPage.clickRunReportButton();
        } else if(button.equalsIgnoreCase("Run Report(online)")) {
            reportsPage.getRunReportButton().click();
        }
    }

    @When("^I select '(.*)' report on reports page$")
    public void iSelectXXXReportOnReportsPage(String report) {
        ReportsPage reportsPage = page(ReportsPage.class);
        reportsPage.getReportRadioButton(getUniqueValue(report)).click();
    }


    @Then("^I 'should' see '/Reports' page$")
    public void iShouldSeeReportsPage() {
        ReportsPage reportsPage = page(ReportsPage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);

        reportsPage.getSaveButton().shouldBe(Condition.visible);
        reportsPage.getCancelButton().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
    }

    @Then("^I should see '(.*)' report on reports page$")
    public void iShouldSeeXXXReportOnReportsPage(String report) {
        ReportsPage reportsPage = page(ReportsPage.class);
        reportsPage.getReport(getUniqueValue(report)).shouldBe(Condition.visible);
    }
}
