package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SubmitRequestPage {

    private SelenideElement getFieldTitleLocator(String title) { return $(By.xpath(String.format("//label[contains(text(), '%s')]", title)));}
    private SelenideElement getSubmitButtonLocator() { return $("[name='commit']"); }

    public SelenideElement getFieldTitle(String title) {
        return getFieldTitleLocator(title);
    }

    public SelenideElement getSumbitButton() {
        return getSubmitButtonLocator();
    }
}
