package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.SupportFtq360Page;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class SupportFtq360StepDefinitions {

    @Then("^I 'should' see 'support.ftq360' page$")
    public void iShouldSeeSupportFtq360Page() {
        SupportFtq360Page supportFtq360Page = page(SupportFtq360Page.class);

        supportFtq360Page.getBlockTitle("GENERAL").shouldBe(Condition.visible);
        supportFtq360Page.getBlockTitle("INSPECTIONS").shouldBe(Condition.visible);
        supportFtq360Page.getBlockTitle("DASHBOARDS").shouldBe(Condition.visible);
        supportFtq360Page.getBlockTitle("REPORTS").shouldBe(Condition.visible);
        supportFtq360Page.getBlockTitle("SETUP").shouldBe(Condition.visible);
        supportFtq360Page.getBlockTitle("OFFLINE").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Community").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Submit a request").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Contact Us").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Sign in").shouldBe(Condition.visible);
        supportFtq360Page.getSearchField().shouldBe(Condition.visible);
        supportFtq360Page.getLogo().shouldBe(Condition.visible);
    }
}
