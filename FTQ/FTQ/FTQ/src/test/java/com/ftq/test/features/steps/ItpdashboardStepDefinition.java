package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.Phase;
import com.ftq.test.pages.ItpdashboardPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.ftq.test.objects.PlanInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.getUniqueValue;
import static com.ftq.test.utils.Common.pause;

public class ItpdashboardStepDefinition {

    private String status, plan, icon, inspection_status;

    @Then("^I 'should' see '/Dashboards/itpdashboard' page$")
    public void iShouldSeeChecklistsItpdashboardPage() {
        ItpdashboardPage itpdashboardPage = page(ItpdashboardPage.class);

        itpdashboardPage.getTitle().shouldBe(Condition.visible);
        itpdashboardPage.getChartTitle("Project").shouldBe(Condition.visible);
        itpdashboardPage.getChartTitle("Status").shouldBe(Condition.visible);
        itpdashboardPage.getChartTitle("Due Dates").shouldBe(Condition.visible);

        itpdashboardPage.getChartTitle("Checklist Category").shouldBe(Condition.visible);
        itpdashboardPage.getChartTitle("Checklist").shouldBe(Condition.visible);

        itpdashboardPage.getChartTitle("Responsible Party").shouldBe(Condition.visible);
        itpdashboardPage.getChartTitle("Inspector Type").shouldBe(Condition.visible);
    }


    @When("^I search '(.*)' in project dropdown$")
    public void iSearchXXXInProjectDropdown(String project) {
        ItpdashboardPage itpdashboardPage = page(ItpdashboardPage.class);

        pause(5000);
        itpdashboardPage.getProjectDropdown().waitUntil(Condition.visible, 20000);
        itpdashboardPage.getProjectDropdown().hover();
        itpdashboardPage.getProjectDropdown().click();
        pause(5000);
        itpdashboardPage.getDropdownSearchField().waitUntil(Condition.visible, 20000);
        itpdashboardPage.getDropdownSearchField().setValue(getUniqueValue(project));
        pause(5000);
        //itpdashboardPage.getProjectInDropdown(getUniqueValue(project)).waitUntil(Condition.visible, 20000);
        itpdashboardPage.getProjectInDropdown(getUniqueValue(project)).click();
    }

    @When("^I click on arrow for '(.*)' plan to see inspection list$")
    public void iClickOnArrowForXXXPlanToSeeInspectionList(String plan) {
        ItpdashboardPage itpdashboardPage = page(ItpdashboardPage.class);

        itpdashboardPage.getPlanArrow(getUniqueValue(plan)).click();
    }

    @Then("^I should see plan info row section:$")
    public void iShouldSeePlanInfoRowSection(List<PlanInfo> planInfos) {
        ItpdashboardPage itpdashboardPage = page(ItpdashboardPage.class);

        for (PlanInfo planInfo : planInfos) {
            status = planInfo.getStatus();
            plan = planInfo.getPlan();
            icon = planInfo.getIcon();
            inspection_status = planInfo.getInspection_status();

            switch (icon) {
                case "red":
                    icon = "Icon_TaskSupplier_4";
                    break;
                case "green":
                    icon = "Icon_TaskSupplier_5";
                    break;
            }

            itpdashboardPage.getPlanStatus(getUniqueValue(plan), status).shouldBe(visible);
            itpdashboardPage.getPlanIcon(getUniqueValue(plan), icon).shouldBe(visible);
            itpdashboardPage.getPlanInspectionStatus(getUniqueValue(plan), inspection_status).shouldBe(visible);
        }
    }
}
