package com.ftq.test.modals;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ChangeCheckpointStatusModal {

    private SelenideElement getYesButtonLocator() { return $(".modal.transition .ui.ok"); }

    public SelenideElement getYesButton() {
        return getYesButtonLocator();
    }
}
