package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.Location;
import com.ftq.test.objects.Phase;
import com.ftq.test.objects.Project;
import com.ftq.test.objects.Equipment;
import com.ftq.test.pages.*;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.*;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.codeborne.selenide.Condition.attribute;

import java.util.List;

import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.*;

public class CommunitySetupStepDefinitions {

    private String code, description, email, phaseName, checklist, locationName, equipmentName;

    @When("^I click on '(.*)' button on community setup page$")
    public void iClickOnXXXButtonOnCommunitySetupPage(String buttonName) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        if (buttonName.equalsIgnoreCase("Add")) {
            communitySetupPage.getAddButton().waitUntil(exist, 60000);
            communitySetupPage.getAddButton().waitUntil(visible, 60000);
            communitySetupPage.getAddButton().click();
        }
        if (buttonName.equalsIgnoreCase("Save")) {
            communitySetupPage.getSaveButton().waitUntil(visible, 60000);
            communitySetupPage.getSaveButton().hover();
            communitySetupPage.getSaveButton().waitUntil(visible, 60000);
            communitySetupPage.getSaveButton().click();
            Common.waitPageLoading();
            pause(1000);
        }
        if (buttonName.equalsIgnoreCase ("Unselect All")) {
            communitySetupPage.clickUnselectAllButton();
        }
    }

    @When("^I fill new project form with next data on community setup page:$")
    public void iFillNewProjectFormWithNextDataOnCommunitySetupPage(List<Project> projects) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        for (Project project : projects) {
            code = project.getCode();
            description = project.getDescription();
            email = project.getEmail();

            if (StringUtils.isNotEmpty(code)) {
              //  communitySetupPage.getNewProjectCodeField().waitUntil(visible, 30000);
                communitySetupPage.getNewProjectCodeField().setValue(getUniqueValue(code));
            }
            if (StringUtils.isNotEmpty(description)) {
                communitySetupPage.getNewProjectDescriptionField().setValue(getUniqueValue(description));
            }
            if (StringUtils.isNotEmpty(email)) {
                communitySetupPage.getNewProjectEmailField().setValue(getUniqueEmail(email));
            }
        }
        Common.pause(2000);
    }

    @When("I delete searched project on community setup page$")
    public void iDeleteSearchedProjectOnCommunitySetupPage() {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.getDeleteButton().waitUntil(exist, 60000);
        communitySetupPage.getDeleteButton().hover();
        communitySetupPage.getDeleteButton().click();
        communitySetupPage.getMarkedToDeleteItem().shouldBe(visible);
    }

    @When("^I click on '(.*)' button on delete modal$")
    public void iClickOnXXXButtonOnDeleteModal(String buttonName){
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.getDeleteModalButton(buttonName).click();
        communitySetupPage.getDeleteModalButton(buttonName).waitUntil(Condition.disappear, 40000);
    }


    @When("I select searched item on community setup page")
    public void iSelectSearchedProjectOnCommunitySetupPage() {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        pause(3000);
        communitySetupPage.getSearchedItemSelect().waitUntil(exist, 30000);
        communitySetupPage.getSearchedItemSelect().waitUntil(visible, 30000);
        communitySetupPage.getSearchedItemSelect().click();
    }

    @When("^I fill new phase form with next data on community setup page:$")
    public void iFillNewPhaseFromWithNextDataOnCommunitySetupPage(List<Phase> phases) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        for (Phase phase : phases) {
            code = phase.getPhaseCode();
            phaseName = phase.getPhaseName();

            if (StringUtils.isNotEmpty(code)) {
                communitySetupPage.getNewPhaseCodeField().setValue(getUniqueValue(code));
            }
            if (StringUtils.isNotEmpty(phaseName)) {
                communitySetupPage.getNewPhaseNameField().setValue(getUniqueValue(phaseName));
            }
            communitySetupPage.getNewPhaseName().shouldHave(attribute("value", getUniqueValue(phaseName)));
        }
    }

    @When("^I fill new location form with next data on community setup page:$")
    public void iFillNewLocationFromWithNextDataOnCommunitySetupPage(List<Location> locations) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        for (Location location : locations) {
            code = location.getLocationCode();
            locationName = location.getLocationName();

            if (StringUtils.isNotEmpty(code)) {
                communitySetupPage.getNewLocationCodeField().click();
                communitySetupPage.getNewLocationCodeField().setValue(getUniqueValue(code));
            }
            if (StringUtils.isNotEmpty(locationName)) {
                communitySetupPage.getNewLocationNameField().setValue(getUniqueValue(locationName));
            }
            communitySetupPage.getNewLocationNameField().shouldHave(attribute("value", getUniqueValue(locationName)));
        }
    }

    @When("^I fill new equipment form with next data on community setup page:$")
    public void iFillNewEquipmentFromWithNextDataOnCommunitySetupPage(List<Equipment> equipments) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        for (Equipment equipment : equipments) {
            code = equipment.getEquipmentCode();
            equipmentName = equipment.getEquipmentName();

            if (StringUtils.isNotEmpty(code)) {
                communitySetupPage.getNewEquipmentCodeField().click();
                communitySetupPage.getNewEquipmentCodeField().setValue(getUniqueValue(code));
            }
            if (StringUtils.isNotEmpty(equipmentName)) {
                communitySetupPage.getNewEquipmentNameField().setValue(getUniqueValue(equipmentName));
            }
            communitySetupPage.getNewEquipmentNameField().shouldHave(attribute("value", getUniqueValue(equipmentName)));
        }
    }

    @When("I select '(.*)' checklists on community setup page")
    public void iSelectXXXChecklistsOnCommunitySetupPage(String checklistsNumber) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.selectPhaseChecklists(checklistsNumber);
    }

    @When("I select '(.*)' subcontractors on community setup page")
    public void iSelectXXXSubcontractorsOnCommunitySetupPage(String subcontractorsNumber) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.selectResponsiblePartyPerson(subcontractorsNumber);}

    @When ("I click on 'Select all' checkbox on community setup page")
    public void iClickOnXXXCheckboxOnCommunitySetupPage (String checkboxNumber) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.selectInspectorChecklists(checkboxNumber);
    }
    @When ("I click on 'create' checkbox on community setup page")
    public void iClickOnCreateCheckboxOnCommunitySetupPage () {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        communitySetupPage.selectCreatePermission();
    }

    @Then("^I '(.*)' see next project in he list on community setup page:$")
    public void iConditionSeeNextProjectInTheListOnCommunitySetupPage(String condition, List<Project> projects) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        Condition should = getVisibleCondition(condition);

        for (Project project : projects) {
            description = project.getDescription();
            email = project.getEmail();

            communitySetupPage.getNewItem().shouldBe(should);

            if (StringUtils.isNotEmpty(description) && condition.equalsIgnoreCase("should")) {
                communitySetupPage.getProjectDescriptionField().shouldHave(attribute("value", getUniqueValue(description)));
            }
            if (StringUtils.isNotEmpty(email)) {
                communitySetupPage.getProjectEmailField().shouldHave(attribute("value", getUniqueEmail(email)));
            }
        }
    }

    @Then("^I '(.*)' see next phase in he list on community setup page:$")
    public void iConditionSeeNextPhaseInTheListOnCommunitySetupPage(String condition, List<Phase> phases) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        Condition should = getVisibleCondition(condition);

        for (Phase phase : phases) {
            phaseName = phase.getPhaseName();

            communitySetupPage.getNewItem().shouldBe(should);

            if (StringUtils.isNotEmpty(phaseName)) {
                communitySetupPage.getPhaseNameField().shouldHave(attribute("value", getUniqueValue(phaseName)));
            }
        }
    }

    @Then("I 'should' see '/Settings/CommunitySetup' page")
    public void iShouldSeeCommunitySetupPage() {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        UserBasePage userBasePage = page(UserBasePage.class);

        accountSetupPage.getProcessMenu().shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Code").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Seq.").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Description").waitUntil(visible, 20000);
        deficienciesArchivePage.getColumnTitle("Description").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Division").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Reports").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Email").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Information").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Private Notes").shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Plan").shouldBe(visible);
        communitySetupPage.getAttachColumn().shouldBe(visible);
        deficienciesArchivePage.getColumnTitle("Active").shouldBe(visible);
        userBasePage.getSearchField().shouldBe(visible);
        communitySetupPage.getSaveButton().shouldBe(visible);
        communitySetupPage.getCancelButton().shouldBe(visible);
        communitySetupPage.getNextButton().shouldBe(visible);
    }

    @And("^I fill ITP plan item data on community setup page:$")
    public void iFillITPPlanItemDataOnCommunitySetupPage(List<Phase> phases) {
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        for (Phase phase : phases) {
            code = phase.getPhaseCode();
            checklist = phase.getChecklist();


            if (StringUtils.isNotEmpty(code)) {
                Common.pause(1000);
                communitySetupPage.getPlanItemCode().setValue(getUniqueValue(code));
            }
            if (StringUtils.isNotEmpty(checklist)) {
                communitySetupPage.getItpChecklistDropdown().click();
//                communitySetupPage.getItpChecklistDropdownMenu(getUniqueValue(checklist)).click();
                communitySetupPage.getItpChecklistDropdownInput().setValue(getUniqueValue(checklist));
                Common.pause(1000);
                communitySetupPage.getItpChecklistDropdownInput().sendKeys(Keys.ENTER);
            }
        }
    }
}
