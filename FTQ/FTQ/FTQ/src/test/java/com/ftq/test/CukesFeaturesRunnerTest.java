package com.ftq.test;

import com.codeborne.selenide.Configuration;
import com.ftq.test.utils.Common;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        format = {"html:target/cucumber-html-report", "json:target/cucumber-html-report/cucumber.json"},
        plugin = {"pretty", "rerun:target/rerun.txt"},
        monochrome = true)

public class CukesFeaturesRunnerTest {

    private static WebDriver webDriver;

    @BeforeClass
    public static void setup() {

        Configuration.baseUrl = Common.getConfigValue("app.url");
        System.out.println("-----------------------------------------------------------");
        String browser = Common.getConfigValue("browser");
        Configuration.reportsFolder = Common.getConfigValue("reports");
        Configuration.timeout = Integer.parseInt(Common.getConfigValue("timeout"));

        if ("chrome".equals(browser)) {
            String os = System.getProperty("os.name").toLowerCase();
            if (os.contains("windows")) {
                System.setProperty("webdriver.chrome.driver", "d:\\chromeDriver\\chromedriver.exe");
            }
            else if (os.contains("linux")) {
                System.setProperty("webdriver.chrome.driver", "src/test/resources/chromeDriver/chromedriver");
            }
            else if (os.contains("mac os x")) {
                System.setProperty("webdriver.chrome.driver", "src/test/resources/chromeDriver/chromedriver_mac");
            }

            ChromeOptions chromeOptions = new ChromeOptions();

            /*
            chromeOptions.addArguments("enable-automation");
            chromeOptions.addArguments("--use-fake-device-for-media-stream");
            chromeOptions.addArguments("--use-fake-ui-for-media-stream");
            chromeOptions.addArguments("--disable-user-media-security");
            chromeOptions.addArguments("--disable-web-security");
            chromeOptions.addArguments("--reduce-security-for-testing");
            chromeOptions.addArguments("--window-size=1600,900");
            chromeOptions.addArguments("--disable-infobars");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--disable-gpu");*/
            /*
             # Force the display compositor back into the browser process by disabling all GL features. This is maybe the best option.
            --disable-gpu --disable-software-rasterizer

            # Disable the GPU process sandbox which allows a message only window to get created there. There are potential security implications for disabling the GPU sandbox but it's better than --no-sandbox.
            --disable-gpu-sandbox

            # Turn off the VizDisplayCompositor feature. This code path is no longer tested on Windows so you may run into other problems. YMMV.
            --disable-features=VizDisplayCompositor
            * */
            //chromeOptions.setBinary("C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-features=VizDisplayCompositor");
            chromeOptions.addArguments("enable-automation");
            chromeOptions.addArguments("--use-fake-device-for-media-stream");
            chromeOptions.addArguments("--use-fake-ui-for-media-stream");
            chromeOptions.addArguments("--disable-user-media-security");
            chromeOptions.addArguments("--disable-web-security");
            chromeOptions.addArguments("--reduce-security-for-testing");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--window-size=1600,900");
            //chromeOptions.setHeadless(true);
            webDriver = new ChromeDriver(chromeOptions);
            setWebDriver(webDriver);
        }
    }

    @AfterClass
    public static void after() {
        getWebDriver().quit();
    }
}
