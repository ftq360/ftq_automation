package com.ftq.test.objects;

public class Inspection {

    private String inspectionName;
    private String projectName;
    private String phaseName;
    private String checkpointName;
    private String responsibleParty;
    private String link;
    private String note;
    private String fieldNumber;
    private String value;
    private String fieldName;
    private String code;

    public String getInspectionName() {
        return inspectionName;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getPhaseName() {
        return phaseName;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public String getResponsibleParty() {
        return responsibleParty;
    }

    public String getLink() {
        return link;
    }

    public String getNote() {
        return note;
    }

    public String getFieldNumber() {
        return fieldNumber;
    }

    public String getValue() {
        return value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getCode() {
        return code;
    }
}
