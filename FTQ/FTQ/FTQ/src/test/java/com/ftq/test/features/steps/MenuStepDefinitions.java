package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.menu.ProcessMenu;
import com.ftq.test.menu.TopMenu;
import com.ftq.test.pages.UserBasePage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.When;
import com.ftq.test.modals.LoadingDataModal;
import static com.codeborne.selenide.Selenide.page;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Condition.*;

public class MenuStepDefinitions {

    @When("^I click on '(.*)' top menu item$")
    public void iClickOnXXXTopMenuItem(String menuItem) {
        Common.printCurrentTime();
        TopMenu topMenu = page(TopMenu.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        LoadingDataModal loadingDataModal = page(LoadingDataModal.class);

        boolean test = Common.waitPageLoading();

        if (menuItem.equalsIgnoreCase("User Icon"))
        {
            topMenu.getUserMenuButton().waitUntil(exist, 60000);
            topMenu.getUserMenuButton().waitUntil(visible, 60000);
            topMenu.getUserMenuButton().shouldBe(Condition.visible);
            Common.pause(3000);
            topMenu.getUserMenuButton().click();
            boolean state = userBasePage.getItemFromMenuDropdown("Logout").isDisplayed();
            if(!state){
                for(int i=0; i<10; i++){
                    Common.pause(1000);
                    loadingDataModal.getLoadingModal().waitUntil(Condition.disappear, 50000);
                    topMenu.getUserMenuButton().click();
                    Common.pause(1000);
                    state = userBasePage.getItemFromMenuDropdown("Logout").isDisplayed();
                    if(state){
                        break;
                    }
                }
            }
        } else if (menuItem.equalsIgnoreCase("Signal icon")) {
            topMenu.getSignalButton().shouldBe(Condition.visible);
            topMenu.getSignalButton().click();
        } else {
            userBasePage.getTopMenuItem(menuItem).waitUntil(exist,5000);
            userBasePage.getTopMenuItem(menuItem).waitUntil(visible,5000);
            userBasePage.getTopMenuItem(menuItem).click();
            Common.pause(1000);
        }
    }

    @When("^I select '(.*)' item in appeared top menu dropdown$")
    public void iSelectXXXItemInAppearedTopMenuDropdown(String itemName) {
        Common.printCurrentTime();
        TopMenu topMenu = page(TopMenu.class);

        if (itemName.equals("Offline") || itemName.equals("Online"))
        {
            topMenu.getNetworkStatusButton(itemName).shouldBe(Condition.visible);
            topMenu.getNetworkStatusButton(itemName).click();

        } else if (itemName.equalsIgnoreCase("Sync Control Panel")) {
            topMenu.getSyncControlPanel().shouldBe(Condition.visible);
            topMenu.getSyncControlPanel().click();
        } else {
            UserBasePage userBasePage = page(UserBasePage.class);
            userBasePage.selectItemFromTopMenuDropdown(itemName);
        }
        Common.pause(1000);
    }

    @When("^I select '(.*)' dropdown in appeared top menu dropdown$")
    public void iSelectXXXDropdownInAppearedTopMenuDropdown(String dropdownName) {
        Common.printCurrentTime();
        UserBasePage userBasePage = page(UserBasePage.class);
        userBasePage.selectDropdownFromTopMenuDropdown(dropdownName);
    }

    @When("I select '(.*)' in process menu")
    public void iSelectXXXInProcessMenu(String menuItem) {
        ProcessMenu processMenu = page(ProcessMenu.class);
        processMenu.getProcessMenuItem(menuItem).waitUntil(visible, 90000);
        processMenu.getProcessMenuItem(menuItem).waitUntil(exist, 90000);
        processMenu.getProcessMenuItem(menuItem).hover();
        processMenu.getProcessMenuItem(menuItem).click();
    }

    @When("I select '(.*)' in top right dropdown")
    public void iSelectXXXInTopRightDropdown(String status) {
        ProcessMenu processMenu = page(ProcessMenu.class);
        processMenu.getTopDropdown().hover();
        processMenu.getTopDropDownList(status).waitUntil(visible, 200000);
        processMenu.getTopDropDownList(status).click();
    }

    @When("^I click on '(.*)' button in top menu$")
    public void iClickOnXXXButtonInTopMenu(String buttonName) {
        TopMenu topMenu = page(TopMenu.class);

        if (buttonName.equalsIgnoreCase("FTQ360")){
            topMenu.getFTQ360Button().click();
        }
    }
}
