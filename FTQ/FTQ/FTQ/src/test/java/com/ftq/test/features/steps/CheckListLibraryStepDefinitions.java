package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ChecklistLibraryPage;
import com.ftq.test.pages.DeficienciesArchivePage;
import com.ftq.test.pages.UserBasePage;
import cucumber.api.java.en.Then;

import java.util.Arrays;
import java.util.function.Consumer;

import static com.codeborne.selenide.Selenide.page;

public class CheckListLibraryStepDefinitions {

    @Then("^I 'should' see '/ChecklistLibrary' page$")
    public void iShouldSeeCheckListLibraryPage() {
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        ChecklistLibraryPage checklistLibraryPage = page(ChecklistLibraryPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);

        String[] columnTitlesStream = {"Checklist", "Description", "Trade", "Rev", "Checkpoint", "Upload Date", "Downloads"};
        Consumer<String> consumer = s -> deficienciesArchivePage.getColumnTitle(s).shouldBe(Condition.visible);
        Arrays.stream(columnTitlesStream).forEach(consumer);

        checklistLibraryPage.getTitle().shouldBe(Condition.visible);
        checklistLibraryPage.getAuthorColumn().shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        checklistLibraryPage.getAllTradesDropdown().shouldBe(Condition.visible);
    }
}