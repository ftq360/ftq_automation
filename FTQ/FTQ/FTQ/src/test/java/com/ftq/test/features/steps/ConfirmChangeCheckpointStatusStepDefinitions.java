package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.modals.ChangeCheckpointStatusModal;
import cucumber.api.java.en.And;

import static com.codeborne.selenide.Selenide.page;

public class ConfirmChangeCheckpointStatusStepDefinitions {
    @And("^I click '(.*)' on Change checkpoint status modal$")
    public void iClickXXXModalOnChangeCheckpointStatusModal(String button) {
        ChangeCheckpointStatusModal changeCheckpointStatusModal = page(ChangeCheckpointStatusModal.class);

        switch (button) {
            case "Yes" :
                changeCheckpointStatusModal.getYesButton().waitUntil(Condition.exist, 30000);
                changeCheckpointStatusModal.getYesButton().click();
                break;
            default: throw new IllegalArgumentException("Color not found");
        }
    }
}
