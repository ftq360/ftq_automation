package com.ftq.test.objects;

public class PlanInfo {

    private String status;
    private String plan;
    private String icon;
    private String inspection_status;

    public String getStatus() { return status; }

    public String getPlan() { return plan; }

    public String getIcon() {
        return icon;
    }

    public String getInspection_status() {
        return inspection_status;
    }
}
