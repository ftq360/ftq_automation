package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SyncControlPanelPage {

    private SelenideElement getInspectionLocator(String inspectionName) { return $(By.xpath(String.format("//a[contains(text(),'%s')]", inspectionName))); }
    private SelenideElement getInspectionSyncStatusLocator(String inspId) { return $(By.xpath(String.format("//a[contains(@href, '%s')]/ancestor::tr//i[@class='green check icon']", inspId))); }
    private SelenideElement getStatusDropdownMenuLocator(String value) { return $(By.xpath(String.format("//ul/li/a[text()='%s']", value)));}
    private SelenideElement getStatusDropdownLocator() { return $(".widest.header__filter.funcbtn span a");}
    private SelenideElement getBottomBtnLocator(String value) { return $(By.xpath(String.format("//*[text()='%s'][@class='big-default-btn']", value)));}
    private SelenideElement getSearchFieldLocator() { return $("#select-menu-filter"); }
    private SelenideElement getSyncModalButtonLocator(String buttonName) { return $(By.xpath((String.format("//div[text()='%s']", buttonName)))); }

    public SelenideElement getInspection(String inspectionName) {
        return getInspectionLocator(inspectionName);
    }

    public void waitUntilInspectionBecomesSync(String inspId) {
        int count = 0;
        while (!getInspectionSyncStatusLocator(inspId).isDisplayed() && count < 10) {
            Common.pause(1000);
            count++;
        }
    }

    public SelenideElement getStatusDropdownMenu(String value) {
        return getStatusDropdownMenuLocator(value);
    }

    public SelenideElement getStatusDropdown() {
        return getStatusDropdownLocator();
    }

    public SelenideElement getSearchField() {
        return getSearchFieldLocator();
    }

    public SelenideElement getBottomButton(String value) {
        return getBottomBtnLocator(value);
    }

    public SelenideElement getSyncModalButton(String buttonName) { return getSyncModalButtonLocator(buttonName); }


}
