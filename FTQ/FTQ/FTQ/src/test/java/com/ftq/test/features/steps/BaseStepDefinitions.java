package com.ftq.test.features.steps;

import com.codeborne.selenide.Screenshots;
import com.ftq.test.pages.LoginPage;
import com.ftq.test.utils.Common;
import com.ftq.test.utils.TestRailsApi;
import com.google.common.io.Files;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import com.ftq.test.pages.CommonPage;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import java.io.File;
import java.io.IOException;

public class BaseStepDefinitions {


    @Before
    public void beforeScenario() {
        String uniqueValue = Common.generateRandomValue();
        Common.setUniqueValue(uniqueValue);
        getWebDriver().manage().deleteAllCookies();
        open("/", LoginPage.class);
        CommonPage.guids = new ArrayList<>();
    }

    @After
    public static void tearDown(Scenario scenario) throws IOException {
        //TestRailsApi testRailsApi = new TestRailsApi();
        String testName = scenario.getName();

        if(scenario.isFailed()) {
            //testRailsApi.setStatus(testName, 5);
            File screenshot = Screenshots.getLastScreenshot();
            byte[] file = Files.toByteArray(screenshot);
            scenario.embed(file, "image/png");
        } else {
            //testRailsApi.setStatus(testName, 1);
        }
        getWebDriver().manage().deleteAllCookies();
    }
}


