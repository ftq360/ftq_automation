package com.ftq.test.menu;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class TopMenu {

    //Elements in the modal

    private SelenideElement getFTQ360ButtonLocator() { return $(".fitted a"); }
    private SelenideElement getUserMenuButtonLocator() { return $(".user.icon"); }
    private SelenideElement getSignalButtonLocator() { return $("[class*='large signal icon']"); }
    private SelenideElement getNetworkStatusButtonLocator(String status) { return $(String.format("[data-bind*='go%s']", status)); }
    private SelenideElement getUserOfflineButtonLocator() { return $(By.xpath("//div[@onclick=\"return app.core.goOfflineAction()\"]")); }
    private SelenideElement getSyncControlPanelLocator() { return $(By.xpath("//a[contains(text(), 'Sync Control Panel')]")); }
    private SelenideElement getShowRecentInspectionsLocator() { return $("img[data-bind*='arePreviousInspectionsLoading']"); }

    //Actions in the modal

    public SelenideElement getFTQ360Button() {
        return getFTQ360ButtonLocator();
    }

    public SelenideElement getUserMenuButton() {
        return getUserMenuButtonLocator();
    }

    public SelenideElement getSignalButton() {return getSignalButtonLocator(); }

    public SelenideElement getUserOfflineButton() { return getUserOfflineButtonLocator(); }

    public SelenideElement getNetworkStatusButton(String status) {
        return getNetworkStatusButtonLocator(status);
    }

    public SelenideElement getSyncControlPanel() {
        return getSyncControlPanelLocator();
    }

    public SelenideElement getShowRecentInspections() {
        return getShowRecentInspectionsLocator();
    }
}


