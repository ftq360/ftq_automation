package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class VendorTaskSetupPage {

    private SelenideElement getDescriptionFieldLocator() { return $(".has-changes [data-bind*='VendorDescription']"); }
    private SelenideElement getEmaiFieldLocator() { return $(".has-changes [data-bind*='Email']"); }
    private SelenideElement getActiveCheckboxLocator() { return $(".has-changes [data-bind*='IsActive']"); }
    private SelenideElement getCodeFieldLocator() { return  $(".has-changes [data-bind*='VendorCode']"); }

    public SelenideElement getDescriptionField() {
        return getDescriptionFieldLocator();
    }

    public SelenideElement getEmaiField() {
        return getEmaiFieldLocator();
    }

    public SelenideElement getActiveCheckbox() {
        return getActiveCheckboxLocator();
    }

    public SelenideElement getCodeField() {
        return getCodeFieldLocator();
    }
}
