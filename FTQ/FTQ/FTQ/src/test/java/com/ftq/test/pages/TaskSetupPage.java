package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TaskSetupPage {

    private SelenideElement getAddButtonLocator() { return $(".th-plus-icon,.th-plus-icon-ex"); }
    private SelenideElement getCategoryDropDownLocator() { return $(By.xpath("//span[text()='Select Category']/parent::a")); }
    private SelenideElement getTypeDropDownLocator() { return $(By.xpath("//span[text()='Select Category']//ancestor::tr//span[text()='Construction Checklist']/parent::a")); }
    private SelenideElement getCategoryItemLocator(String category) { return $(By.xpath(String.format("(//li[contains(.,'%s')])[1]", category)));}
    private SelenideElement getTypeItemLocator(String type) { return $(By.xpath(String.format("(//li[contains(@class, 'active-result')][text()='%s'])[1]", type))); }
//    private SelenideElement getSubCheckpointTypeItemLocator(String type, String row) { return $(By.xpath(String.format("")))}
    private SelenideElement getTextFieldLocator() { return $("td>textarea.textarea-vertical.error");}
//    private SelenideElement getSubCheckpointTextFieldLocator
    private SelenideElement getSubCheckpointTypeFieldLocator(String row) {return $(By.xpath(String.format("//tr[%s]//td[6]/*[contains(@class, 'container-single')]", row))); }
    private SelenideElement getNewSubCheckpointTypeFieldLocator() {return $(By.xpath(String.format("//tr[@class='has-changes'][2]//*[contains(@class, 'container-single')]"))); }
    private SelenideElement getSaveButtonLocator() { return $("[data-bind*='click: save']");}
    private ElementsCollection getChecklistsListLocator() { return $$("[data-bind*='TaskLabel']"); }
    private SelenideElement getCategoryInputFieldLocator() { return $(By.xpath("//span[text()='Select Category']/../following-sibling::div//input")); }
    private SelenideElement getSendInvitationButtonLocator() { return $("[data-bind*='click: action']"); }
    private SelenideElement getAttachDropDownLocator() { return $(".attach-btn-select"); }
    private SelenideElement getAttachDropDownOptionLocator(String option) { return $(By.xpath(String.format("//div[@class='attach-btn-select']//li[text()='%s']", option))); }
    private SelenideElement getFileInputLocator() { return $(".attach-btn-select ul[style='display: block;'] input[type='file']"); }
    private SelenideElement getDeleteButtonLocator() { return $(".delete-icon"); }
    private SelenideElement getMarkedToDeleteItemLocator() { return $(".marked-for-delete"); }
    private SelenideElement getPlusIconLocator() { return $(".plus-icon"); }
    private SelenideElement getOptionByRowLocator(String option, String row) { return $(By.xpath(String.format("//tr[%s]//a[text()='%s']", row, option))); }
    private SelenideElement getIfElseCheckboxLocator() { return $(".ui.checkbox"); }
//    private SelenideElement getPreconditionDropdownLocator() { return $(".row:nth-of-type(2) .seven.wide.column"); }
    private SelenideElement getPreconditionDropdownLocator() { return $("select[data-bind*='selectedAccessor']"); }
    private SelenideElement getPreconditionValueInDropdownLocator(String precondition) { return $(By.xpath(String.format("//select/option[text()='%s']", precondition))); }
    private SelenideElement getConditionDropdownLocator() { return $(".row:nth-of-type(2) .nine.wide.column"); }
    private SelenideElement getConditionValueInDropdownLocator(String condition) { return $(By.xpath(String.format("//select/option[text()='%s']", condition))); }
    private SelenideElement getIfElseActionBtnLocator(String action)  { return $(By.xpath(String.format("//*[@class='actions']/*[text()='%s']", action))); }

    public SelenideElement getAddButton() {
        return getAddButtonLocator();
    }

    public SelenideElement getCategoryDropDown() {
        return getCategoryDropDownLocator();
    }

    public SelenideElement getPlusIcon(){
        return getPlusIconLocator();
    }

    public SelenideElement getOptionByRow(String option, String row){
        return getOptionByRowLocator(option, row);
    }

    public SelenideElement getTypeDropDown() {
        return getTypeDropDownLocator();
    }

    public SelenideElement getCategoryItem(String category) {
        return getCategoryItemLocator(category);
    }

    public SelenideElement getIfElseCheckbox(){
        return getIfElseCheckboxLocator();
    }

    public SelenideElement getPreconditionDropdown(){
        return getPreconditionDropdownLocator();
    }

    public SelenideElement getPreconditionValueInDropdown(String precondition){
        return getPreconditionValueInDropdownLocator(precondition);
    }

    public SelenideElement getConditionDropdown(){
        return getConditionDropdownLocator();
    }

    public SelenideElement getConditionValueInDropdown(String condition){
        return getConditionValueInDropdownLocator(condition);
    }

    public SelenideElement getIfElseActionBtn(String action) {
        return getIfElseActionBtnLocator(action);
    }

    public SelenideElement getTypeItem(String type) {
        return getTypeItemLocator(type);
    }

//    public SelenideElement getSubCheckpointTypeItem(String type, String row) {
//        return getSubCheckpointTypeItemLocator(type, row);
//    }

    public SelenideElement getTextField() {
        return getTextFieldLocator();
    }

//    public SelenideElement getSubCheckpointTextField() {
//        return getSubCheckpointTextFieldLocator();
//    }
    public SelenideElement getNewSubCheckpointTypeField(String row) {
        return getNewSubCheckpointTypeFieldLocator();
    }
    public SelenideElement getSubCheckpointTypeField(String row) {
        return getSubCheckpointTypeFieldLocator(row);
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public ElementsCollection getCheclistsList() {
        return getChecklistsListLocator();
    }

    public SelenideElement getDeleteButton() { return getDeleteButtonLocator(); }

    public SelenideElement getMarkedToDeleteItem() { return getMarkedToDeleteItemLocator(); }

    public SelenideElement getCategoryInputField() {
        return getCategoryInputFieldLocator();
    }

    public SelenideElement getSendInvitationButton() {
        return getSendInvitationButtonLocator();
    }

    public SelenideElement getAttachDropDown() {
        return getAttachDropDownLocator();
    }

    public SelenideElement getAttachDropDownOption(String option) {
        return getAttachDropDownOptionLocator(option);
    }

    public SelenideElement getFileInput() {
        return getFileInputLocator();
    }
}
