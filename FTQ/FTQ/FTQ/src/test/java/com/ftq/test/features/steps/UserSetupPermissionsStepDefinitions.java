package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.User;
import com.ftq.test.pages.UserSetupPermissionsPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.codeborne.selenide.Selenide.page;

public class UserSetupPermissionsStepDefinitions {

    String username, inspect, editInsp, dataOverrideDelete;

    @Then("^I 'should' see user with following data on User Setup Permissions page:$")
    public void iShouldSeeUserWithFollowingDataOnUserSetupPermissionsPage(List<User> users) {
        UserSetupPermissionsPage userSetupPermissionsPage = page(UserSetupPermissionsPage.class);

        for (User user : users) {
            username = user.getUsername();
            inspect = user.getInspect();
            editInsp = user.getEditInsp();
            dataOverrideDelete = user.getDataOverrideDelete();

            if (StringUtils.isNotEmpty(inspect)) {
                userSetupPermissionsPage.getCreateInspCheckbox(Common.getUniqueValue(username)).shouldBe(Condition.checked);
            }
            if (StringUtils.isNotEmpty(editInsp)) {
                userSetupPermissionsPage.getEditInspCheckbox(Common.getUniqueValue(username)).shouldBe(Condition.checked);
            }
            if (StringUtils.isNotEmpty(dataOverrideDelete)) {
                userSetupPermissionsPage.getDataDeleteOverrideCheckbox(Common.getUniqueValue(username)).shouldBe(Condition.checked);
            }
        }
    }

    @When("^I click on '(.*)' checkbox for '(.*)' user on User Setup Permissions page$")
    public void iClickOnXXXCheckboxForXXXUserOnUserSetupPermissionsPage(String checkbox, String username) {
        UserSetupPermissionsPage userSetupPermissionsPage = page(UserSetupPermissionsPage.class);

        if (checkbox.equalsIgnoreCase ("Data Overwrite or Delete")) {
            userSetupPermissionsPage.getDataDeleteOverrideCheckboxAction(Common.getUniqueValue(username)).click();
        }
    }
}
