package com.ftq.test.objects;

public class TaskSetup {

    private String text;
    private String category;
    private String attachLink;
    private String attachNote;
    private String file;
    private String type;
    private String row;
    private String typeCheckpointRow;
    private String typeCheckpoint;

    public String getText() {
        return text;
    }

    public String getCategory() {
        return category;
    }

    public String getType() {
        return type;
    }

    public String getTypeCheckpointRow() {
        return typeCheckpointRow;
    }
    public String getTypeCheckpoint() {
        return typeCheckpoint;
    }

    public String getRow() {
        return row;
    }

    public String getAttachLink() {
        return attachLink;
    }

    public String getAttachNote() {
        return attachNote;
    }

    public String getFile() {
        return file;
    }
}
