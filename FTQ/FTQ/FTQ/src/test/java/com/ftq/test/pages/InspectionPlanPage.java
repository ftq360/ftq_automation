package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class InspectionPlanPage {

    private SelenideElement getSaveButtonLocator() { return $("span[data-bind*='save']");}
    private SelenideElement getCancelButtonLocator() { return $("span[data-bind*='cancel']");}
    private SelenideElement getCodeColumnLocator() { return $(By.xpath("//th[text()='Code']")); }
    private SelenideElement getLocationColumnLocator() { return $(By.xpath("//th[text()='Location']")); }
    private SelenideElement getInspectorColumnLocator() { return $(By.xpath("//th[text()='Inspector']")); }
    private SelenideElement getShowDropdownLocator() { return $("#filter-preset"); }
    private SelenideElement getProjectDropdownLocator(String project) { return $(By.xpath(String.format("//span[contains(text(),'%s')]", project))); }
    private SelenideElement getJobDropdownLocator() { return $("#job_selector_chzn"); }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public SelenideElement getCancelButton() {
        return getCancelButtonLocator();
    }

    public SelenideElement getCodeColumn() {
        return getCodeColumnLocator();
    }

    public SelenideElement getLocationColumn() {
        return getLocationColumnLocator();
    }

    public SelenideElement getInspectorColumn() {
        return getInspectorColumnLocator();
    }

    public SelenideElement getShowDropdown() {
        return getShowDropdownLocator();
    }

    public SelenideElement getProjectDropdown(String project) {
        return getProjectDropdownLocator(project);
    }

    public SelenideElement getJobDropdown() {
        return getJobDropdownLocator();
    }
}
