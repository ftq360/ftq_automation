package com.ftq.test.features.steps;

import com.ftq.test.pages.PerformancePage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;

public class PerformanceSteps {

    @Then("^I should see (inspection ids|deficiency ids|favorite reports|checklists|projects|users|rps) are loaded$")
    public void iSeeItemsAreLoaded(String item) {
        Common.printCurrentTime();
        PerformancePage performancePage = page(PerformancePage.class);
        switch (item) {
            case "inspection ids":
                performancePage.getInspectionIds().waitUntil(visible, 60000);
                break;
            case "deficiency ids":
                performancePage.getDeficiencyIds().waitUntil(visible, 60000);
                break;
            default:
                performancePage.getRadioButtons().waitUntil(visible, 60000);

        }
    }


}
