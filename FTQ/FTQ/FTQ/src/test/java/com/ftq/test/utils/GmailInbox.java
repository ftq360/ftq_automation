package com.ftq.test.utils;

import com.sun.mail.util.BASE64DecoderStream;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.*;

public class GmailInbox {

    HashMap<String, String> mailData = new HashMap<>();
    Properties props = new Properties();

    public HashMap read() {

        try {
            props.load(new FileInputStream(new File("src/test/resources/smtp.properties")));

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Common.getConfigValue("gmail.address"), Common.getConfigValue("gmail.password"));
                }
            });
            session.setDebug(true);

            Store store = session.getStore("imaps");
            store.connect("imap.gmail.com", Common.getConfigValue("gmail.address"), Common.getConfigValue("gmail.password"));


            Common.pause(2000);
            int messageCount = 0;
            int counter = 0;
            Folder inbox = null;
            while (messageCount == 0 && counter < 40) {
                inbox = store.getFolder("inbox");
                inbox.open(Folder.READ_ONLY);
                messageCount = inbox.getMessageCount();
                counter++;
                Common.pause(10000);
            }


            System.out.println("Total Messages: " + messageCount);

            Message[] messages = inbox.getMessages();
            System.out.println("------------------------------");

            for (int i = 0; i < messageCount; i++) {

                String subject = messages[i].getSubject();

                System.out.println("Mail Subject:- " + subject);

                mailData.put("subject", subject);

                Message message = messages[i];
                System.out.println("---------------------------------");
                writePart(message);
            }

            inbox.close(true);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mailData;
    }

    private void writePart(Part p) throws Exception {

        //check if the content is plain text
        if (p.isMimeType("text/plain")) {
//            System.out.println("This is plain text");
//            System.out.println("---------------------------");
//            System.out.println((String) p.getContent());
            mailData.put("content", (String) p.getContent());
        }
        //check if the content has attachment
        else if (p.isMimeType("multipart/*")) {
//            System.out.println("This is a Multipart");
//            System.out.println("---------------------------");
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++)
                writePart(mp.getBodyPart(i));
        }
        //check if the content is a nested message
        else if (p.isMimeType("message/rfc822")) {
//            System.out.println("This is a Nested Message");
//            System.out.println("---------------------------");
            writePart((Part) p.getContent());
        }
        //check if the content is an inline image
        else if (p.getContentType().contains("image/")) {
//            System.out.println("content type" + p.getContentType());
            File f = new File("image" + new Date().getTime() + ".jpg");
            DataOutputStream output = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(f)));
            BASE64DecoderStream test =
                    (BASE64DecoderStream) p
                            .getContent();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = test.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        }

    }

    public void deleteAllEmails() {
        try {
            props.load(new FileInputStream(new File("src/test/resources/smtp.properties")));

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Common.getConfigValue("gmail.address"), Common.getConfigValue("gmail.password"));
                }
            });
            session.setDebug(true);

            Store store = session.getStore("imaps");
            store.connect("imap.gmail.com", Common.getConfigValue("gmail.address"), Common.getConfigValue("gmail.password"));

            Common.pause(2000);
            Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_WRITE);
            int messageCount = inbox.getMessageCount();

            System.out.println("Total Messages: " + messageCount);

            Message[] messages = inbox.getMessages();
            System.out.println("------------------------------");

            for (int i = 0; i < messageCount; i++) {

                Message message = messages[i];
                message.setFlag(Flags.Flag.DELETED, true);
                Common.pause(2000);
            }

            inbox.close(true);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Common.pause(3000);
    }
}
