package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.*;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.ftq.test.utils.Common.getUniqueValue;

public class SyncControlPanelStepDefinitions {

    @When("^I open inspection with '(.*)' name on sync control panel page$")
    public void iOpenInspectionWithXXXNameOnSyncControlPanelPage(String inspectionName) {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        if(inspectionName.equalsIgnoreCase("guid")) {
            syncControlPanelPage.getInspection(CommonPage.guid).click();
        } else {
            syncControlPanelPage.getInspection(InspectionPage.inspectionId).click();
        }
    }

    @Then("^I '(.*)' see '(.*)' inspection on sync control panel page$")
    public void iConditionSeeXXXInspectionOnSyncControlPanelPage(String condition, String inspectionName) {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        Condition should = getVisibleCondition(condition);

        if(inspectionName.equalsIgnoreCase("guid")) {
            syncControlPanelPage.getInspection(CommonPage.guid).shouldBe(should);
        } else {
            syncControlPanelPage.getInspection(inspectionName).shouldBe(should);
        }
    }

    @Then("^I should wait until inspection with '(.*)' id is synchronized on sync control panel$")
    public void iShouldWaitUntilInspectionWithXXXIdIsSynchronizedOnSycnControlPanel(String inspId) {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        if(inspId.equalsIgnoreCase("inspection id")) {
            syncControlPanelPage.waitUntilInspectionBecomesSync(InspectionPage.inspectionId);
        }
    }

    @When("^I open searched item on sync control panel$")
    public void iOpenSearchedItemOnSyncControlPanel() {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        syncControlPanelPage.getInspection(InspectionPage.inspectionId).click();
    }

    @When("^I select '(.*)' in top dropdown menu on 'Sync Control Panel' page")
    public void iSelectXXXInTopDropDownMenu(String value) {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        syncControlPanelPage.getStatusDropdown().click();
        if(value.equalsIgnoreCase("Ready to Save to Device")) {
            syncControlPanelPage.getStatusDropdownMenu(value).click();
        }
    }

    @When("^I click on '(.*)' button on 'Sync Control Panel' page$")
    public void iClickXXXBtnOnSyncControlPanel(String value) {
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        syncControlPanelPage.getBottomButton(value).click();
    }

    @When("^I click on '(.*)' button in 'Save to Device' modal window$")
    public void iClickOnXXXButtonOnDeleteModal(String buttonName){
        SyncControlPanelPage syncControlPanelPage = page(SyncControlPanelPage.class);
        syncControlPanelPage.getSyncModalButton(buttonName).click();
    }

}
