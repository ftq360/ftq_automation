package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.DeficienciesPage;
import com.ftq.test.pages.UserBasePage;
import com.ftq.test.pages.ViewRecentInspectionsPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.page;

public class DeficienciesStepDefinitions {

    @Then("^I 'should' see '/Deficiencies' page$")
    public void iShouldSeeDeficienciesPage() {
        DeficienciesPage deficienciesPage = page(DeficienciesPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);

        deficienciesPage.getTitle().shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Insp. ID").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Def. ID").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Created").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Fixed").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Status").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("R4R").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Checklist/Notes").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Chkpt./Notes").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Resp. Party/Crew").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Proj./Phase/Equip.").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Insp.").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("R").shouldBe(Condition.visible);
        deficienciesPage.getColumnTitle("Last Chkpt. Insp.").shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        viewRecentInspectionsPage.getForProjectsDropdown().shouldBe(Condition.visible);
    }

    @When("^I click on (deficiency|inspection) id on deficiencies page$")
    public void iClickOnXXXIdOnDeficienciesPage(String idType) {
        DeficienciesPage deficienciesPage = page(DeficienciesPage.class);

        if(idType.equalsIgnoreCase("deficiency")) {
            deficienciesPage.getDeficiencyId().click();
        } else if(idType.equalsIgnoreCase("inspection")) {
            deficienciesPage.getInspectionId().click();
        }
    }
}
