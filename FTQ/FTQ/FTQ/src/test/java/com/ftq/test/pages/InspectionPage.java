package com.ftq.test.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import cucumber.api.java.eo.Se;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.ftq.test.utils.Common.*;
import static java.lang.Math.toIntExact;

public class InspectionPage {

    public static String inspectionId;

    private SelenideElement getSaveButtonLocator() { return $(".ui.primary.button .save.icon"); }
    private SelenideElement getInspectionIdLocator() { return  $("[data-bind*='DataCollRecordID']"); }
    private SelenideElement getNotesFieldLocator() { return $(".ui.fluid.form.ftq-header.input"); }
    private SelenideElement getLocationFieldLocator() { return $("[params*='LocationID'] .ftq-header>[data-bind*='value:selectedText']"); }
    private SelenideElement getNotesInputFieldLocator() { return $(By.xpath("//arta[contains(@params, 'Enter Your Notes')]//textarea")); }
    private SelenideElement getCheckpointLocator(String checkpointName) { return $(By.xpath(String.format("//div[text()='%s']/../..//div[@class='field']", checkpointName))); }
    private SelenideElement getCheckpointInputFieldLocator(String checkpointName) { return $(By.xpath(String.format("//div[text()='%s']/../..//div[@class='field']//div[@class='note-editable']", checkpointName))); }
    private SelenideElement getReadyForReviewCheckboxLocator() { return $(By.xpath("//label[text()='Ready for review']")); }
    private SelenideElement getUploadInputFieldLocator() { return $("input.cm-file"); }
    private SelenideElement getCameraButtonLocator(String checkpoint) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//a", checkpoint))); }
    private String  getCameraButtonLocatorString(String checkpoint) { return String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//a", checkpoint); }
    private SelenideElement getImageSaveButtonLocator() { return $("[title=Save]"); }
    private SelenideElement getCheckboxInCheckpointLocator(String checkpoint, String checkbox) { return $(By.xpath(String.format("//div[text()='%s']/../..//label[contains(text(), '%s')]/preceding-sibling:: input", checkpoint, checkbox))); }
    private SelenideElement getAttachedToCheckpointFileLocator(String checkpointName) { return $(By.xpath(String.format("//div[text()='%s']/../../..//img", checkpointName))); }
    private SelenideElement getDeleteButtonLocator() { return $(".ui.negative.button"); }
    private SelenideElement getInspectionNameLocator() { return $("[data-bind=\"text:TaskTextSupplier\"]"); }
    private SelenideElement getSyncedToServerFieldLocator() { return $(By.xpath("//div[contains(@class,'computer only')]//span[text()='Synced to server']")); }
    private String getCheckboxLocatorPath(String checkpointName) { return String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']", checkpointName); }
    private SelenideElement getSavedToDeviceFieldLocator() { return $(By.xpath("//div[contains(@class,'computer only')]//span[text()='Saved to device']")); }
    private SelenideElement getInspectionProjectNameLocator() { return $("[data-bind*='CommunityText']"); }
    private SelenideElement getInspectionPhaseNameLocator() { return $("[data-bind*='JobText']"); }
    private SelenideElement getInspectionCheckpointNameLocator(String checkpointName) { return $(By.xpath(String.format("//div[contains(@data-bind,'CheckpointText') and .='%s']", checkpointName))); }
    private SelenideElement getResponsiblePartyForCheckpointLocator(String checkpointName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div/following-sibling::div//label[.='Responsible Party']/following-sibling::dropdown//input[contains(@data-bind,'selectedText')]", checkpointName))); }
    private SelenideElement getDynamicButtonLocator(String button) { return $(By.xpath(String.format("//button/span[text()='%s']", button)));}
    private SelenideElement getCheckpointSegmentSectionLocator(String checkpointName) { return $(By.xpath(String.format("//div[@class='ui ftq checkpoint segment']//div[text()='%s']", checkpointName))); }
    private SelenideElement getCheckpointTitleSectionLocator(String checkpointName) { return $(By.xpath(String.format("//*[@class='ui basic segment']//span[text()='%s']", checkpointName))); }
    private SelenideElement getCommunicationAddNewbtnLocator() { return  $(By.xpath("//*[@class='ui right icon']")); }
    private SelenideElement getDropdownUnderInspectionHeadersLocator() { return $(By.xpath("//span/span[text()='Communications']")); }
    private SelenideElement getInspectionIDinCommunitiesSectionLocator() {return $(By.xpath("//*[@class='ui basic table inverted ftq-sub-header container']//*[contains(@data-bind, 'DataCollRecordID')]")); }
    private SelenideElement getActionBtnCommunicationModalWindowLocator(String button) {return $(String.format(".ui.modal.transition.visible.active .ui%s", button)); }
    private SelenideElement getUndoButtonLocator(String checkpointButton) { return $(By.xpath(String.format("//button/span[text()='%s']/parent::button/following-sibling::button", checkpointButton)));}
    private SelenideElement getSignatureOptionLocator() { return $(".cm-uploaddlg__signature"); }
    private SelenideElement getPhotoOptionLocator() { return $(".cm-uploaddlg__capture"); }
    private SelenideElement getDrawingBoardLocator() { return $(".upper-canvas.cm-edit__canvas"); }
    private SelenideElement getCaptureVideoLocator() { return $(".cm-capture__video"); }
    private SelenideElement getStatusDropDownLocator() { return $(By.xpath("//div[@class='pusher']//span[contains(text(),'Status')]/parent::div/following-sibling::div//div/div/input")); }
    private SelenideElement getStatusOverrideCheckboxLocator() { return $(By.xpath("//label[contains(text(),'Override')]")); }
    private SelenideElement getStatusMenuOptionLocator(String option) { return $(By.xpath(String.format("//div[@class='scrolling menu']/div[contains(text(), '%s')]", option))); }
    private SelenideElement getTopBarBtn(String button) { return  $(By.xpath(String.format("//*[contains(@class, 'ui button')][text()='%s']", button))); }
    private SelenideElement getRowStatusFilter(String row_status) { return  $(By.xpath(String.format("//*[@id='status-filter-sidebar']//span[text()='%s']", row_status))); }
    private SelenideElement getChecklistIconLocator(String checklist) { return $(By.xpath(String.format("//div[contains(text(),'%s')]/preceding-sibling::img", checklist)));}
    private SelenideElement getDueDateFieldLocator() { return $("input[data-bind*='DatePunchDue']"); }
    private SelenideElement getTopBorderLocator() { return $(".ui.middle.aligned.condensed.grid.container"); }
    private SelenideElement getTextInsteadSaveButtonLocator(String text) { return $(By.xpath(String.format("//span[contains(text(), '%s')]", text))); }
    private SelenideElement getChecklistReferencesLocator() { return $(By.xpath("//span[text()='Checklist References']")); }
    private SelenideElement getProjectReferencesLocator() { return $(By.xpath("//span[text()='Project References']")); }
    private SelenideElement getPhaseReferencesLocator() { return $(By.xpath("//span[text()='Phase References']")); }
    private ElementsCollection getAttachmentsListLocator() { return $$(By.xpath("//div[@class='att card']//span")); }
    private ElementsCollection getAttachmentsLocator() { return  $$(".att.card"); }
    private SelenideElement getCopyButtonLocator() { return $(By.xpath("//button[contains(., 'Copy')]")); }
    private SelenideElement getLastInspectedLocator() { return $("span[data-bind*='DateInspectionEntry']"); }
    private SelenideElement getResponsiblePartyInputLocator() { return $(By.xpath("//label[text()='Responsible Party']/parent::div/following-sibling::div//div[contains(@class,'ui input')]/input")); }
    private ElementsCollection getDynamicMatrixFieldsListLocator() { return $$(By.xpath("//tbody//td[@class='arta-td']"));}
    private SelenideElement getDynamicMatrixRowsListLocator() { return $("tbody.sortable>tr:nth-of-type(8)");}
    private SelenideElement getActiveTextAreaLocator() { return $(".arta-td textarea"); }
    private ElementsCollection getAttachmentNoteFieldsLocator() { return $$("[data-bind*='textInput: Note']"); }
    private SelenideElement getShowAllButtonLocator() { return $("[data-bind*='text: isExpanded()']"); }
    private ElementsCollection getAttachmentsFromCheckpointLocator(String checkpoint) { return $$(By.xpath(String.format("//div[text()='%s']//ancestor::div[@class='ui ftq checkpoint segment']//a[@class='image']", checkpoint))); }
    private SelenideElement getCheckpointFieldLocator(String checkpoint, String fieldName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[contains(text(),'%s')]", checkpoint, fieldName))); }
    private SelenideElement getCheckpointFieldByNameLocator(String checkpoint, String fieldName) {return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq primary section segment grid']//*[contains(@params, '%s')]", checkpoint, fieldName)));}
    private SelenideElement getCheckpointFieldInputByNameLocator(String checkpoint, String fieldName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq primary section segment grid']//*[contains(@params, '%s')]//div[@class='note-editable']", checkpoint, fieldName))); }
    private SelenideElement getCheckpointFieldInputLocator(String checkpoint, String fieldName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[contains(@params,'%s')]//div[@class='note-editable']", checkpoint, fieldName))); }
    private SelenideElement getFilledCheckpointFieldLocator(String checkpoint, String fieldName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[contains(@params,'%s')]", checkpoint, fieldName))); }
    private SelenideElement getCheckpointFieldDropdownLocator(String checkpoint, String fieldName) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[text()='%s']/following-sibling::dropdown", checkpoint, fieldName))); }
    private SelenideElement getDropdownSearchFieldLocator() { return $(".ui.floating.dropdown.active.visible input[name='search']"); }
    private SelenideElement getRiskFactorInputFieldLocator(String checkpoint, String field) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[text()='%s']/following-sibling::div/input", checkpoint, field))); }
    private SelenideElement getDueDateInputFieldLocator(String checkpoint) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[text()='Due Date']/following-sibling::div//input", checkpoint))); }
    private SelenideElement getNextMonthButtonLocator() { return $("[class='pika-single is-bound'] .pika-next"); }
    private SelenideElement getFirstDayButtonLocator() { return $("[class='pika-single is-bound'] [data-day='1']"); }
    private SelenideElement getDropdownInputLocator(String checkpoint, String dropdown) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[text()='%s']/following-sibling::dropdown//input[@readonly]", checkpoint, dropdown))); }
    private SelenideElement getReadyForReviewCheckboxLocator(String checkpoint) { return $(By.xpath(String.format("//div[text()='%s']/ancestor::div[@class='ui ftq checkpoint segment']//*[text()='Ready for review']/preceding-sibling::input", checkpoint))); }
    private SelenideElement getInspectedByLocator() { return $("span[data-bind='text:CrewTextCustomer']"); }
    private SelenideElement getITPDropdownLocator() { return $("[params='label:app.prefs.value(app.prefs.id.prefITP)'] .dropdown .ftq-header.fluid"); }
    private SelenideElement getITPDropdownOptionLocator(String option) { return $(By.xpath(String.format("//div[text()='[%s]']", option))); }
    private SelenideElement getShareCheckboxLocator() { return $(By.xpath("//input[contains(@data-bind, 'IsShared')]/following-sibling::label")); }
    private SelenideElement getBackToOriginalButtonLocator() { return  $(By.xpath("//a[.='Back to the original']")); }
    private SelenideElement getLocationFieldDropDownLocator() { return $("dropdown[params*='location']"); }
    private SelenideElement getCreatedDateEditableLocator() { return  $(By.xpath("//*[contains(@data-bind, 'DateInspectionEntry')]")); }
    private SelenideElement getDateInCalendarWindowLocator(String date) { return $(By.xpath(String.format("//*[@class='pika-table']//*[@data-day='%s']/button", date))); }
    private SelenideElement getErrorModalHeaderLocator() { return $(".ui.modal.transition.visible.active>.header"); }
    private SelenideElement getErrorModalTextLocator() { return $(".ui.modal.transition.visible.active>.content>span"); }
    private SelenideElement getOkButtonInErrorModalLocator() { return $(".ui.modal.transition.visible.active .ui.ok.primary.button"); }

    public int getTopBorderSize() {
        return toIntExact(executeJavaScript("return document.querySelector('.ui.middle.aligned.condensed.grid.container').clientHeight"));
    }

    public SelenideElement getInspectionId() {
        return getInspectionIdLocator();
    }

    public void waitUntilInspectionIdAppears() {
        int count = 0;
        while(getInspectionIdLocator().text().equals("") && count < 20) {
            Common.pause(1000);
            count++;
        }
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public void setNotestInputField(String text) {
        getNotesFieldLocator().waitUntil(Condition.visible, 60000);
        getNotesFieldLocator().click();
        getNotesInputFieldLocator().clear();
        getNotesFieldLocator().click();
        getNotesInputFieldLocator().waitUntil(Condition.visible, 60000);
        getNotesInputFieldLocator().sendKeys(text);
    }

    public void setAlreadyFilledNotes(String text) {
        getNotesFieldLocator().click();
        getNotesInputFieldLocator().clear();
        getNotesFieldLocator().click();
        getNotesInputFieldLocator().sendKeys(text);
    }

    public SelenideElement getNotesField() {
        return getNotesFieldLocator();
    }

    public SelenideElement getLocationField() {
        return getLocationFieldLocator();
    }

    public void setCheckpoint(String checkpointName, String checkpointValue) {
        scrollToBottomOfElementByXpath(getCheckboxLocatorPath(checkpointName));
        executeJavaScript("$('html, body').animate({scrollBottom: 10000}, 10000);");
        getCheckpointLocator(checkpointName).waitUntil(exist, 20000);
        getCheckpointLocator(checkpointName).click();
        getCheckpointInputFieldLocator(checkpointName).sendKeys(checkpointValue);
    }

    public void clickOnCheckbox(String checkpointName, String checkboxName) {
//        System.out.println(String.format("document.evaluate(\"//div[text()='%s']/../..//label[text()='%s']/preceding-sibling:: input\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);", checkpointName, checkboxName));
        executeJavaScript(String.format("document.evaluate(\"//div[text()='%s']/../..//label[text()='%s']/preceding-sibling:: input\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);", checkpointName, checkboxName));
        executeJavaScript(String.format("document.evaluate(\"//div[text()='%s']/../..//label[text()='%s']/preceding-sibling:: input\", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE).snapshotItem(0).click()", checkpointName, checkboxName));
    }

    public void clickWithoutScrolling(String checkpointName, String checkboxName) {
        executeJavaScript(String.format("document.evaluate(\"//div[text()='%s']/../..//label[contains(text(), '%s')]/preceding-sibling:: input\", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE).snapshotItem(0).click()", checkpointName, checkboxName));
    }

    public SelenideElement getReadyForReviewCheckbox() {
        return getReadyForReviewCheckboxLocator();
    }

    public void attachFileToCheckpoint(String fileName, String checkpoint) {
        getCameraButtonLocator(checkpoint).scrollTo().hover();
        executeJavaScript("window.scrollBy(0,-250)", "");
        getCameraButtonLocator(checkpoint).hover();
        getCameraButtonLocator(checkpoint).click();
        String pathToFile = String.format("src/test/resources/test_files/%s.jpg", fileName);
        getUploadInputFieldLocator().uploadFile(new File(pathToFile));
    }

    public void attachMultipleFilesToCheckpoint(int fileAmount, String fileName, String checkpoint) {
        scrollToTopOfElementByXpath(getCheckboxLocatorPath(checkpoint));
        executeJavaScript("$('.ui.middle.aligned.condensed.grid.container').css('display', 'none');");
        getCameraButtonLocator(checkpoint).click();
        executeJavaScript("$('.ui.middle.aligned.condensed.grid.container').css('display', 'block');");
        executeJavaScript("$('#cm-uploaddlg + div').css('display', 'block');");
        executeJavaScript("$('div#cm-panel').css('display', 'block');");
        executeJavaScript("$('.cm-file').css('display', 'block').css({position:'absolute',top: '200px',left: '100px'})");
       // executeJavaScript("$('html, body').animate({scrollTop: 100}, 100);");
        String basePath = new File(String.format("src/test/resources/test_files/%s.jpg", fileName)).getAbsolutePath();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < fileAmount; i++) {
            sb.append(basePath).append("\n");
        }
        getUploadInputFieldLocator().sendKeys(sb.toString().trim());
        executeJavaScript("$('#cm-uploaddlg + div').css('display', 'none');");
        executeJavaScript("$('div#cm-panel').css('display', 'none');");
        executeJavaScript("$('.cm-file').css('display', 'none').css({position:'absolute',top: '200px',left: '100px'})");
        executeJavaScript("$('html, body').animate({scrollBottom: 500}, 500);");
        pause(2000);
        if(getErrorModalHeaderLocator().isDisplayed() == false){
            getCameraButtonLocator(checkpoint).click();
        }
    }

    public SelenideElement getImageSaveButton() {
        return getImageSaveButtonLocator();
    }

    public SelenideElement getCheckboxInCheckpoint(String checkpoint, String checkbox) {
        return getCheckboxInCheckpointLocator(checkpoint, checkbox);
    }

    public SelenideElement getAttachedToCheckpointFile(String checkpointName) {
        return getAttachedToCheckpointFileLocator(checkpointName);
    }

    public SelenideElement getDeleteButton() {
        return getDeleteButtonLocator();
    }

    public SelenideElement getInspectionName() {
        return getInspectionNameLocator();
    }

    public SelenideElement getSyncedToServerField() {
        return getSyncedToServerFieldLocator();
    }

    public SelenideElement getSavedToDeviceField() {
        return getSavedToDeviceFieldLocator();
    }

    public SelenideElement getInspectionProjectName() {
        return getInspectionProjectNameLocator();
    }

    public SelenideElement getInspectionPhaseName() {
        return getInspectionPhaseNameLocator();
    }

    public SelenideElement getInspectionCheckpointName(String checkpointName) {
        return getInspectionCheckpointNameLocator(checkpointName);
    }

    public SelenideElement getResponsiblePartyForCheckpoint(String checkpointName) {
        return getResponsiblePartyForCheckpointLocator(checkpointName);
    }

    public SelenideElement getDynamicButton(String button) {
        return getDynamicButtonLocator(button);
    }

    public SelenideElement getCheckpointSegmentSection(String checkpointName) {
        return getCheckpointSegmentSectionLocator(checkpointName);
    }

    public SelenideElement getCheckpointTitleSection(String checkpointName) {
        return getCheckpointTitleSectionLocator(checkpointName);
    }

    public SelenideElement getCommunicationAddNewbtn(){
        return getCommunicationAddNewbtnLocator();
    }

    public SelenideElement getActionBtnCommunicationModalWindow(String button) {
        return getActionBtnCommunicationModalWindowLocator(button);
    }

    public SelenideElement getDropdownUnderInspectionHeaders(){
        return getDropdownUnderInspectionHeadersLocator();
    }

    public SelenideElement getInspectionIDinCommunitiesSection(){
//        pause(2000);
//        scrollToTopOfElementByXpath("//*[@class='ui basic table inverted ftq-sub-header container']//*[contains(@data-bind, 'DataCollRecordID')]");
      //  pause(2000);
        scrollToBottomOfElementByXpath("//*[@class='ui basic table inverted ftq-sub-header container']//*[contains(@data-bind, 'DataCollRecordID')]");
        executeJavaScript("$('html, body').animate({scrollTop: 600}, 50);");
        return getInspectionIDinCommunitiesSectionLocator();
    }

    public SelenideElement getUndoButton(String checkpointButton) {
        return getUndoButtonLocator(checkpointButton);
    }

    public SelenideElement getCameraButton(String checkpointName) {
        return getCameraButtonLocator(checkpointName);
    }

    public SelenideElement getSignatureOption() {
        return getSignatureOptionLocator();
    }

    public SelenideElement getDrawingBoard() {
        return getDrawingBoardLocator();
    }

    public void drawSignature() {
        Actions builder = new Actions(getWebDriver());
        Action drawAction = builder.moveToElement(getDrawingBoardLocator(), 135, 15)
                .clickAndHold()
                .moveByOffset(165, 15)
                .moveByOffset(185, 15)
                .build();
        drawAction.perform();
    }

    public SelenideElement getPhotoOption() {
        return getPhotoOptionLocator();
    }

    public SelenideElement getCaptureVideo() {
        return getCaptureVideoLocator();
    }

    public SelenideElement getStatusDropDown() {
        return getStatusDropDownLocator();
    }

    public SelenideElement getStatusOverrideCheckbox() {
        return getStatusOverrideCheckboxLocator();
    }

    public void scrollToStatusDropdown() {
        getResponsiblePartyInputLocator().scrollTo();
    }

    public SelenideElement getTopInspectionBarBtn(String option) {
        return getTopBarBtn(option);
    }

    public SelenideElement getRowStatusInFilter(String row_status) {
        return getRowStatusFilter(row_status);
    }

    public SelenideElement getStatusMenuOption(String option) {
        return getStatusMenuOptionLocator(option);
    }

    public SelenideElement getDueDateField() {
        return getDueDateFieldLocator();
    }

    public SelenideElement getChecklistIcon(String checklist) {
        return getChecklistIconLocator(checklist);
    }

    public SelenideElement getTextInsteadSaveButton(String text) {
        return getTextInsteadSaveButtonLocator(text);
    }

    public SelenideElement getChecklistReferences() {
        return getChecklistReferencesLocator();
    }

    public SelenideElement getProjectReferences() {
        return getProjectReferencesLocator();
    }

    public SelenideElement getPhaseReferences() {
        return getPhaseReferencesLocator();
    }

    public ElementsCollection getAttachmentsList() {
        return getAttachmentsListLocator();
    }

    public ElementsCollection getAttachments() {
        return  getAttachmentsLocator();
    }

    public SelenideElement getCopyButton() { return getCopyButtonLocator(); }

    public SelenideElement getLastInspected() {
        return getLastInspectedLocator();
    }

    public SelenideElement getResponsiblePartyInput() {
        return getResponsiblePartyInputLocator();
    }

    public SelenideElement getInspectedBy() {
        return getInspectedByLocator();
    }

    public String getStatus() {
        return executeJavaScript("return document.evaluate(\"//span[contains(text(),'Status') and @data-element]/ancestor::head-field//input[@type='text']\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value");
    }

    public ElementsCollection getAttachmentNoteFields(){
        return getAttachmentNoteFieldsLocator();
    }

    public void clickShowAllButton() {
        getShowAllButtonLocator().scrollTo();
        executeJavaScript("$(\"[style*='position: fixed; top: 0;']\").css('display', 'none');");
        executeJavaScript("$(\"[style*='position: fixed; bottom: 0;']\").css('display', 'none');");
        getShowAllButtonLocator().click();
        executeJavaScript("$(\"[style*='position: fixed; top: 0;']\").css('display', 'block');");
        executeJavaScript("$(\"[style*='position: fixed; bottom: 0;']\").css('display', 'block');");
    }

    public ElementsCollection getAttachmentsFromCheckpoint(String checkpoint) {
        return getAttachmentsFromCheckpointLocator(checkpoint);
    }

    public ElementsCollection getDynamicMatrixFieldsList() {
        return getDynamicMatrixFieldsListLocator();
    }

    public SelenideElement getDynamicMatrixRowsList() {
        return getDynamicMatrixRowsListLocator();
    }

    public SelenideElement getActiveTextArea() {
        return getActiveTextAreaLocator();
    }

    public String getLocationText() {
        return executeJavaScript("return document.evaluate(\"//label[text()='Location']/parent::div/following-sibling::div//div[contains(@class,'ui input')]/input\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value");
    }

    public SelenideElement getCheckpointField(String checkpoint, String fieldName) {
        return getCheckpointFieldLocator(checkpoint, fieldName);
    }

    public SelenideElement getCheckpointFieldByName(String checkpoint, String fieldName) {
        return getCheckpointFieldByNameLocator(checkpoint, fieldName);
    }

    public SelenideElement getCheckpointFieldInputByName(String checkpoint, String fieldName) {
        return getCheckpointFieldInputByNameLocator(checkpoint, fieldName);
    }

    public SelenideElement getCheckpointFieldInput(String checkpoint, String fieldName) {
        return getCheckpointFieldInputLocator(checkpoint, fieldName);
    }

    public void setCheckpointDropdown(String checkpoint, String dropdown, String value) {
        pause(2000);
        getCheckpointFieldDropdownLocator(checkpoint, dropdown).waitUntil(Condition.visible, 20000);
        getCheckpointFieldDropdownLocator(checkpoint, dropdown).click();
        getDropdownSearchFieldLocator().waitUntil(Condition.exist, 20000);
        getDropdownSearchFieldLocator().setValue(value).pressEnter();
    }

    public void setLocationDropdown(String location) {
        getLocationFieldDropDownLocator().click();
        getDropdownSearchFieldLocator().setValue(location).pressEnter();
    }

    public SelenideElement getRiskFactorInputField(String checkpoint, String field) {
        return getRiskFactorInputFieldLocator(checkpoint, field);
    }

    public void setNextMonthFirstDayDate(String checkpoint) {
        getDueDateInputFieldLocator(checkpoint).click();
        getNextMonthButtonLocator().waitUntil(visible, 20000);
        getNextMonthButtonLocator().click();
        pause(2000);
        getFirstDayButtonLocator().click();
    }

    public SelenideElement getDueDateInputField(String checkpoint) {
        return getDueDateInputFieldLocator(checkpoint);
    }

    public SelenideElement getDropdownInputField(String checkpoint, String dropdown) {
        return getDropdownInputLocator(checkpoint, dropdown);
    }

    public SelenideElement getFilledCheckpointField(String checkpoint, String fieldName) {
        return getFilledCheckpointFieldLocator(checkpoint, fieldName);
    }

    public SelenideElement getReadyForReviewCheckbox(String checkpoint) {
        return getReadyForReviewCheckboxLocator(checkpoint);
    }

    public SelenideElement getITPDropdown() {
        return getITPDropdownLocator();
    }

    public SelenideElement getITPDropdownOption(String option) {
        return getITPDropdownOptionLocator(option);
    }

    public SelenideElement getShareCheckbox() {
        return getShareCheckboxLocator();
    }

    public SelenideElement getBackToOriginalButton() {
        return getBackToOriginalButtonLocator();
    }

    public SelenideElement getCreatedDateEditable(){
        return getCreatedDateEditableLocator();
    }

    public SelenideElement getDateInCalendarWindow(String date){
        return getDateInCalendarWindowLocator(date);
    }

    public SelenideElement getErrorModalHeader() { return getErrorModalHeaderLocator(); }

    public SelenideElement getErrorModalText() { return getErrorModalTextLocator(); }

    public SelenideElement getOkButtonInErrorModal() { return getOkButtonInErrorModalLocator(); }
}