package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ITPSchedulerPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ITPSchedulerStepDefinitions {

    @Then("^I 'should' see '/Checklists/ItpScheduler' page$")
    public void iShouldSeeItpListPage() {
        ITPSchedulerPage iTPSchedulerPage = page(ITPSchedulerPage.class);

        iTPSchedulerPage.getTitle().shouldBe(Condition.visible);
        iTPSchedulerPage.getAssignBtn().shouldBe(Condition.visible);
        iTPSchedulerPage.getStatusDropdown().shouldBe(Condition.visible);
        iTPSchedulerPage.getProjectDropdown().shouldBe(Condition.visible);
        iTPSchedulerPage.getSearchField().shouldBe(Condition.visible);
        iTPSchedulerPage.getMainTableHeader("ITP", "text").shouldBe(Condition.visible);
        iTPSchedulerPage.getMainTableHeader("% Done", "complete").shouldBe(Condition.visible);
        iTPSchedulerPage.getMainTableHeader("Inspector", "inspector").shouldBe(Condition.visible);
        iTPSchedulerPage.getMainTableHeader("Start date", "start_date").shouldBe(Condition.visible);
        iTPSchedulerPage.getMainTableHeader("Due date", "end_date").shouldBe(Condition.visible);
        iTPSchedulerPage.getTableWithData().shouldBe(Condition.visible);
    }
}
