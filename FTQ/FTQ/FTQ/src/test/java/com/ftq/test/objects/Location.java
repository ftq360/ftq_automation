package com.ftq.test.objects;

public class Location {

    private String code;
    private String locationName;

    public String getLocationCode() { return code; }

    public String getLocationName() { return locationName; }
}
