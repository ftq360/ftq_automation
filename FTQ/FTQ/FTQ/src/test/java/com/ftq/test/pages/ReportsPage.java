package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class ReportsPage {

    private SelenideElement getCancelButtonLocator() { return $("[data-bind*='click: cancel']"); }
    private SelenideElement getSaveButtonLocator() { return $("[data-bind*='click: save']"); }
    private SelenideElement getDateRangeLocator(String dateRange) { return $(By.xpath(String.format("//span[contains(text(),'%s')]/preceding-sibling::span//input", dateRange))); }
    private SelenideElement getSelectButtonLocator() { return $(".big-default-btn"); }
    private SelenideElement getRunReportButtonLocator() { return $("[data-bind*='runReport']"); }
    private SelenideElement getReportLocator(String report) { return $(By.xpath(String.format("//span[text()='%s']", report))); }
    private SelenideElement getReportRadioButtonLocator(String report) { return $(By.xpath(String.format("//span[text()='%s']/parent::td/preceding-sibling::td//input", report))); }

    public SelenideElement getCancelButton() {
        return getCancelButtonLocator();
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public SelenideElement getDateRange(String dateRange) {
        return getDateRangeLocator(dateRange);
    }

    public SelenideElement getSelectButton() {
        return getSelectButtonLocator();
    }

    public SelenideElement getRunReportButton() {
        return getRunReportButtonLocator();
    }

    public void clickRunReportButton() {
        switchTo().defaultContent();
        switchTo().frame("report-frame");
        getRunReportButtonLocator().click();
    }

    public SelenideElement getReport(String report) {
        return getReportLocator(report);
    }

    public SelenideElement getReportRadioButton(String report) {
        return getReportRadioButtonLocator(report);
    }
}
