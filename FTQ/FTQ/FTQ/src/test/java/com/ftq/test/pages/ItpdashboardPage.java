package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ItpdashboardPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[contains(.,'ITP Plan Completion Dashboard')]")); }
    private SelenideElement getChartTitleLocator(String chartName) { return $(By.xpath(String.format("//h3[text()='%s']", chartName))); }
    private SelenideElement getProjectDropdownLocator() { return $(By.xpath("//header/div[2]/*[contains(@id, 'project_selector')]")); }
    private SelenideElement getDropdownSearchFieldLocator() { return $(By.xpath("//*[contains(@id, 'project_selector')]/*[contains(@class, 'drop')]//input"));}
    private SelenideElement getProjectInDropdownLocator(String project) {return $(By.xpath(String.format("//*[contains(@class, 'active-result')]/*[contains(text(), '%s')]", project))); }
    private SelenideElement getPlanArrowLocator(String plan) { return $(By.xpath(String.format("//*[contains(text(), '%s')]/ancestor::li//i/img", plan))); }
    private SelenideElement getPlanStatusLocator(String plan, String status) { return $(By.xpath(String.format("//*[contains(text(), '%s')]/ancestor::li//span[text()='%s']", plan, status))); }
    private SelenideElement getPlanIconLocator(String plan, String icon) {return $(By.xpath(String.format("//*[contains(text(), '%s')]/ancestor::li//img[contains(@src, '%s')]", plan, icon))); }
    private SelenideElement getPlanInspectionStatusLocator(String plan, String inspectionStatus) {return $(By.xpath(String.format("//*[contains(text(), '%s')]/ancestor::li//*[@class='plain-multiline-editor'][text()='%s']", plan, inspectionStatus))); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getChartTitle(String chartName) {
        return getChartTitleLocator(chartName);
    }

    public SelenideElement getProjectDropdown() {
        return getProjectDropdownLocator();
    }

    public SelenideElement getDropdownSearchField() {
        return getDropdownSearchFieldLocator();
    }

    public SelenideElement getProjectInDropdown(String project) {
        return getProjectInDropdownLocator(project);
    }

    public SelenideElement getPlanArrow(String plan) {
        return getPlanArrowLocator(plan);
    }

    public SelenideElement getPlanStatus(String plan, String status) {
        return getPlanStatusLocator(plan, status);
    }

    public SelenideElement getPlanIcon(String plan, String icon) {
        return getPlanIconLocator(plan, icon);
    }

    public SelenideElement getPlanInspectionStatus(String plan, String inspectionStatus) {
        return getPlanInspectionStatusLocator(plan, inspectionStatus);
    }
}
