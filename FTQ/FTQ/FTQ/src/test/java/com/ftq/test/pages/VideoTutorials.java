package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class VideoTutorials {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[contains(text(),'Videos')]"));}

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

}
