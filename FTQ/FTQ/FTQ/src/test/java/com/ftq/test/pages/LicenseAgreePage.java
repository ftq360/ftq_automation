package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LicenseAgreePage {

    //Elements on the page

    private SelenideElement clickCheckboxButtonLocator() { return $("#chkIAgree"); }
    private SelenideElement getAcceptButtonLocator() { return $("#btnProceed");}

    //Actions on the page

    public SelenideElement clickCheckboxButton() { return clickCheckboxButtonLocator();}
    public SelenideElement getAcceptButton() { return getAcceptButtonLocator();}

}
