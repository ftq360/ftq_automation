package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.*;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class InspectionPlanStepDefinition {

    @Then("^I 'should' see '/Checklists/InspectionPlan' page$")
    public void iShouldSeeChecklistsInspectionPlanPage() {
        InspectionPlanPage inspectionPlanPage = page(InspectionPlanPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        UserBasePage userBasePage = page(UserBasePage.class);

        inspectionPlanPage.getCancelButton().shouldBe(Condition.visible);
        inspectionPlanPage.getSaveButton().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Create").shouldBe(Condition.visible);
        inspectionPlanPage.getCodeColumn().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Seq.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Due").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Phase").shouldBe(Condition.visible);
        inspectionPlanPage.getLocationColumn().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Checklist").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Responsible Party").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Equipment").shouldBe(Condition.visible);
        inspectionPlanPage.getInspectorColumn().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. Type").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Min").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Notes").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("% Complete").shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        inspectionPlanPage.getShowDropdown().shouldBe(Condition.visible);
        inspectionPlanPage.getProjectDropdown("Clean Project").shouldBe(Condition.visible);
        inspectionPlanPage.getJobDropdown().shouldBe(Condition.visible);
    }
}
