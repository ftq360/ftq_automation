package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.Report;
import com.ftq.test.objects.SendReport;
import com.ftq.test.pages.CommunitySetupPage;
import com.ftq.test.pages.ReportsHistoryPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Keys;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.getUniqueValue;

public class ReportsHistoryStepDefinitions {

    private String reportName, requestDate, dateRange, to, format;

    @When("^I click on Refresh button until first report becomes Completed on reports history page$")
    public void iClickOnRefreshButtonUntilFirstReportBecomesCompletedOnReportsHistoryPage() {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);
        reportsHistoryPage.clickRefreshButtonUntilReportStatusBecomesCompleted();
    }

    @Then("^I 'should' see '/Reports/History' page$")
    public void iShouldSeeReportsHistoryPage() {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        reportsHistoryPage.getTitle().shouldBe(Condition.visible);
        reportsHistoryPage.getRefreshButton().shouldBe(Condition.visible);
    }

    @Then("^I should see following report on reports history page:$")
    public void iShouldSeeFollowingReportOnReportsHistoryPage(List<Report> reports) {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        for(Report report : reports) {
            reportName = report.getReportName();
            requestDate = report.getRequestDate();
            dateRange = report.getDateRange();

            if(StringUtils.isNotEmpty(reportName)) {
                reportsHistoryPage.getReportName(getUniqueValue(reportName)).shouldBe(Condition.visible);
            }
            if(StringUtils.isNotEmpty(requestDate)) {
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
                LocalDateTime now = LocalDateTime.now();
                reportsHistoryPage.getRequestDate(dtf.format(now)).shouldBe(Condition.visible);
            }
            if(StringUtils.isNotEmpty(dateRange)) {
                reportsHistoryPage.getRequestDate(dateRange).waitUntil(Condition.visible, 20000);
                reportsHistoryPage.getRequestDate(dateRange).shouldBe(Condition.visible);
            }
        }
    }

    @When("^I click on '(.*)' button on reports history page$")
    public void iClickOnXXXButtonForFollowingReportOnReportsHistoryPage(String button) {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);
        if(button.equalsIgnoreCase("email")) {
            reportsHistoryPage.getEmailButton().click();
        }
    }

    @When("^I clear '(.*)' field on send report modal$")
    public void iClearXXXFieldOnSendReportModal(String field) {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        if(field.equalsIgnoreCase("to:")) {
            reportsHistoryPage.getSendButton().shouldBe(Condition.visible);
            for(int i = 0; i < reportsHistoryPage.getClearButtonsList().size(); i++) {
                reportsHistoryPage.getClearButtonsList().get(i).click();
            }
        }
    }

    @When("^I fill send report modal with following data:$")
    public void iFillSendReportModalWithFollowingData(List<SendReport> sendReports) {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        for(SendReport sendReport : sendReports) {
            to = sendReport.getTo();
            format = sendReport.getFormat();

            if(StringUtils.isNotEmpty(to)) {
                reportsHistoryPage.getEmailField("To:").setValue(Common.getConfigValue("gmail.address"));
                reportsHistoryPage.getEmailField("To:").sendKeys(Keys.ENTER);
            }
            if(StringUtils.isNotEmpty(format)) {
                reportsHistoryPage.getFormatRadioButton(format.toLowerCase());
            }
        }
    }

    @When("^I click on '(.*)' button on send report modal$")
    public void iClickOnXXXButtonOnSendReportModal(String button) {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        if(button.equalsIgnoreCase("send")) {
            reportsHistoryPage.getSendButton().click();
        }
    }

    @When("^I delete all reports on reports history page$")
    public void iDeleteAllReportsOnReportsHistoryPage() {
        ReportsHistoryPage reportsHistoryPage = page(ReportsHistoryPage.class);

        for(int i = reportsHistoryPage.getReportDeleteButtonsList().size()-1; i >= 0; i--) {
            Common.pause(1000);
            if(reportsHistoryPage.getReportDeleteButtonsList().get(i).isDisplayed()) {
                reportsHistoryPage.getReportDeleteButtonsList().get(i).click();
                reportsHistoryPage.getYesModalButton().click();
            }
        }
    }
}