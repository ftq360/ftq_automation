package com.ftq.test.objects;

public class File {

    private String fileName;
    private String checkpoint;

    public String getFileName() {
        return fileName;
    }

    public String getCheckpoint() {
        return checkpoint;
    }
}
