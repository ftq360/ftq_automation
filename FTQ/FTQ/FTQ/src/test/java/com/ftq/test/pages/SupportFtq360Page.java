package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SupportFtq360Page {

    private SelenideElement getBlockTitleLocator(String title) { return $(By.xpath(String.format("//h4[text()='%s']", title)));}
    private SelenideElement getHeaderLinkLocator(String linkName) { return $(By.xpath(String.format("//a[text()='%s']", linkName))); }
    private SelenideElement getSearchFieldLocator() { return $("#query"); }
    private SelenideElement getLogoLocator() { return $("[alt='Logo']"); }

    public SelenideElement getBlockTitle(String title) {
        return getBlockTitleLocator(title);
    }

    public SelenideElement getHeaderLink(String linkName) {
        return getHeaderLinkLocator(linkName);
    }

    public SelenideElement getSearchField() {
        return getSearchFieldLocator();
    }

    public SelenideElement getLogo() {
        return getLogoLocator();
    }
}
