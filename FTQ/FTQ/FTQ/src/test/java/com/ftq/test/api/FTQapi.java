package com.ftq.test.api;

import com.ftq.test.utils.Common;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class FTQapi extends RequestSender{

    public static void getLoginToken() {

        JsonObject userObject = new JsonObject();
        userObject.addProperty("txtUsername", Common.getConfigValue("username"));
        userObject.addProperty("txtPassword", Common.getConfigValue("password"));

        System.out.println("Getting Login Token");
        HttpResponse httpResponse = sendPostSetToken("/Account/Login", userObject.toString());
        Header[] headers = httpResponse.getAllHeaders();

        for (Header header : headers) {
            if (header.getName().equalsIgnoreCase("Set-Cookie")) {
                String tmp = header.getValue();
                __RequestVerificationToken = tmp.substring(tmp.indexOf("=") + 1, tmp.indexOf(";"));
            }
        }
    }

    public FTQapi createChecklist() {

        JsonObject rowsObject = new JsonObject();
        rowsObject.addProperty("toggleColumn", "null");
        rowsObject.addProperty("AttachmentBundleID", "null");
        rowsObject.addProperty("CommunityID", "null");
        rowsObject.addProperty("preview", "null");
        rowsObject.addProperty("TaskCode", "CT57");
        rowsObject.addProperty("TaskQASequence", 57);
        rowsObject.addProperty("TradeID", 104250);
        rowsObject.addProperty("TaskTypeID", 1);
        rowsObject.addProperty("TaskLabel", "qaqa123");
        rowsObject.addProperty("Attach", "null");
        rowsObject.addProperty("Active", 1);
        rowsObject.addProperty("UseCommonCheckpoints", 1);
        rowsObject.addProperty("undefined", "null");
        rowsObject.addProperty("newRowFlag", 2);
        rowsObject.addProperty("deletedFlag", false);
        rowsObject.addProperty("isFreshFlag", false);
        rowsObject.addProperty("isExpanded", false);
        rowsObject.addProperty("toggleIcon", "/Content/images/Expand-Right-Grid-Checklist13x13.png");
        rowsObject.addProperty("hasChildren", false);
        rowsObject.addProperty("hasAttachments", false);
        rowsObject.add("errors", new JsonArray());
        rowsObject.add("children", new JsonArray());

        JsonArray rowsArray = new JsonArray();
        rowsArray.add(rowsObject);

        JsonObject checklistObject = new JsonObject();
        checklistObject.add("rows", rowsArray);

        System.out.println("Creating Checklist: " + checklistObject.toString());
        String response = sendPost("/Settings/SaveTasks", checklistObject.toString());
        System.out.println("Creating Checklist response: " + response);

        return this;
    }
}
