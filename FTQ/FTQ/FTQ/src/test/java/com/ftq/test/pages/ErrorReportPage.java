package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ErrorReportPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//div[contains(text(), 'Please contact FTQ360 Support for assistance.')]")); }
    private SelenideElement getDateInputLocator() { return $("[placeholder='Enter date/time here']"); }
    private SelenideElement getNoteInputLocator() { return $("[placeholder='Enter note here']"); }
    private SelenideElement getSendReportButtonLocator() { return $(".ui.primary.button"); }
    private SelenideElement getBackButtonLocator() { return $("[data-bind*='back']"); }
    private SelenideElement getDeleteAllDataButtonLocator() { return $(".ui.red.button"); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getDateInput() {
        return getDateInputLocator();
    }

    public SelenideElement getNoteInput() {
        return getNoteInputLocator();
    }

    public SelenideElement getSendReportButton() {
        return getSendReportButtonLocator();
    }

    public SelenideElement getBackButton() {
        return getBackButtonLocator();
    }

    public SelenideElement getDeleteAllDataButton() {
        return getDeleteAllDataButtonLocator();
    }


}
