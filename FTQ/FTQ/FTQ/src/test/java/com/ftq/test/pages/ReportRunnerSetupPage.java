package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ReportRunnerSetupPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Activate Reports']")); }
    private SelenideElement getRunOnScheduleColumnLocator() { return $(By.xpath("//th[text()='Run on Schedule']")); }
    private SelenideElement getScheduleColumnLocator() { return $(By.xpath("//th[text()='Schedule']")); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getRunOnScheduleColumn() {
        return getRunOnScheduleColumnLocator();
    }

    public SelenideElement getScheduleColumn() {
        return getScheduleColumnLocator();
    }

}
