package com.ftq.test.menu;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ProcessMenu {

    //Elements in the modal

    private SelenideElement getProcessMenuItemLocator(String menuItem) { return $(By.xpath(String.format("//div[@class='sidenav-item-title' and text()='%s']/..", menuItem))); }
    private SelenideElement getTopDropdownLocator() { return $(By.cssSelector(".header-item #header-filter")); }
    private SelenideElement getTopDropDownListLocator(String status) {return  $(By.xpath(String.format("//*[@class='funcnav']//a[text()='%s']", status))); }

    //Actions in the modal

    public SelenideElement getProcessMenuItem(String menuItem) {
        return getProcessMenuItemLocator(menuItem);
    }

    public SelenideElement getTopDropdown() {
        return getTopDropdownLocator();
    }

    public SelenideElement getTopDropDownList(String status) {
        return getTopDropDownListLocator(status);
    }
}


