package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.UserLicenseAgreementPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class UserLicenseAgreementStepDefinitions {

    @Then("^I 'should' see '/Library/UserMgmt/LicenseView.aspx' page$")
    public void iShouldSeeLicensePage() {
        UserLicenseAgreementPage userLicenseAgreementPage = page(UserLicenseAgreementPage.class);

        userLicenseAgreementPage.getTitle().shouldBe(Condition.visible);
        userLicenseAgreementPage.getLicenseAgreementField().shouldBe(Condition.visible);
    }
}
