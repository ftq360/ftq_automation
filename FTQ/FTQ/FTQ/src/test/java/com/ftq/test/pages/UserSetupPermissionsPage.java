package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UserSetupPermissionsPage {

    private SelenideElement getEditInspCheckboxLocator(String username) { return $(By.xpath(String.format("//span[text()='%s']/parent::td/following-sibling::td/input[contains(@id,'parent.Edit')]", username))); }
    private SelenideElement getCreateInspCheckboxLocator(String username) { return $(By.xpath(String.format("//span[text()='%s']/parent::td/following-sibling::td/input[contains(@id,'parent.Create')]", username))); }
    private SelenideElement getDataDeleteOverrideCheckboxLocator(String username) { return $(By.xpath(String.format("//span[text()='%s']/parent::td/following-sibling::td/input[contains(@id,'parent.R13')]/ancestor::td/label", username))); }
    private SelenideElement getDataDeleteOverrideCheckboxActionLocator(String username) { return $(By.xpath(String.format("//span[text()='%s']/parent::td/following-sibling::td/input[contains(@id,'parent.R13')]/ancestor::td/label", username))); }


    public SelenideElement getCreateInspCheckbox(String username) {
        return getCreateInspCheckboxLocator(username);
    }

    public SelenideElement getEditInspCheckbox(String username) {
        return getEditInspCheckboxLocator(username);
    }

    public SelenideElement getDataDeleteOverrideCheckbox(String username) {
        return getDataDeleteOverrideCheckboxLocator(username);
    }

    public SelenideElement getDataDeleteOverrideCheckboxAction(String username) {
        return getDataDeleteOverrideCheckboxActionLocator(username);
    }
}
