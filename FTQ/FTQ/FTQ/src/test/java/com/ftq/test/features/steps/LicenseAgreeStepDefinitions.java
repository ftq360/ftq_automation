package com.ftq.test.features.steps;

import com.ftq.test.pages.LicenseAgreePage;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Selenide.page;

public class LicenseAgreeStepDefinitions {

    @When("^I should check checkbox on license agree page$")
    public void iAgreeToTheLicenseAgreementTermsAndConditionsOnLicense(){
        LicenseAgreePage licenseAgreePage = page(LicenseAgreePage.class);
        licenseAgreePage.clickCheckboxButton().click();

    }

    @When("^I click on '(.*)' button on license agree page$")
    public void iClickOnXXXButtonOnLicenseAgreePage(String buttonName){
        LicenseAgreePage licenseAgreePage = page(LicenseAgreePage.class);

        if (buttonName.equalsIgnoreCase("Accept")) {
            licenseAgreePage.getAcceptButton().click();
        }
    }
}
