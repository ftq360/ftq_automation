package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BaseSetupPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Setup']")); }
    private SelenideElement getButtonLocator(String buttonName) { return $(By.xpath(String.format("//span[text()='%s']", buttonName))); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getButton(String buttonName) {
        return getButtonLocator(buttonName);
    }
}
