package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.OpenItemDashboardPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class OpenItemDashboardStepDefinition {

    @Then("^I 'should' see '/Dashboards/OpenItemDashboard' page$")
    public void iShouldSeeChecklistsOpenItemDashboardPage() {
        OpenItemDashboardPage openItemDashboardPage = page(OpenItemDashboardPage.class);

        openItemDashboardPage.getTitle().shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Project").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Status").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Primary Responsible Party/Crew").shouldBe(Condition.visible);

        openItemDashboardPage.getChartTitle("Deficiency Responsible Party/Crew").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Inspector").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Checklist").shouldBe(Condition.visible);

        openItemDashboardPage.getChartTitle("Category of Deficiency").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Priority").shouldBe(Condition.visible);
        openItemDashboardPage.getChartTitle("Stats").shouldBe(Condition.visible);
    }
}
