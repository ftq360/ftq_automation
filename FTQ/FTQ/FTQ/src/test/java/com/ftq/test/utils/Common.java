package com.ftq.test.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.executeJavaScript;

public class Common {

    private static String uniqueValue;

    public static void pause(int msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get value from config file based on key
     *
     * @param key Key from config file
     * @return String value from config
     */
    public static String getConfigValue(String key) {

        ResourceBundle config = ResourceBundle.getBundle("settings");
        return config.getString(key);
    }

    /**
     * Generates random value for test data.
     *
     * @return String contains dash and seconds
     */
    public static String generateRandomValue() {
        return String.format("-%s", RandomStringUtils.randomAlphanumeric(4).toLowerCase());
    }

    /**
     * Set unic value for test run.
     *
     * @param value value to set
     */
    public static void setUniqueValue(String value) {
        uniqueValue = value.replace("-", "");
    }

    /**
     * Get unique value for test run.
     *
     * @param value value to convert to unique value
     * @return String converted unique value
     */
    public static String getUniqueValue(String value) {

        if (value.equalsIgnoreCase("uniqueValue")) {
            value = uniqueValue;
        } else if (value.contains("pre-setup")) {
            value = getPreSetupValue(value);
        } else {
            value = value + uniqueValue;
        }
        return value;
    }

    public static String getUniqueEmail(String email) {
        String[] parsedEmail = email.split("@");
        return getUniqueValue(parsedEmail[0]) + "@" + parsedEmail[1];
    }

    /**
     * Generate unique value base on original content of the value
     *
     * @param value value to convert
     * @return String converted value with unique part
     */
    public static String getTextFieldValue(String value) {
        if (value.contains("@")) {
            String[] email = value.split("@");
            value = getUniqueValue(email[0]) + "@" + email[1];
        } else {
            value = getUniqueValue(value);
        }
        return value;
    }

    /**
     * Get pre setup value for static data.
     *
     * @param object value to convert
     * @return String converted value
     */
    public static String getPreSetupValue(String object) {
        return object.replace("pre-setup", "").trim();
    }

    public static void scrollToBottomOfElementByXpath(String locator) {
        executeJavaScript(String.format("document.evaluate(\"%s\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);\n", locator));
    }

    public static void scrollToTopOfElementByXpath(String locator) {
        executeJavaScript(String.format("document.evaluate(\"%s\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(false);\n", locator));
    }

    public static void refreshPage() {
        executeJavaScript("window.location.reload();");
    }

    public static void printCurrentTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
    }

    public static boolean waitPageLoading() {
        String state;
        boolean state2 = false;
        for (int i = 0; i < 10; i++) {
            pause(1000);
            state = executeJavaScript("return document.readyState;");
            if (state.equals("complete")) {
                state2 = true;
                break;
            }
        }
        return state2;
    }

    public static boolean waitVMLoading() {
        boolean jsResult;
        int waitStep = 500;
        int waitTimeout = 10000;
        for(int elapsed =0;elapsed<=waitTimeout;elapsed+=waitStep){
            jsResult = executeJavaScript("return typeof(app)==='object'&&typeof(app.vm)==='object'&&app.vm.isLoading();");
            if(jsResult){
                return true;
            }
            pause(waitStep);
        }
        return false;
    }
}
