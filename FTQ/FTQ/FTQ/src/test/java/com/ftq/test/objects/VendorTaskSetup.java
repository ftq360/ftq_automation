package com.ftq.test.objects;

public class VendorTaskSetup {

    private String description;
    private String email;
    private boolean isActive;

    public String getDescription() {
        return description;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return isActive;
    }
}
