package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CommonPage {

    public static String url, guid;
    public static ArrayList<String> guids = new ArrayList<>();

    private SelenideElement getDashboardHeaderLocator(String headerName) { return $(By.xpath(String.format("//h1[.='%s']", headerName))); }
    private SelenideElement getSectionNameLocator(String sectionName) { return $(By.xpath(String.format("//h3[.='%s']", sectionName))); }
    private ElementsCollection getRadioButtonsListLocator() { return $$("[name='rowselect']"); }
    private SelenideElement getColumnTitleLocator(String title) { return $(By.xpath(String.format("//th[contains(.,'%s') and contains(@style,'width')]", title)));}
    private SelenideElement getSpecificColumnTitleLocator(String title) { return $(By.xpath(String.format("//th[contains(.,'%s')]", title)));}
    private SelenideElement getLegendTitleLocator(String legendTitle) { return $(By.xpath(String.format("//legend[contains(text(),'%s')]", legendTitle)));}
    private SelenideElement getButtonLocator(String button) { return $(By.xpath(String.format("(//*[text()='%s' and contains(@class,'btn')])[last()]", button))); }
    private SelenideElement getRefreshButtonLocator() { return $(".refresh.link.icon"); }
    private ElementsCollection getTitlesLocator() { return $$(".item-text"); }
    private SelenideElement getGlobalNotificationLocator() { return $("div.toast-message"); }
    private SelenideElement getPlanModeCheckboxLocator() { return $("input[name='example']"); }
    private SelenideElement getActionModalButtonLocator(String buttonName) { return $(By.xpath((String.format("//div[text()='%s']", buttonName)))); }
    private SelenideElement getLoadingSpinnerLocator() {return $(By.cssSelector("#wait-mask-popup .app-waitmask")); }

    public SelenideElement getDashboardHeader(String headerName) {
        return getDashboardHeaderLocator(headerName);
    }

    public SelenideElement getSectionName(String sectionName) {
        return getSectionNameLocator(sectionName);
    }

    public ElementsCollection getRadioButtonsList() {
        return getRadioButtonsListLocator();
    }

    public SelenideElement getColumnTitle(String title) {
        return getColumnTitleLocator(title);
    }

    public SelenideElement getLegendTitle(String legendTitle) {
        return getLegendTitleLocator(legendTitle);
    }

    public SelenideElement getButton(String button) {
        return getButtonLocator(button);
    }

    public SelenideElement getSpecificColumnTitle(String title) {
        return getSpecificColumnTitleLocator(title);
    }

    public SelenideElement getRefreshButton() {
        return getRefreshButtonLocator();
    }

    public ElementsCollection getTitles() {
        return getTitlesLocator();
    }

    public SelenideElement getGlobalNotification() {
        return getGlobalNotificationLocator();
    }

    public SelenideElement getActionModalButton(String buttonName) { return getActionModalButtonLocator(buttonName); }

    public SelenideElement getLoadingSpinner() {
        return getLoadingSpinnerLocator();
    }
}
