package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AccountSetupPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h2[text()='Account details']")); }
    private SelenideElement getProcessMenuLocator() { return $(".sidenav"); }
    private SelenideElement getAccountDetailsTableLocator() { return $(".primary-table.setup-table"); }
    private SelenideElement getSaveButtonLocator() { return $(".big-default-btn"); }
    private SelenideElement getCancelButtonLocator() { return $(".big-common-btn"); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getProcessMenu() {
        return getProcessMenuLocator();
    }

    public SelenideElement getAccountDetailsTable() {
        return getAccountDetailsTableLocator();
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }

    public SelenideElement getCancelButton() {
        return getCancelButtonLocator();
    }
}
