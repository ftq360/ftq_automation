package com.ftq.test.features.steps;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.ftq.test.objects.File;
import com.ftq.test.objects.Inspection;
import com.ftq.test.pages.InspectionPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.Keys;

import java.time.LocalDate;

import java.text.SimpleDateFormat;
import  java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.ftq.test.utils.Common.*;
import static java.lang.Math.abs;
import static java.lang.Math.toIntExact;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class InspectionStepDefinitions {

    private String fileName, checkpoint, inspectionName, projectName, phaseName, checkpointName, responsibleParty, note, link, fieldNumber, value, fieldName, code;
    private SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
    private Calendar calendar = Calendar.getInstance();

    @When("^I click on '(.*)' button on inspection page$")
    public void iSelectXXXButtonOnInspectionPage(String buttonName) {
        Common.printCurrentTime();
        InspectionPage inspectionPage = page(InspectionPage.class);

        if (buttonName.equalsIgnoreCase("Save")) {
           // inspectionPage.getSaveButton().is(exist);
            inspectionPage.getSaveButton().waitUntil(visible, 20000);
            inspectionPage.getSaveButton().click();
//            if(inspectionPage.getSaveButton().is(visible)) {
//                inspectionPage.getSaveButton().click();
//            }
        }
        else if (buttonName.contains("Undo")) {
            inspectionPage.getUndoButton(buttonName.split("for")[1].trim()).click();
        }
        else if (buttonName.equalsIgnoreCase("Delete")) {
            inspectionPage.getDeleteButton().click();
        }
        else if (buttonName.equalsIgnoreCase("Copy")) {
            inspectionPage.getCopyButton().click();
        }
        else if (buttonName.equalsIgnoreCase("Show All")) {
            inspectionPage.clickShowAllButton();
        }
        else if (buttonName.equalsIgnoreCase("Back to original")) {
            inspectionPage.getBackToOriginalButton().click();
        }
        else {
            inspectionPage.getDynamicButton(buttonName).scrollTo();
            executeJavaScript("$('.ui.middle.grid.container').parents('div').eq(0).hide();");
            inspectionPage.getDynamicButton(buttonName).click();
        }
    }

    @When("^I click on '(.*)' button '(.*)' times to add rows$")
    public void iClickXXXButtonSeveralTimesOnInspectionPage(String buttonName, int time) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        for(int i = 1; i<=time; i++){
            inspectionPage.getDynamicButton(buttonName).waitUntil(exist, 20000);
            inspectionPage.getDynamicButton(buttonName).scrollTo();
            executeJavaScript("$('.ui.middle.grid.container').parents('div').eq(0).hide();");
            inspectionPage.getDynamicButton(buttonName).click();
            pause(2000);
        }
    }

    @When("^I fill '(.*)' field with '(.*)' text on inspection page$")
    public void iFillXXXFieldWithXXXTextOnInspectionPage(String fieldName, String text) {
        Common.printCurrentTime();
        InspectionPage inspectionPage = page(InspectionPage.class);
        if(fieldName.equalsIgnoreCase("Notes")) {
            pause(2000);
            executeJavaScript("$('html, body').animate({scrollTop: 0}, 0);");
            if(text.equalsIgnoreCase("performance")) {
                inspectionPage.setAlreadyFilledNotes(getUniqueValue(text));
            } else if(text.equalsIgnoreCase("performance2")) {
                inspectionPage.setAlreadyFilledNotes(getUniqueValue(text));
            }
            else {
                inspectionPage.setNotestInputField(getUniqueValue(text));

            }
        } else if(fieldName.equalsIgnoreCase("Location")){
            inspectionPage.setLocationDropdown(text);
        }
    }

    @When("^I fill '(.*)' checkpoint with '(.*)' text on inspection page$")
    public void iFillXXXCheckpointWithXXXTextOnInspectionPage(String checkpointName, String text) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.setCheckpoint(checkpointName, getUniqueValue(text));
    }

    @When("^I click on '(.*)' checkbox in '(.*)' checkpoint on inspection page$")
    public void iCheckXXXCheckboxInXXXCheckpointOnInspectionPage(String checkboxName, String text) {
        Common.printCurrentTime();
        InspectionPage inspectionPage = page(InspectionPage.class);
        if(text.equalsIgnoreCase("123")) {
            Common.pause(3000);
            inspectionPage.clickWithoutScrolling(text, checkboxName);
        } else {
            Common.pause(5000);
            inspectionPage.clickOnCheckbox(text, checkboxName);
        }
    }

    @When("^I attach file to checkpoint on inspection page:$")
    public void iAttachFileToCheckpointOnInspectionPage(List<File> files) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        files.stream().forEach(s -> {
            inspectionPage.attachFileToCheckpoint(s.getFileName(), s.getCheckpoint());
            inspectionPage.getImageSaveButton().click();
        });
    }

    @When("^I attach (\\d+) files to checkpoint on inspection page:$")
    public void iAttachXXXFilesToCheckpointOnInspectionPage(int fileAmount, List<File> files) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        files.stream().forEach(s -> {
            pause(6000);
            inspectionPage.attachMultipleFilesToCheckpoint(fileAmount, s.getFileName(), s.getCheckpoint());
        });
    }

    @Then("^I should see '(.*)' header and '(.*)' text in appeared error modal$")
    public void iShouldSeeXXXHeaderXXXTextInAppearedErrorModal(String header, String text) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getErrorModalHeader().shouldHave(text(header));
        inspectionPage.getErrorModalText().shouldHave(text(text));
    }

    @When("^I click on 'Ok' in upload error modal$")
    public void iClickOnOkInUploadErrorModal() {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getOkButtonInErrorModal().click();
    }

    @Then("^I should wait until Inspection ID value appears on inspection page$")
    public void iShouldWaitUntilInspectionIdAppearsOnInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getSyncedToServerField().waitUntil(visible, 90000);
        executeJavaScript("$('html, body').animate({scrollTop: 200}, 100);");
        inspectionPage.waitUntilInspectionIdAppears();
        inspectionPage.inspectionId = inspectionPage.getInspectionId().text();
    }

    @Then("^I should see valid Inspection ID on inspection page$")
    public void iShouldSeeValidInspectionIdOnInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getInspectionId().shouldHave(text(InspectionPage.inspectionId));
    }

    @Then("^I '(.*)' see appeared QC fields for checkpoint on inspection page$")
    public void iConditionSeeAppearedQcFieldForCheckpointOnInspectionPage(String condition) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);
        inspectionPage.getReadyForReviewCheckbox().shouldBe(should);
    }

    @Then("^I should see Notes field filled with '(.*)' text on inspection page$")
    public void iShouldSeeNotesFieldFilledWithXXXTextOnInspectionPage(String text) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getNotesField().waitUntil(visible, 60000);
        inspectionPage.getNotesField().shouldHave(text(getUniqueValue(text)));
    }

    @Then("^I should see Due Date field filled with '(.*)' text on inspection page$")
    public void iShouldSeeDueDateFieldFilledWithXXXTextOnInspectionPage(String text) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        String currentDatePlus = LocalDate.parse("current").plusDays(2).toString();

        inspectionPage.getDueDateField().waitUntil(visible, 60000);
        inspectionPage.getDueDateField().shouldHave(text(currentDatePlus));
    }

    @Then("^I should see Location field filled with '(.*)' text on inspection page$")
    public void iShouldSeeLocationFieldFilledWithXXXTextOnInspectionPage(String text) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getLocationField().waitUntil(visible, 60000);
        inspectionPage.getLocationField().shouldHave(value(text));
    }

    @Then("^I should see checked '(.*)' checkbox for '(.*)' checkpoint on inspection page$")
    public void iShouldSeeCheckedXXXCheckboxForXXXCheckpointOnInspectionPage(String checkbox, String checkpoint) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCheckboxInCheckpoint(checkpoint, checkbox).shouldBe(checked);
    }

    @Then("^I should see attached file to '(.*)' checkpoint on inspection page$")
    public void iShouldSeeAttachedFileToXXXCheckpointOnInspectionPage(String checkpointName) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getAttachedToCheckpointFile(checkpointName).shouldBe(visible);
    }

    @Then("^I should see '(.*)' inspection on inspection page$")
    public void iShouldSeeXXXInspectionOnInspectionPage(String inspectionName) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getInspectionName().shouldHave(text(inspectionName));
    }

    @Then("^I should wait until '(.*)' notification appears on inspection page$")
    public void iShouldWaitUntilXXXNotificationAppearsOnInspectionPage(String savedNotification) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        if (savedNotification.equalsIgnoreCase("Synced to server"))
        {
            inspectionPage.getSyncedToServerField().waitUntil(visible, 60000);
            inspectionPage.getSyncedToServerField().shouldBe(visible);
        }
        if (savedNotification.equalsIgnoreCase("Saved to device"))
        {
            inspectionPage.getSavedToDeviceField().shouldHave(text(savedNotification));
         }
    }

    @Then("^I '(.*)' see inspection with following data on inspection page:$")
    public void iConditionSeeInspectionWithFollowingDataOnInspectionPage(String condition, List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);

        inspections.stream().forEach(inspection -> {
            inspectionName = inspection.getInspectionName();
            projectName = inspection.getProjectName();
            phaseName = inspection.getPhaseName();
            checkpointName = inspection.getCheckpointName();
            responsibleParty = inspection.getResponsibleParty();
            if (StringUtils.isNotEmpty(inspectionName)) {
                inspectionPage.getInspectionName().shouldHave(text(getUniqueValue(inspectionName)));
            }
            if (StringUtils.isNotEmpty(projectName)) {
                inspectionPage.getInspectionProjectName().shouldHave(text(projectName));
            }
            if (StringUtils.isNotEmpty(phaseName)) {
                inspectionPage.getInspectionPhaseName().shouldHave(text(phaseName));
            }
            if (StringUtils.isNotEmpty(checkpointName)) {
                inspectionPage.getInspectionCheckpointName(getUniqueValue(checkpointName)).shouldBe(should);
            }
        });
    }

    @Then("I should see '(.*)' responsibility party on '(.*)' checkpoint on inspection page")
    public void iShouldSeeXXXResponsibilityPartyOnXXXCheckpointOnInspectionPage(String responsibilityPartyName, String checkpointName) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        assertThat(inspectionPage.getResponsiblePartyForCheckpoint(checkpointName).attr("value").contains(getUniqueValue(responsibilityPartyName)), equalTo(true));
    }

    @Then("^I '(.*)' see '(.*)' checkpoint on Inspection page$")
    public void iShouldSeeFollowingCheckpointOnInspectionPage(String condition, String checkpointName) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);

        inspectionPage.getCheckpointSegmentSection(checkpointName).waitUntil(should, 50000);
        inspectionPage.getCheckpointSegmentSection(checkpointName).shouldBe(should);
    }

    @Then("^I '(.*)' see '(.*)' checkpoint title section on Inspection page$")
    public void iShouldSeeFollowingCheckpointTitleSectionOnInspectionPage(String condition, String checkpointName) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);

        inspectionPage.getCheckpointTitleSection(checkpointName).waitUntil(should, 50000);
        inspectionPage.getCheckpointTitleSection(checkpointName).shouldBe(should);
    }



    @When("^I click on 'Add New' for \"Communications\" action$")
    public void iClickXXXforCommunicationsAction(){
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getCommunicationAddNewbtn().waitUntil(visible, 60000);
        executeJavaScript("$('html, body').animate({scrollTop: 400}, 0);");
        inspectionPage.getCommunicationAddNewbtn().click();
    }

    @When("^I click on '(.*)' button in Communication modal window$")
    public void iClickOnXXXButtonInCommunicationModalWindow(String button){
        InspectionPage inspectionPage = page(InspectionPage.class);

        if(button.equalsIgnoreCase("add")) {
            button = ".primary.ok.button";
        }
        inspectionPage.getActionBtnCommunicationModalWindow(button).click();
    }

    @When("^I click on \"Communications\" dropdown under inspection headers$")
    public void iClickOnXXXDropDownUnderInspectionHeaders(){
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getDropdownUnderInspectionHeaders().click();
    }

    @When("^I click on \"Inspection Id\" in Communications opened dropdown$")
    public void iClickOnInspectionIdInComunicationsOpenedDropDown(){
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getInspectionIDinCommunitiesSection().hover();
        inspectionPage.getInspectionIDinCommunitiesSection().click();
    }

    @When("^I click on camera button in '(.*)' checkpoint on inspection page$")
    public void iClickOnCameraButtonInXXXCheckpointOnInspectionPage(String checkpoint) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCameraButton(checkpoint).scrollTo();
        executeJavaScript("$('.ui.middle.grid.container').parents('div').eq(0).hide();");
        inspectionPage.getCameraButton(checkpoint).click();
        executeJavaScript("$('.ui.basic.grid.container').parents('div').eq(0).show();");
    }

    @When("^I select '(.*)' option in camera dropdown on inspection page$")
    public void iSelectXXXOptionInCameraDropdownOnInspectionPage(String option) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        if(option.equalsIgnoreCase("Signature")) {
            inspectionPage.getSignatureOption().click();
        }
        if(option.equalsIgnoreCase("Photo")) {
            inspectionPage.getPhotoOption().click();
        }
    }

    @When("^I draw signature on drawing board$")
    public void iDrawImageOnDrawingBoard() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.drawSignature();
    }

    @When("^I click on '(.*)' button on drawing board$")
    public void iClickOnXXXButtonOnDrawingBoard(String button) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        if(button.equalsIgnoreCase("Save")){
            inspectionPage.getImageSaveButton().click();
        }
    }

    @When("^I click on capture video on inspection page$")
    public void iClickOnCaptureVideoOnInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCaptureVideo().click();
    }

    @Then("^I should see drawing board$")
    public void iShouldSeeDrawingBoard() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getDrawingBoard().shouldBe(visible);
    }

    @When("^I select '(.*)' status on the 'Inspection' page$")
    public void iSelectXXXStatusOnTheInspectionPage(String status) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.scrollToStatusDropdown();
        inspectionPage.getStatusDropDown().click();
        executeJavaScript("$('.ui.basic.grid.container').parents('div').eq(0).hide();");
        Common.pause(1000);
        inspectionPage.getStatusMenuOption(status).click();
        executeJavaScript("$('.ui.basic.grid.container').parents('div').eq(0).show();");
    }

    @When("^I type '(.*)' Due Date on the 'Inspection' page$")
    public void iTypeXXXDueDateOnTheInspectionPage(String date) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        String currentDatePlus = LocalDate.now().plusDays(date.charAt(date.length()-1)).toString();

        executeJavaScript("document.querySelector(\"input[data-bind*='DatePunchDue']\").removeAttribute('readonly');");
        inspectionPage.getDueDateField().setValue(currentDatePlus);
        pause(2000);
    }

    @Then("^I should see '(.*)' icon for '(.*)' on the Inspections page$")
    public void iShouldSeeGreenIconForChecklistOnTheInspectionsPage(String color, String checklist) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        Map<String, String> colors = new HashMap<>();
        colors.put("green", "Icon_TaskSupplier_5.png");
        colors.put("grey", "Icon_TaskSupplier_2.png");
        colors.put("blue", "Icon_TaskSupplier.png");
        colors.put("yellow", "Icon_TaskSupplier_3.png");
        colors.put("red", "Icon_TaskSupplier_4.png");
        colors.put("red2", "Icon_TaskSupplier_9.png");
        colors.put("green2", "Icon_TaskSupplier_8.png");

        if(color.contains(color)) {
            color = colors.get(color);
        }
        assertThat(inspectionPage.getChecklistIcon(getUniqueValue(checklist)).attr("src").contains(color), equalTo(true));
    }

    @Then("^I should see page scrolled to checkpoint on inspection page$")
    public void iShouldSeePageScrolledToCheckpointOnInspectionPage() {
        while(toIntExact(executeJavaScript("return window.pageYOffset")) == 0) {
            Common.pause(3000);
        }
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getNotesField().waitUntil(visible, 60000);
        inspectionPage.getNotesField().shouldBe(visible);
        Common.pause(3000); // wait for scrolling animation
        int validAccuracy = 7;
        int actualOffset = toIntExact(executeJavaScript("return window.pageYOffset"));
        int yOffset = CommonStepDefinitions.yOffsetCoodinate;
        assertThat(abs(actualOffset) < (yOffset + validAccuracy), equalTo(true));
    }

    @Then("^I should see '(.*)' text instead of save button on inspection page$")
    public void iShouldSeeXXXTextInsteadOfSaveButtonOnInspectionPage(String text) {
        Common.printCurrentTime();
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getTextInsteadSaveButton(text).waitUntil(visible, 60000);
        inspectionPage.getTextInsteadSaveButton(text).shouldBe(visible);
        Common.printCurrentTime();
    }

    @And("^I click '(.*)' on Inspection page$")
    public void iClickXXXOnInspectionPage(String element) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        Common.waitPageLoading();
        if (element.equalsIgnoreCase("Checklist References")) {
            inspectionPage.getChecklistReferences().waitUntil(exist, 60000);
            inspectionPage.getChecklistReferences().waitUntil(visible, 60000);
            executeJavaScript("$('html, body').animate({scrollTop: 200}, 100);");
            inspectionPage.getChecklistReferences().hover();
            inspectionPage.getChecklistReferences().click();
        } else if (element.equalsIgnoreCase("Project References")) {
            inspectionPage.getProjectReferences().waitUntil(visible, 60000);
            executeJavaScript("$('html, body').animate({scrollTop: 500}, 100);");
            inspectionPage.getProjectReferences().hover();
            inspectionPage.getProjectReferences().click();
        } else if (element.equalsIgnoreCase("Phase References")) {
            inspectionPage.getPhaseReferences().waitUntil(visible, 60000);
            executeJavaScript("$('html, body').animate({scrollTop: 700}, 100);");
            inspectionPage.getPhaseReferences().hover();
            inspectionPage.getPhaseReferences().click();
        }
        Common.waitPageLoading();
    }

    @Then("^I 'should' see following attachments on Checklist References section:$")
    public void iShouldSeeFollowingAttachmentsOnChecklistReferencesSection(List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        ElementsCollection attachmentsCollection = inspectionPage.getAttachmentsList();

        inspections.stream().forEach(inspection -> {
            link = inspection.getLink();
            note = inspection.getNote();

            attachmentsCollection.stream().forEach(attachment -> {
                String text = attachment.getText();
                if (StringUtils.isNotEmpty(link) && text.equalsIgnoreCase(link)) {
                    Assert.assertThat(text, equalTo(link));
                }
                if (StringUtils.isNotEmpty(note) && text.equalsIgnoreCase(note)) {
                    Assert.assertThat(text, equalTo(note));
                }
            });
        });
    }

    @Then("^I should see '(\\d+)' as amount of attachments$")
    public void iShouldSeeXXXAsAmountOfAttachments(int amount) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Common.waitPageLoading();
        pause(7000);
        inspectionPage.getAttachments().shouldHaveSize(amount);
    }


    @Then("^I should see same data on Inspection page as data from first inspection in list on Create Inspection page$")
    public void iShouldSeeSameDataOnInspectionPageAsDataFromFirstInspectionInListOnCreateInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getInspectionId().shouldHave(text(CreateInspectionStepDefinition.data.get("inspectionId")));
        String inspectionDate = CreateInspectionStepDefinition.data.get("lastActivity");
        assertThat(inspectionPage.getLastInspected().text(), equalTo(inspectionDate));
        inspectionPage.getStatusDropDown().shouldHave(value(CreateInspectionStepDefinition.data.get("status")));
        inspectionPage.getInspectionName().shouldHave(text(CreateInspectionStepDefinition.data.get("checklist")));
        inspectionPage.getResponsiblePartyInput().shouldHave(value(CreateInspectionStepDefinition.data.get("crew")));
        inspectionPage.getInspectionProjectName().shouldHave(text(CreateInspectionStepDefinition.data.get("project").split("/")[0]));
        inspectionPage.getInspectionPhaseName().shouldHave(text(CreateInspectionStepDefinition.data.get("project").split("/")[1]));
        assertThat(inspectionPage.getLocationText(), equalTo(CreateInspectionStepDefinition.data.get("project").split("/")[2]));
        inspectionPage.getInspectedBy().shouldHave(text(CreateInspectionStepDefinition.data.get("lastInspection").split("\\(")[0]));
    }

    @Then("^I should see '(.*)' status on inspection page$")
    public void iShouldSeeXXXStatusOnInspectionPage(String status){
        InspectionPage inspectionPage = page(InspectionPage.class);
        Common.waitPageLoading();
        Assert.assertThat(inspectionPage.getStatus(), equalTo(status));
    }

    @When("^I click on '(.*)' in top Inspection bar$")
    public void iClickInspectionFilterInTopBar(String button){
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getTopInspectionBarBtn(button).waitUntil(exist, 20000);
        inspectionPage.getTopInspectionBarBtn(button).click();
    }

    @When("^I click on '(.*)' row status in status filter$")
    public void iClickRowInStatusFilter(String row){
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getRowStatusInFilter(row).click();
    }

    @When("^I select override checkbox for status on the 'Inspection' page$")
    public void iSelectOverrideCheckboxForStatusOnTheInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.scrollToStatusDropdown();
        inspectionPage.getStatusDropDown().click();
        inspectionPage.getStatusOverrideCheckbox().setSelected(true);
        inspectionPage.getStatusDropDown().click();
    }

    @And("^I (fill|clear) Dynamic Matrix fields on inspection page:$")
    public void iFillDynamicMatrixFieldsOnInspectionPage(String action, List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        for(Inspection inspection : inspections) {
            fieldNumber = inspection.getFieldNumber();
            value = inspection.getValue();

            pause(1000);
            inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).scrollTo();
          //  executeJavaScript("$('.table-header').get(0).scrollIntoView()");
            inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).hover();
            inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).click();
            if(action.equalsIgnoreCase("fill")) {
                System.out.println("TEST" + getUniqueValue(value));
                pause(1000);
                inspectionPage.getActiveTextArea().sendKeys(getUniqueValue(value));
                pause(3000);
            } else {
                inspectionPage.getActiveTextArea().waitUntil(visible, 30000);
                inspectionPage.getActiveTextArea().clear();
            }
        }
    }

    @When("^I fill '(.*)' cells with '(.*)' value in Dynamic Matrix on inspection page$")
    public void iFillBigDynamicMatrixFieldsOnInspectionPage(String numbers, String value) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        if (numbers.contains(",")) {
            String[] cellNum = numbers.split(",");
            for (String number : cellNum) {
                inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(number) - 1).scrollTo();
                inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(number) - 1).click();
                pause(1000);
                inspectionPage.getActiveTextArea().setValue(getUniqueValue(value+ number));
                pause(1000);
            }
        }else{
            //String cellNum[] = numbers.split("...");
            int number = Integer.parseInt(numbers);
            for(int i=1; i<=number; i++){
                inspectionPage.getDynamicMatrixFieldsList().get(i-1).scrollTo();
                inspectionPage.getDynamicMatrixFieldsList().get(i-1).click();
                pause(1000);
                inspectionPage.getActiveTextArea().setValue(getUniqueValue(value + i));
                pause(1000);
            }
        }
    }

    @When("^I click on '(.*)' cell and press '(.*)' key '(.*)' times in Dynamic Matrix on inspection page$")
    public void iClickCellPressXXXKeyXXXTimesInDynamicMatrixOnInspectionPage(int number, String button, int time) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getDynamicMatrixFieldsList().get(number-1).scrollTo();
        inspectionPage.getDynamicMatrixFieldsList().get(number-1).click();
        for(int i = 0; i < time; i++) {
            switch (button) {
                case "Tab":
                    inspectionPage.getActiveTextArea().sendKeys(Keys.TAB);
                    break;
            }
            pause(2000);
        }


    }

    @When("^I shift '(.*)' row to '(.*)' row in Dynamic Matrix on inspection page$")
    public void iShiftRowInDynamicMatrixOnInspectionPage(int shiftedRow, int toRow){
        InspectionPage inspectionPage = page(InspectionPage.class);

        pause(5000);
        inspectionPage.getDynamicMatrixFieldsList().get(toRow-1).scrollTo();

        pause(5000);
        inspectionPage.getDynamicMatrixFieldsList().get(shiftedRow-1).dragAndDropTo(inspectionPage.getDynamicMatrixFieldsList().get(toRow-1));
    }

    @When("^I comment each attachment with '(.*)' text on inspection page$")
    public void iCommentEachAttachmentOnInspectionPage(String comment) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        for(int i = 0; i < inspectionPage.getAttachmentNoteFields().size(); i++) {
            inspectionPage.getAttachmentNoteFields().get(i).scrollTo();
            String request = String.format("return document.querySelectorAll(\"[data-bind*='textInput: Note']\")[%s].value", i);
            if(executeJavaScript(request).toString().equalsIgnoreCase("")) {
                inspectionPage.getAttachmentNoteFields().get(i).setValue(getUniqueValue(comment + i));
            }
        }
    }

    @Then("^I should see '(\\d+)' attachments in '(.*)' checkpoint on inspection page$")
    public void iShouldSeeXXXAttachmentsInXXXCheckpointOnInspectionPage(int amount, String checkpoint) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        System.out.println(inspectionPage.getAttachmentsFromCheckpoint(checkpoint).size());
        inspectionPage.getAttachmentsFromCheckpoint(checkpoint).shouldHave(CollectionCondition.size(amount));
    }

    @Then("^I should see Dynamic Matrix with following data on inspection page:$")
    public void iShouldSeeDynamicMatrixWithFollowingDataOnInspectionPage(List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        for(Inspection inspection : inspections) {
            fieldNumber = inspection.getFieldNumber();
            value = inspection.getValue();

           // inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).waitUntil(visible, 20000);
            inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).scrollTo();
            pause(5000);
            assertThat(inspectionPage.getDynamicMatrixFieldsList().get(Integer.parseInt(fieldNumber) - 1).getText().contains(getUniqueValue(value)), equalTo(true));
        }
    }

    @Then("^I should see following fields for '(.*)' checkpoint on inspection page:$")
    public void iShouldSeeFollowingFieldsForXXXCheckpointOnInspectionPage(String checkpoint, List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        for(Inspection inspection : inspections) {
            fieldName = inspection.getFieldName();
            inspectionPage.getCheckpointField(checkpoint, fieldName).shouldBe(visible);
        }
    }

    @When("^I fill following fields for '(.*)' checkpoint with following data on inspection page:$")
    public void iFillFollowingFieldsForXXXCheckpointWithFollowingDataOnInspectionPage(String checkpoint, List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCameraButton(checkpoint).scrollTo();

        for(Inspection inspection : inspections) {
            fieldName = inspection.getFieldName();
            value = inspection.getValue();

            switch (fieldName.toLowerCase()) {
                case "ready for review":
                    inspectionPage.getCheckpointField(checkpoint, "Ready for review").setSelected(Boolean.valueOf(value));
                    break;
                case "responsible party": case "location":
                    inspectionPage.setCheckpointDropdown(checkpoint, fieldName, getUniqueValue(value));
                    break;
                case "risk factor":
                    if(value == "empty") { value = " "; }
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).click();
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).clear();
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).sendKeys(Keys.BACK_SPACE);
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).sendKeys(value);
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).sendKeys(Keys.ENTER);
                    break;
                case "due date":
                    inspectionPage.setNextMonthFirstDayDate(checkpoint);
                    break;
//                case "Enter Observation":
//                    inspectionPage.getCheckpointObservationField(checkpoint).waitUntil(exist, 30000);
//                    inspectionPage.getCheckpointObservationField(checkpoint).click();
//                    inspectionPage.getCheckpointObservationFieldInput.waitUntil(exist, 30000);
//                    inspectionPage.getCheckpointObservationFieldInput.setValue(getUniqueValue(value));
                default:
                    inspectionPage.getCheckpointFieldByName(checkpoint, fieldName).waitUntil(exist, 30000);
                    inspectionPage.getCheckpointFieldByName(checkpoint, fieldName).hover();
                    inspectionPage.getCheckpointFieldByName(checkpoint, fieldName).click();
                    inspectionPage.getCheckpointFieldInputByName(checkpoint, fieldName).waitUntil(exist, 30000);
                    inspectionPage.getCheckpointFieldInputByName(checkpoint, fieldName).setValue(getUniqueValue(value));
//                    inspectionPage.getCheckpointField(checkpoint, fieldName).waitUntil(exist, 30000);
//                    inspectionPage.getCheckpointField(checkpoint, fieldName).click();
//                    inspectionPage.getCheckpointFieldInput(checkpoint, fieldName).waitUntil(exist, 30000);
//                    inspectionPage.getCheckpointFieldInput(checkpoint, fieldName).setValue(getUniqueValue(value));
            }
        }
    }

    @Then("^I should see following data in '(.*)' checkpoint on inspection page:$")
    public void iShouldSeeFollowingDataInXXXCheckpointOnInspectionPage(String checkpoint, List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCameraButton(checkpoint).waitUntil(exist, 10000);
        inspectionPage.getCameraButton(checkpoint).scrollTo();

        for(Inspection inspection : inspections) {
            fieldName = inspection.getFieldName();
            value = inspection.getValue();

            switch (fieldName.toLowerCase()) {
                case "ready for review":
                    inspectionPage.getReadyForReviewCheckbox(checkpoint).shouldBe(checked);
                    break;
                case "responsible party": case "location":
                    assertThat(inspectionPage.getDropdownInputField(checkpoint, fieldName).getAttribute("value").contains(getUniqueValue(value)), equalTo(true));
                    break;
                case "risk factor": case "testr2": case "testr3":
                    inspectionPage.getRiskFactorInputField(checkpoint, fieldName).shouldHave(attribute("value", value));
                    break;
                case "due date":
                    if(value.equalsIgnoreCase("first day of next month")) {
                        Calendar nextMonth = (Calendar) calendar.clone();
                        nextMonth.add(Calendar.MONTH, 1);
                        nextMonth.set(nextMonth.get(Calendar.YEAR), nextMonth.get(Calendar.MONTH), 1);
                        pause(5000);
                        System.out.println(format.format(nextMonth.getTime()));
                        inspectionPage.getDueDateInputField(checkpoint).shouldHave(attribute("value", format.format(nextMonth.getTime())));
                    }else if(value.equalsIgnoreCase("first day of second next month")){
                        Calendar nextMonth = (Calendar) calendar.clone();
                        nextMonth.add(Calendar.MONTH, 2);
                        nextMonth.set(nextMonth.get(Calendar.YEAR), nextMonth.get(Calendar.MONTH), 1);
                        inspectionPage.getDueDateInputField(checkpoint).shouldHave(attribute("value", format.format(nextMonth.getTime())));
                    }
                    break;
                default:
                    inspectionPage.getFilledCheckpointField(checkpoint, fieldName).click();
                    inspectionPage.getCheckpointFieldInput(checkpoint, fieldName).shouldHave(text(getUniqueValue(value)));
            }
        }
    }

    @Then("^I '(.*)' see '(.*)' risk factor in '(.*)' checkpoint$")
    public void iClickITPDropdownOnInspectionPage(String condition, String riskFactor, String checkpoint) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);

        inspectionPage.getRiskFactorInputField(checkpoint, riskFactor).shouldBe(should);
    }

    @And("^I click ITP dropdown on Inspection page$")
    public void iClickITPDropdownOnInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getITPDropdown().click();
    }

    @Then("^I 'should' see following code into ITP drop down on Inspection page:$")
    public void iShouldSeeFollowingCodeIntoITPDropDownOnInspectionPage(List<Inspection> inspections) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        for(Inspection inspection : inspections) {
            code = inspection.getCode();

            if (StringUtils.isNotEmpty(code)) {
                inspectionPage.getITPDropdownOption(getUniqueValue(code)).shouldBe(exist);
            }
        }
    }

    @When("^I click on share checkbox on inspection page$")
    public void iClickOnShareCheckboxOnInspectionPage() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getShareCheckbox().click();
    }

    @Then("^I '(.*)' see 'Inspection created date' section is editable$")
    public void iShouldSeeInspectionCreatedDateSectionIsEditable(String condition) {
        InspectionPage inspectionPage = page(InspectionPage.class);
        Condition should = getVisibleCondition(condition);

        inspectionPage.getCreatedDateEditable().shouldBe(should);
    }

    @When("^I click on 'Inspection created date' section$")
    public void iClickInspectionCreatedDateSection() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        inspectionPage.getCreatedDateEditable().waitUntil(visible, 20000);
        inspectionPage.getCreatedDateEditable().click();
    }

    @When("^I select '(.*)' date in calendar modal window$")
    public void iSelectXXXDateInCalendarModalWindow(String date) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        inspectionPage.getDateInCalendarWindow(date).waitUntil(visible, 20000);
        inspectionPage.getDateInCalendarWindow(date).click();
    }

    @Then("^I should see '(.*)' inspection creation date$")
    public void iShouldSeeXXXInspectionCreationDate(String date) {
        InspectionPage inspectionPage = page(InspectionPage.class);

        //assertThat(
                inspectionPage.getCreatedDateEditable().shouldHave(text(date));
                        //.text(), equalTo(date));
    }
}