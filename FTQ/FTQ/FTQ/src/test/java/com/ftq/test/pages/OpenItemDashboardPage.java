package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class OpenItemDashboardPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[contains(.,'Priority Open Deficiency Dashboard')]")); }
    private SelenideElement getChartTitleLocator(String chartName) { return $(By.xpath(String.format("//h3[text()='%s']", chartName))); }

    public SelenideElement getChartTitle(String chartName) {
        return getChartTitleLocator(chartName);
    }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }
}
