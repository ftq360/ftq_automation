package com.ftq.test.modals;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class NoteLinkModal {

    private SelenideElement getTextFieldLocator() { return $(".mfp-content textarea,.mfp-content input"); }
    private SelenideElement getSaveButtonLocator() { return $("#app-dlg-ok"); }

    public SelenideElement getTextField() {
        return getTextFieldLocator();
    }

    public SelenideElement getSaveButton() {
        return getSaveButtonLocator();
    }
}
