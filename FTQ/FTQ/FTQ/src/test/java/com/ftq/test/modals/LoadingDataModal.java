package com.ftq.test.modals;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class LoadingDataModal {

    //Elements in the modal

    private SelenideElement getModalBodyElement() { return $("[style*=transition]"); }
    private SelenideElement getCloseModalButtonElement() { return  $(".message-banner__img"); }

    //Actions in the modal

    public SelenideElement getLoadingModal() {
        return getModalBodyElement();
    }

    public SelenideElement getCloseModalButton() {
        return getCloseModalButtonElement();
    }
}


