package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.ftq.test.menu.TopMenu;
import com.ftq.test.objects.Phase;
import com.ftq.test.pages.CommonPage;
import com.ftq.test.pages.CreateInspectionPage;
import com.ftq.test.pages.UserBasePage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.ftq.test.utils.Common.*;

public class CreateInspectionStepDefinition {

    public static HashMap<String, String> data = new HashMap<>();
    private String code, checklist;

    @When("^I select '(.*)' (?:division|project|phase|inspection|responsibility party|location|equipment) on create inspection page$")
    public void iSelectXXXDivision(String divisionName) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);
        pause(3);
        createInspectionPage.selectDivision(getUniqueValue(divisionName)).waitUntil(exist, 40000);
        createInspectionPage.selectDivision(getUniqueValue(divisionName)).hover();
        createInspectionPage.selectDivision(getUniqueValue(divisionName)).click();
    }

    @Then("^I '(.*)' see '(.*)' (?:project|responsible party) on create inspection page$")
    public void iConditionSeeProject(String condition, String projectName) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);
        Condition should = getVisibleCondition(condition);

        createInspectionPage.getDivision(getUniqueValue(projectName)).waitUntil(should, 60000);
        createInspectionPage.getDivision(getUniqueValue(projectName)).shouldBe(should);
    }

    @Then("^I 'should' see '/Inspections/Create' page$")
    public void iShouldSeeCreateInspectionPagePage() {
        UserBasePage userBasePage = page(UserBasePage.class);
        TopMenu topMenu = page(TopMenu.class);

        userBasePage.getTopMenuItem("Setup").shouldBe(Condition.visible);
        userBasePage.getTopMenuItem("Inspections").shouldBe(Condition.visible);
        userBasePage.getTopMenuItem("Dashboards").shouldBe(Condition.visible);
        userBasePage.getTopMenuItem("Report").shouldBe(Condition.visible);
        userBasePage.getTopMenuItem("Help").shouldBe(Condition.visible);
        topMenu.getUserMenuButton().shouldBe(Condition.visible);
        topMenu.getSignalButton().shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        userBasePage.getTitle().shouldBe(Condition.visible);
        //userBasePage.getPlanModeCheckbox().shouldBe(Condition.visible);
    }

    @And("^I click '(.*)' button on Create Inspection page$")
    public void iClickXXXButtonOnCreateInspectionPage(String button) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        if (button.equalsIgnoreCase("Show Recent Inspections")) {
            createInspectionPage.getShowRecentInspectionsButton().click();
        }
    }

    @And("^I get data from first inspection in list on Create Inspection page$")
    public void iGetDataFromFirstInspectionInListOnCreateInspectionPage() {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        data.put("inspectionId", createInspectionPage.getInspectionId().text());
        data.put("lastActivity", createInspectionPage.getLastActivity().text());
        data.put("status", createInspectionPage.getStatus().text());
        data.put("checklist", createInspectionPage.getChecklist().text());
        data.put("crew", createInspectionPage.getCrew().text());
        data.put("project", createInspectionPage.getProject().text());
        data.put("lastInspection", createInspectionPage.getLastInspection().text());
    }

    @And("^I click first inspection in list on Create Inspection page$")
    public void iClickInspectionInListOnCreateInspectionPage() {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        createInspectionPage.getInspectionId().click();
    }

    @When("^I click on '(.*)' button on Create Inspection page$")
    public void iClickOnXXXButtonOnCreateInspectionPage(String button){
        TopMenu topMenu = page(TopMenu.class);

        if(button.equalsIgnoreCase("Show Recent Inspections")) {
            topMenu.getShowRecentInspections().click();
        }
    }

    @Then("^I should see inspections with saved guids on (?:Create Inspection|Sync Control Panel) page$")
    public void iShouldSeeInspectionsWithSavedGuidsOnCreateInspectionPage() {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        pause(1000);
        for(String s : CommonPage.guids) {
            createInspectionPage.getInspectionGUID(s).waitUntil(Condition.visible, 120000);
            createInspectionPage.getInspectionGUID(s).shouldBe(Condition.visible);
        }
    }

    @And("^I get inspections list on create inspection page$")
    public void iGetInspectionsListOnCreateInspectionPage() {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        List<SelenideElement> selenideElements =  createInspectionPage.getInspectionsList();
        List<String> inspections = new ArrayList<>();

        for (SelenideElement selenideElement : selenideElements) {
            inspections.add(selenideElement.text());
        }
    }

    @And("^I (uncheck|check) 'Plan mode' checkbox on create inspection page$")
    public void iXXXPlanModeCheckboxOnCreateInspectionPage(String action) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);
        createInspectionPage.getPlanModeCheckbox().waitUntil(visible, 20000);
        createInspectionPage.setPlanMode(action.equals("check"));
//        System.out.println(createInspectionPage.getPlanModeCheckbox().has(cssClass("checked")));
//        if((!createInspectionPage.getPlanModeCheckbox().has(cssClass("checked"))) && action.equals("check")) {
//            createInspectionPage.getPlanModeCheckbox().click();
//            pause(3000);
//        } else if (createInspectionPage.getPlanModeCheckbox().has(cssClass("checked")) && action.equals("uncheck")) {
//            createInspectionPage.getPlanModeCheckbox().click();
//            pause(3000);
//        }

    }

    @And("^I uncheck 'Plan mode' if it checked$")
    public void IUncheckPlanModeIfItChecked() {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);
        createInspectionPage.getPlanModeCheckbox().waitUntil(exist, 20000);
        if (createInspectionPage.getInspectionSelectionStepTitle("Plan").is(exist)) {
            createInspectionPage.getPlanModeCheckbox().click();
        }
    }

    @Then("^I should see itp plan item with following data on create inspection page:$")
    public void iShouldSeeItpPlanItemWithFollowingDataOnCreateInspectionPage(List<Phase> phases) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);
        for (Phase phase : phases) {
            code = phase.getPhaseCode();
            checklist = phase.getChecklist();

            createInspectionPage.getTitle(checklist).waitUntil(visible, 20000);
            createInspectionPage.getTitle(checklist).shouldBe(visible);
            createInspectionPage.getTitle(getUniqueValue(code)).shouldBe(visible);
        }
    }

    @When("^I click '(.*)' checklist on Create Inspection page$")
    public void iClickIxxxChecklistOnCreateInspectionPage(String code) {
        CreateInspectionPage createInspectionPage = page(CreateInspectionPage.class);

        createInspectionPage.getChecklistCode(getUniqueValue(code)).click();
    }
}
