package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.SupportFtq360Page;
import com.ftq.test.pages.VideoTutorials;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class VideoTutorialsStepDefinitions {

    @Then("^I 'should see 'Videos' page$")
    public void iShouldSeeVideosPage() {
        VideoTutorials videoTutorials = page(VideoTutorials.class);
        SupportFtq360Page supportFtq360Page = page(SupportFtq360Page.class);

        videoTutorials.getTitle().shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Community").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Submit a request").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Contact Us").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Sign in").shouldBe(Condition.visible);
        supportFtq360Page.getLogo().shouldBe(Condition.visible);
        supportFtq360Page.getSearchField().shouldBe(Condition.visible);
    }

}
