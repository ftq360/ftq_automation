package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ErrorReportPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ErrorReportStepDefinitions {

    @Then("^I 'should' see '/Home/ErrorReport' page$")
    public void iShouldSeeHomeErrorReportPage() {
        ErrorReportPage errorReportPage = page(ErrorReportPage.class);

        errorReportPage.getTitle().shouldBe(Condition.visible);
        errorReportPage.getDateInput().shouldBe(Condition.visible);
        errorReportPage.getNoteInput().shouldBe(Condition.visible);
        errorReportPage.getBackButton().shouldBe(Condition.visible);
        errorReportPage.getSendReportButton().shouldBe(Condition.visible);
        errorReportPage.getDeleteAllDataButton().shouldBe(Condition.visible);
    }
}
