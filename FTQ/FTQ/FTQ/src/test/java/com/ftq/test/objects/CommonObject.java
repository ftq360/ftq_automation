package com.ftq.test.objects;

public class CommonObject {

    private String column;
    private String button;

    public String getColumn() {
        return column;
    }

    public String getButton() {
        return button;
    }
}
