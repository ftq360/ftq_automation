package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;


public class UserBasePage {

    //Elements on the page

    private SelenideElement getTopMenuItemLocator(String menuItem) { return $(By.xpath(String.format("//div[@id='appTopMenu']/div[contains(.,'%s')]", menuItem))); }
    private SelenideElement getTopMenuItemFromDropdown(String itemName) { return $(By.xpath(String.format("//div[contains(@class,'visible')]//a[@class='item' and contains(.,'%s')]", itemName))); }
    private SelenideElement getDropdownFromTopMenuDropdown(String dropdownName) { return $(By.xpath(String.format("//div[@class='ui dropdown item' and contains(.,'%s')]", dropdownName))); }
    private SelenideElement getNotificationLocator(String notification) { return $(By.xpath(String.format("//h3[text()='%s']", notification))); }
    private SelenideElement getSearchFieldLocator() { return $("#select-menu-filter"); }
    private SelenideElement getXIconInSearchFieldLocator() { return $(".crossimg"); }
    private SelenideElement getItemToSaveLocator() { return $(".has-changes"); }
    private SelenideElement getBlueSaveButtonLocator() { return $(By.xpath("//span[@class='big-default-btn' and .='Save']")); }
    private SelenideElement getGreySaveButtonLocator() { return $(By.xpath("//span[@class='big-common-btn' and .='Save']")); }
    private SelenideElement getOfflineNotificationLocator() { return $(By.xpath("//div[@class='toast-message']")); }
    private SelenideElement getSaveNotificationLocator() { return $(By.xpath("//*[@id=\"toast-container\"]")); }
    private SelenideElement getTitleLocator() { return $(By.xpath("//h2[text()='Division']")); }
    private SelenideElement getPlanModeCheckboxLocator() { return $("[data-bind*='isPlanSelectionVisible']"); }

    //Actions on the page

    public SelenideElement getTopMenuItem(String menuItem) {
        return getTopMenuItemLocator(menuItem);
    }

    public void selectItemFromTopMenuDropdown(String itemName) {
        Common.waitPageLoading();
        getTopMenuItemFromDropdown(itemName).waitUntil(exist, 60000);
        //getTopMenuItemFromDropdown(itemName).waitUntil(visible, 60000);
        getTopMenuItemFromDropdown(itemName).click();
    }

    public SelenideElement getItemFromMenuDropdown(String itemName) {
        return  getTopMenuItemFromDropdown(itemName);
    }

    public void selectDropdownFromTopMenuDropdown(String dropdownName) {
        Common.pause(3000);
        getDropdownFromTopMenuDropdown(dropdownName).waitUntil(exist, 60000);
        Common.pause(3000);
        getDropdownFromTopMenuDropdown(dropdownName).waitUntil(visible, 60000);
    //    getDropdownFromTopMenuDropdown(dropdownName).hover();
        getDropdownFromTopMenuDropdown(dropdownName).hover();
        Common.pause(2000);
    }

    public static Condition getVisibleCondition(String condition) {
        if (condition.equalsIgnoreCase("should")) {
            return visible;
        }
        else {
            return hidden;
        }
    }

    public static Condition getExistCondition(String condition) {
        if (condition.equalsIgnoreCase("should")) {
            return exist;
        }
        else {
            return hidden;
        }
    }

    public SelenideElement getNotification(String notification) {
        return getNotificationLocator(notification);
    }

    public SelenideElement getSearchField() {
        return getSearchFieldLocator();
    }

    public SelenideElement getXIconInSearchField() {
        return getXIconInSearchFieldLocator();
    }

    public SelenideElement getItemToSave() {
        return getItemToSaveLocator();
    }

    public SelenideElement getBlueSaveButton() {
        return getBlueSaveButtonLocator();
    }

    public SelenideElement getGreySaveButton() {
        return getGreySaveButtonLocator();
    }

    public SelenideElement getToastNotification() {
        return getOfflineNotificationLocator();
    }

    public SelenideElement getSaveNotification() {
        return getSaveNotificationLocator();
    }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getPlanModeCheckbox() {
        return getPlanModeCheckboxLocator();
    }
}
