package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.CommonPage;
import com.ftq.test.pages.ForgotPasswordPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.getUniqueValue;

public class ForgotPasswordStepDefinitions {

    @Then("^I should see '(.*)' text in Forgot Password modal$")
    public void iShouldSeeForgotPasswordLinkUnderLoginForm(String text){
        ForgotPasswordPage forgotPasswordPage = page(ForgotPasswordPage.class);

        forgotPasswordPage.getForgotPasswordModalText(text).shouldBe(Condition.visible);
    }

    @When("^I click on 'Reset' button in Forgot Password form$")
    public void iTypeInUserNameField(){
        ForgotPasswordPage forgotPasswordPage = page(ForgotPasswordPage.class);

        forgotPasswordPage.getResetBtnL().click();
    }

    @When("^I type '(.*)' in 'User name' field$")
    public void iTypeInUserNameField(String text){
        ForgotPasswordPage forgotPasswordPage = page(ForgotPasswordPage.class);

        forgotPasswordPage.getUserNameField().setValue(getUniqueValue(text));
    }

    @Then("^I should see in forgot password form following text:$")
    public void IShouldSeeInForgotPasswordFormFollowingText(String message) {
        ForgotPasswordPage forgotPasswordPage = page(ForgotPasswordPage.class);

        forgotPasswordPage.getConfirmationSendingResetPasswordText(message).shouldBe(Condition.visible);
    }

}
