package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ReportsInteractivePage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Interactive Reports']"));}
    private SelenideElement getReportsOptionListLocator() { return $(".option-list"); }
    private SelenideElement getReportByLocator(String reportSelection) { return $(By.xpath(String.format("//span[contains(.,'by %s')]/parent::a", reportSelection))); }
    private SelenideElement getSelectAllButtonLocator() { return $(".big-default-btn"); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getReportsOptionList() {
        return getReportsOptionListLocator();
    }

    public SelenideElement getReportBy(String reportSelection) {
        return getReportByLocator(reportSelection);
    }

    public SelenideElement getSelectAllButton() {
        return getSelectAllButtonLocator();
    }
}
