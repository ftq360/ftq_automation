package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PerformancePage {

    private SelenideElement getInspectionIdsLocator() { return $("[aria-describedby='checklists-grid_cb']"); }
    private SelenideElement getRadioButtonsLocator() { return $("[type='radio']"); }
    private SelenideElement getDeficiencyIdsLocator() { return $("[aria-describedby='deficiency-grid_DataCollRecordID']"); }

    public SelenideElement getInspectionIds() {
        return getInspectionIdsLocator();
    }

    public SelenideElement getDeficiencyIds() {
        return getDeficiencyIdsLocator();
    }

    public SelenideElement getRadioButtons() {
        return getRadioButtonsLocator();
    }
}
