package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.modals.NoteLinkModal;
import com.ftq.test.objects.TaskSetup;
import com.ftq.test.objects.OptionCondition;
import com.ftq.test.pages.*;
import com.ftq.test.utils.Common;
import com.ftq.test.pages.UserBasePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import static com.codeborne.selenide.Condition.*;
import org.openqa.selenium.Keys;

import java.io.File;
import java.util.List;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.utils.Common.getUniqueValue;
import static com.ftq.test.utils.Common.pause;

public class TaskSetupStepDefinitions {

    String text, category, attachLink, attachNote, file, type, row, typeCheckpointRow, typeCheckpoint, precondition, condition;

    @When("^I click '(.*)' button on the (?:Vendor|)TaskSetup page$")
    public void iClickXXXButtonOnTheTaskSetup(String button) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        pause(2000);
        if (button.equalsIgnoreCase("+Add")) {
            taskSetupPage.getAddButton().waitUntil(visible, 120000);
            taskSetupPage.getAddButton().waitUntil(exist, 120000);
            taskSetupPage.getAddButton().click();
        }
        else if(button.equalsIgnoreCase("Save")) {
            taskSetupPage.getSaveButton().waitUntil(exist, 30000);
            taskSetupPage.getSaveButton().click();
            taskSetupPage.getSaveButton().waitUntil(visible, 30000);
        }
        pause(2000);
    }

    @And("^I add following data for new (?:checklist|checkpoint) on the TaskSetup page:$")
    public void iAddFollowingDataForNewChecklist(List<TaskSetup> tasksSetup) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        for (TaskSetup taskSetup : tasksSetup) {
            text = taskSetup.getText();
            category = taskSetup.getCategory();
            type = taskSetup.getType();
            typeCheckpointRow = taskSetup.getTypeCheckpointRow();
            typeCheckpoint = taskSetup.getTypeCheckpoint();

            if (StringUtils.isNotEmpty(type)) {
                Common.pause(1000);
                taskSetupPage.getTypeDropDown().hover();
                taskSetupPage.getTypeDropDown().click();
                taskSetupPage.getTypeItem(type).waitUntil(exist, 40000);
                taskSetupPage.getTypeItem(type).waitUntil(visible, 40000);
                taskSetupPage.getTypeItem(type).hover();
                taskSetupPage.getTypeItem(type).click();
            }
            if (StringUtils.isNotEmpty(typeCheckpointRow)) {
                Common.pause(1000);
                taskSetupPage.getSubCheckpointTypeField(typeCheckpointRow).hover();
                taskSetupPage.getSubCheckpointTypeField(typeCheckpointRow).click();
                taskSetupPage.getTypeItem(typeCheckpoint).waitUntil(exist, 40000);
                taskSetupPage.getTypeItem(typeCheckpoint).waitUntil(visible, 40000);
                taskSetupPage.getTypeItem(typeCheckpoint).hover();
                taskSetupPage.getTypeItem(typeCheckpoint).click();
            }
            if (StringUtils.isNotEmpty(text)) {
                Common.pause(1000);
                taskSetupPage.getTextField().setValue(getUniqueValue(text));
            }
            if (StringUtils.isNotEmpty(category)) {
                Common.pause(1000);
                taskSetupPage.getCategoryDropDown().waitUntil(exist, 20000);
                taskSetupPage.getCategoryDropDown().waitUntil(visible, 20000);
                taskSetupPage.getCategoryDropDown().hover();
                taskSetupPage.getCategoryDropDown().click();
                taskSetupPage.getCategoryItem(category).waitUntil(exist, 40000);
                taskSetupPage.getCategoryItem(category).waitUntil(visible, 40000);
                taskSetupPage.getCategoryItem(category).hover();
                taskSetupPage.getCategoryItem(category).click();
            }
        }
    }

    @And("^I set If-Else condition for checkpoint on the TaskSetup page:$")
    public void iSetIfElseConditionForCheckpointOnTheTaskSetupPage(List<OptionCondition> optionConditions) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        taskSetupPage.getIfElseCheckbox().waitUntil(visible, 20000);
        taskSetupPage.getIfElseCheckbox().click();

        for (OptionCondition optionCondition : optionConditions) {
            precondition = optionCondition.getPreCondition();
            condition = optionCondition.getCondition();

            if (StringUtils.isNotEmpty(precondition)) {
                Common.pause(1000);
                taskSetupPage.getPreconditionDropdown().click();
                taskSetupPage.getPreconditionValueInDropdown(precondition).waitUntil(visible, 20000);
                taskSetupPage.getPreconditionValueInDropdown(precondition).click();
            }
            if (StringUtils.isNotEmpty(condition)) {
                Common.pause(1000);
                taskSetupPage.getConditionDropdown().click();
                taskSetupPage.getConditionValueInDropdown(condition).waitUntil(visible, 20000);
                taskSetupPage.getConditionValueInDropdown(condition).click();
            }
        }
        taskSetupPage.getIfElseActionBtn("Save").click();
    }

    @And("^I click '(.*)' for '(.*)' checkpoint in row on the TaskSetup page$")
    public void iClickXXXForXXCheckpointInRowOnTheTaskSetupPage(String option, String row) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        pause(5000);
        taskSetupPage.getOptionByRow(option, row).waitUntil(visible, 20000);
        taskSetupPage.getOptionByRow(option, row).click();
    }

    @And("^I click on plus icon for checkpoint$")
    public void iClickOnPlusIconForCheckpoint() {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        taskSetupPage.getPlusIcon().waitUntil(visible, 20000);
        taskSetupPage.getPlusIcon().click();
    }

    @And("^I add sub-checkpoint with data:$")
    public void  iAddSubCheckpointWithData(List<TaskSetup> tasksSetup) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);

        for (TaskSetup taskSetup : tasksSetup) {
            text = taskSetup.getText();
            row = taskSetup.getRow();
            type = taskSetup.getType();

            taskSetupPage.getPlusIcon().waitUntil(visible, 20000);
            taskSetupPage.getPlusIcon().click();
            pause(4000);

            if (StringUtils.isNotEmpty(text)) {
                taskSetupPage.getTextField().setValue(text);
            }
            if (StringUtils.isNotEmpty(type)) {
                taskSetupPage.getNewSubCheckpointTypeField(row).hover();
                taskSetupPage.getNewSubCheckpointTypeField(row).click();
                taskSetupPage.getTypeItem(type).waitUntil(exist, 40000);
                taskSetupPage.getTypeItem(type).waitUntil(visible, 40000);
                taskSetupPage.getTypeItem(type).hover();
                taskSetupPage.getTypeItem(type).click();
            }

            taskSetupPage.getSaveButton().waitUntil(exist, 30000);
            taskSetupPage.getSaveButton().click();
            taskSetupPage.getSaveButton().waitUntil(visible, 30000);
            pause(4000);
        }

    }


    @And("^I select '(.*)' checklist on TaskSetup page$")
    public void iSelectXXXChecklistOnTaskSetupPage(String checklistName) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);
        CommonPage commonPage = page(CommonPage.class);

        int numberOfElement = 0;
        for(int i = 0; i < taskSetupPage.getCheclistsList().size(); i++) {
            if(taskSetupPage.getCheclistsList().get(i).text().equalsIgnoreCase(checklistName)) {
                numberOfElement = i;
                break;
            }
        }
        commonPage.getRadioButtonsList().get(numberOfElement).click();
    }

    @When("I delete searched checklist on checklist setup page$")
    public void iDeleteSearchedProjectOnCommunitySetupPage() {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);
        taskSetupPage.getDeleteButton().waitUntil(exist, 60000);
        taskSetupPage.getDeleteButton().hover();
        taskSetupPage.getDeleteButton().click();
        taskSetupPage.getMarkedToDeleteItem().shouldBe(visible);
    }

    @Then("^I 'should' see '/Settings/TaskSetup' page$")
    public void iShouldSeeTaskSetupPage() {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);
        UserBasePage userBasePage = page(UserBasePage.class);
        AccountSetupPage accountSetupPage = page(AccountSetupPage.class);

        taskSetupPage.getAddButton().shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
        accountSetupPage.getProcessMenu().shouldBe(Condition.visible);
    }

    @And("^I add following attachments on the TaskSetup page:$")
    public void iAddFollowingAttachmentsOnTheTaskSetupPage(List<TaskSetup> tasksSetup) {
        TaskSetupPage taskSetupPage = page(TaskSetupPage.class);
        NoteLinkModal noteLinkModal = page(NoteLinkModal.class);
        UserBasePage userBasePage = page(UserBasePage.class);

        for (TaskSetup taskSetup : tasksSetup) {
            attachLink = taskSetup.getAttachLink();
            attachNote = taskSetup.getAttachNote();
            file = taskSetup.getFile();

            if (StringUtils.isNotEmpty(attachNote)) {
                taskSetupPage.getAttachDropDown().waitUntil(exist, 20000);
                taskSetupPage.getAttachDropDown().hover();
                taskSetupPage.getAttachDropDown().click();
                pause(2000);
                taskSetupPage.getAttachDropDownOption("Note").waitUntil(exist, 30000);
                taskSetupPage.getAttachDropDownOption("Note").hover();
                taskSetupPage.getAttachDropDownOption("Note").click();
                noteLinkModal.getTextField().setValue(attachNote);
                noteLinkModal.getSaveButton().click();
            }
            if (StringUtils.isNotEmpty(attachLink)) {
                pause(3000);
                taskSetupPage.getAttachDropDown().click();
                taskSetupPage.getAttachDropDownOption("Link").waitUntil(exist, 30000);
                taskSetupPage.getAttachDropDownOption("Link").click();
                noteLinkModal.getTextField().setValue(attachLink);
                noteLinkModal.getSaveButton().click();
            }
            if (StringUtils.isNotEmpty(file)) {
                taskSetupPage.getAttachDropDown().click();
//                taskSetupPage.getAttachDropDownOption("File").click();
                String pathToFile = String.format("src/test/resources/test_files/%s.jpg", file);
                taskSetupPage.getFileInput().waitUntil(exist, 30000);
                taskSetupPage.getFileInput().uploadFile(new File(pathToFile));
            }
        }
        pause(2000);
    }
}
