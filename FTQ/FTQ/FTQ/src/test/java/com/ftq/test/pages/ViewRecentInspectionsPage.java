package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import com.ftq.test.utils.Common;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static java.lang.Math.toIntExact;

public class ViewRecentInspectionsPage {

    private SelenideElement getDeleteButtonLocator() { return $("[data-bind*='big-common-btn']"); }
    private SelenideElement getSearchedInspectionLocator() { return $("[data-bind*='RecordID']"); }
    private SelenideElement getShowDropdownLocator() { return $(By.xpath("//a[contains(.,'Show')]")); }
    private SelenideElement getForProjectsDropdownLocator() { return $("#project_selector_chzn a"); }
    private SelenideElement getHeaderOfTableLocator() { return $(".headers"); }
    private SelenideElement getSearchFieldLocator() { return $(".search"); }
    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[contains(text(),'Inspections')]")); }
    private SelenideElement getInspectionCheckboxLocator(String value) { return $(By.xpath(String.format("//*[contains(text(), '%s')]/ancestor::tr//input[@type='checkbox']/ancestor::td/label", value)));}
    private SelenideElement getInspectionLinkLocator(String value) { return $(By.xpath(String.format("//span[@class='plain-editor']//a[contains(text(), '%s')]", value)));}

    public SelenideElement getDeleteButton() {
        return getDeleteButtonLocator();
    }

    public SelenideElement getSearchedInspection() {
        int countOfElements = toIntExact(executeJavaScript("return document.querySelectorAll(\"[data-bind*='isFreshFlag']\").length"));
        int count = 0;
        while (countOfElements != 1 && count < 10) {
            Common.pause(500);
            countOfElements = toIntExact(executeJavaScript("return document.querySelectorAll(\"[data-bind*='isFreshFlag']\").length"));
            count++;
        }
        return getSearchedInspectionLocator();
    }

    public SelenideElement getInspectionCheckbox(String value) {
        return getInspectionCheckboxLocator(value);
    }

    public SelenideElement getInspectionLink(String value) {
        return getInspectionLinkLocator(value);
    }

    public void selectSearchedInspection() {
        executeJavaScript("$('[type=\"checkbox\"]').click()");
    }

    public SelenideElement getShowDropdown() {
        return getShowDropdownLocator();
    }

    public SelenideElement getForProjectsDropdown() {
        return getForProjectsDropdownLocator();
    }

    public SelenideElement getHeaderOfTable() {
        return getHeaderOfTableLocator();
    }

    public SelenideElement getSearchField() {
        return getSearchFieldLocator();
    }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }
}
