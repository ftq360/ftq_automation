package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AttachmentsManagementPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Attachments Management']")); }
    private SelenideElement getDropdownLocator(String dropdownName) { return $(By.xpath(String.format("//span[text()='%s']", dropdownName))); }
    private SelenideElement getFileColumnLocator() { return $(By.xpath("//th[text()='File']")); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getDropdown(String dropdownName) {
        return getDropdownLocator(dropdownName);
    }

    public SelenideElement getFileLocator() {
        return getFileColumnLocator();
    }
}
