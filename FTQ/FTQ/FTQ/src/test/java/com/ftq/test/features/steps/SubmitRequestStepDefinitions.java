package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.SubmitRequestPage;
import com.ftq.test.pages.SupportFtq360Page;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class SubmitRequestStepDefinitions {

    @Then("^I 'should' see 'Submit a request' page$")
    public void iShouldSeeSubmitRequestPage() {
        SubmitRequestPage submitRequestPage = page(SubmitRequestPage.class);
        SupportFtq360Page supportFtq360Page = page(SupportFtq360Page.class);

        submitRequestPage.getFieldTitle("Your email address").waitUntil(Condition.visible, 10000);
        submitRequestPage.getFieldTitle("Your email address").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Name:").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Company:").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Phone Number:").shouldBe(Condition.visible);
       // submitRequestPage.getFieldTitle("Email Address:").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Subject").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Description").shouldBe(Condition.visible);
        submitRequestPage.getFieldTitle("Attachments").scrollTo();
        submitRequestPage.getFieldTitle("Attachments").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Community").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Submit a request").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Contact Us").shouldBe(Condition.visible);
        supportFtq360Page.getHeaderLink("Sign in").shouldBe(Condition.visible);
        supportFtq360Page.getSearchField().shouldBe(Condition.visible);
        supportFtq360Page.getLogo().shouldBe(Condition.visible);
        submitRequestPage.getSumbitButton().shouldBe(Condition.visible);
    }
}
