package com.ftq.test.features.steps;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.ftq.test.objects.User;
import com.ftq.test.pages.*;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.ftq.test.pages.UserSetupPage.getVisibleConditionUser;
import static com.ftq.test.utils.Common.*;

public class UserSetupStepDefinitions {

    private String firstName, inspectionType, lastName, password, confirmPassword, email, username, affiliation;

    @When("^I click on '(.*)' button on user setup page$")
    public void iClickOnXXXButtonOnUserSetupPage(String buttonName) {
        UserSetupPage userSetupPage = page(UserSetupPage.class);

        if (buttonName.equalsIgnoreCase("Add")) {
            Common.pause(1000);
            userSetupPage.getAddButton().waitUntil(visible, 60000);
            userSetupPage.getAddButton().click();
        }
        else if (buttonName.equalsIgnoreCase("Save")) {
            pause(3000);
            userSetupPage.getSaveButton().waitUntil(exist, 60000);
            userSetupPage.getSaveButton().waitUntil(visible, 60000);
            userSetupPage.clickSaveButton();
        }
        else if (buttonName.equalsIgnoreCase("Cancel")) {
            userSetupPage.getCancelButton().click();
        }
        else if (buttonName.equalsIgnoreCase("Send Invitation")) {
            userSetupPage.getSendInvitationButton().click();
        }
    }

    @Then("^I select '(.*)' step on user setup page$")
    public void iSelectXXXStepOnUserSetupPage(String step) {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        userSetupPage.getStepItem(step).click();
        userSetupPage.getRightPanelData().waitUntil(visible,5000);
    }

    @When("^I fill new user form with next data on user setup page:$")
    public void iFillNewUserFormWithNextDataOnUserSetupPage(List<User> users){
        UserSetupPage userSetupPage = page(UserSetupPage.class);

        for (User user : users) {
            username = user.getUsername();
            firstName = user.getFirstName();
            inspectionType = user.getInspectionType();
            lastName = user.getLastName();
            password = user.getPassword();
            confirmPassword = user.getConfirmPassword();
            email = user.getNewUserEmail();
            affiliation = user.getAffiliation();

           if (StringUtils.isNotEmpty(firstName)) {
                userSetupPage.getNewUserFirstNameField().setValue(firstName);
           }
           if (StringUtils.isNotEmpty(lastName)){
               userSetupPage.getNewUserLastNameField().setValue(lastName);
           }
           if (StringUtils.isNotEmpty(password)){
               userSetupPage.getNewUserPassword().setValue(password);
           }
           if (StringUtils.isNotEmpty(confirmPassword)) {
               userSetupPage.getConfirmNewPassword().setValue(confirmPassword);
           }
           if (StringUtils.isNotEmpty(email)) {
                userSetupPage.getNewUserEmailField().setValue(getUniqueEmail(email));
           }
           if (StringUtils.isNotEmpty(username)){
                userSetupPage.getNewUserUserName().setValue(getUniqueValue(username));
            }
            if (StringUtils.isNotEmpty(inspectionType)) {
                userSetupPage.selectNewUserInspectionType(inspectionType);
            }
           if (StringUtils.isNotEmpty(affiliation)) {
               userSetupPage.selectAffiliation(affiliation);
           }
            Common.pause(1000);
        }
    }

    @When("^I edit searched user with next data on user setup page:$")
    public void iEditSearchedUserWithNextDataOnUserSetupPage(List<User> users){
        UserSetupPage userSetupPage = page(UserSetupPage.class);

        for (User user : users) {
            password = user.getPassword();
            confirmPassword = user.getConfirmPassword();

            if (StringUtils.isNotEmpty(password)){
                userSetupPage.getUserPassword().setValue(password);
            }
            if (StringUtils.isNotEmpty(confirmPassword)) {
                userSetupPage.getConfirmPassword().setValue(confirmPassword);
            }
        }
    }

    @When("^I select searched user on user setup page$")
    public void iSelectSearchedUserOnUserSetupPage() {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        userSetupPage.getSearchedUserRadioButton().click();
    }

    @When("^I click on delete button for selected user on user setup page$")
    public void iDeleteSelectedUserOnUserSetupPage() {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        userSetupPage.getDeleteButton().click();
        userSetupPage.getMarkedToDeleteItem().shouldBe(visible);
    }

    @Then("^I '(.*)' see created user on user setup page$")
    public void iConditionSeeCreatedUserOnUserSetupPage(String condition) {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        Condition should = getVisibleConditionUser(condition);

        userSetupPage.getSearchedUser().shouldBe(should);
    }

    @Then("^I '(.*)' see user with following data on user setup page:$")
    public void iConditionSeeUserWithFollowingDataOnUserSetupPage(String condition, List<User> userList) {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        CommunitySetupPage communitySetupPage = page(CommunitySetupPage.class);

        Condition should = getVisibleCondition(condition);

        if(condition.equalsIgnoreCase("should")) {

            for (User user : userList) {
                username = user.getUsername();
                firstName = user.getFirstName();
                inspectionType = user.getInspectionType();
                lastName = user.getLastName();
                email = user.getNewUserEmail();

                userSetupPage.getUsernameField().shouldHave(attribute("value", getUniqueValue(username)));
                userSetupPage.getFirstNameField().shouldHave(attribute("value", firstName));
                userSetupPage.getLastNameField().shouldHave(attribute("value", lastName));
                userSetupPage.getEmailField().shouldHave(attribute("value", getUniqueEmail(email)));
                userSetupPage.getInspType().shouldHave(text(inspectionType));
            }
        } else {
            userSetupPage.getNoListedUsersText().shouldHave(text("There are no users listed"));
        }
    }

    @Then("^I shouldn't see deleted user on user setup page$")
    public void iShouldnTSeeDeletedUserOnUserSetupPage(){
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        userSetupPage.getEmptyMessage().shouldBe(Condition.visible);
    }

    @Then("^I 'should' see '/Settings/UserSetup' page$")
    public void iShouldSeeUserSetupPage() {
        UserBasePage userBasePage = page(UserBasePage.class);

        userBasePage.getSearchField().shouldBe(visible);

    }

    @Then("^I should see '(\\d+)' new line added on User Setup page$")
    public void iShouldSeeNewLineAddedOnUserSetupPage(int number) {
        UserSetupPage userSetupPage = page(UserSetupPage.class);
        userSetupPage.getNewUserLinesList().shouldBe(CollectionCondition.size(number));
    }


}