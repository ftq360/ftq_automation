package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ITPListPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ITPListingStepDefinitions {

    @Then("^I 'should' see '/Checklists/ItpList' page$")
    public void iShouldSeeItpListPage() {
        ITPListPage itpListPage = page(ITPListPage.class);

        itpListPage.getTitle().shouldBe(Condition.visible);
        itpListPage.getTableHeader("PercentDone", "% Done").shouldBe(Condition.visible);
        itpListPage.getTableHeader("PlanItemCode", "Code").shouldBe(Condition.visible);
        itpListPage.getTableHeader("StartDate", "Start").shouldBe(Condition.visible);
        itpListPage.getTableHeader("ScheduleDate", "Due").shouldBe(Condition.visible);
        itpListPage.getTableHeader("TaskSupplier", "Checklist").shouldBe(Condition.visible);
        itpListPage.getTableHeader("CommunityLabel", "Proj.").shouldBe(Condition.visible);
        itpListPage.getTableHeader("JobsLabel", "Phase/Loc").shouldBe(Condition.visible);
        itpListPage.getTableHeader("ObjectText", "Equip.").shouldBe(Condition.visible);
        itpListPage.getTableHeader("VendorDescriptionCustomer", "Insp. Affiliation").shouldBe(Condition.visible);
        itpListPage.getTableHeader("VendorDescriptionSupplier", "Resp. Party").shouldBe(Condition.visible);
        itpListPage.getTableHeader("CrewLabelCustomer", "Insp.").shouldBe(Condition.visible);
        itpListPage.getTableHeader("MinQuantity", "Qty.").shouldBe(Condition.visible);
        itpListPage.getTableHeader("Notes", "Notes").shouldBe(Condition.visible);
        itpListPage.getDownloadBtn().shouldBe(Condition.visible);
    }
}
