package com.ftq.test.objects;

public class Project {

    private String code;
    private String description;
    private String email;

    public String getCode() { return code; }

    public String getDescription() { return description; }

    public String getEmail() { return email; }
}
