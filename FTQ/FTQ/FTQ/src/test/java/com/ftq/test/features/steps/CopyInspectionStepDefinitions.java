package com.ftq.test.features.steps;

import com.ftq.test.modals.CopyInspectionModal;
import cucumber.api.java.en.And;

import static com.codeborne.selenide.Selenide.page;

public class CopyInspectionStepDefinitions {
    @And("^I click '(.*)' on Copy inspection modal$")
    public void iClickXXXModalOnCopyInspectionModal(String button) {
        CopyInspectionModal copyInspectionModal = page(CopyInspectionModal.class);

        switch (button) {
            case "Yes" : copyInspectionModal.getYesButton().click();
                break;
            default: throw new IllegalArgumentException("Color not found");
        }
    }
}
