package com.ftq.test.objects;

public class Report {

    private String reportName;
    private String requestDate;
    private String dateRange;

    public String getRequestDate() {
        return requestDate;
    }

    public String getDateRange() {
        return dateRange;
    }

    public String getReportName() {
        return reportName;
    }
}
