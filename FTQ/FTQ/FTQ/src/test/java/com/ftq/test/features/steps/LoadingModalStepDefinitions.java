package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.menu.TopMenu;
import com.ftq.test.modals.LoadingDataModal;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class LoadingModalStepDefinitions {

    @Then("^I should wait until 'Loading data' modal will be closed$")
    public void checkLoadingModalIsDisplayed() {
        Common.printCurrentTime();
        LoadingDataModal loadingDataModal = page(LoadingDataModal.class);

        Common.pause(3000);
        Common.waitPageLoading();
        loadingDataModal.getLoadingModal().waitUntil(Condition.disappear, 400000);
        Common.pause(3000);
        Common.waitPageLoading();
        loadingDataModal.getLoadingModal().waitUntil(Condition.disappear, 350000);
        Common.waitPageLoading();
        loadingDataModal.getLoadingModal().waitUntil(Condition.disappear, 350000);
    }

    @Then("^I should wait until data will be loaded$")
    public void checkVmIsLoaded() {
        Common.printCurrentTime();
        Common.pause(500);
        Common.waitPageLoading();
        Common.waitVMLoading();
    }
}
