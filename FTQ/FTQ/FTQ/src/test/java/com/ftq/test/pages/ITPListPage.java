package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class ITPListPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='ITP List']")); }
    private SelenideElement getTableHeaderLocator(String class_n, String name) { return  $(By.xpath(String.format("//*[contains(@id, 'itp-grid_%s')][text()='%s']", class_n, name))); }
    private SelenideElement getDownloadBtnLocator() { return $(By.cssSelector("#export-grid")); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getTableHeader(String class_n, String name) {
        return getTableHeaderLocator(class_n, name);
    }

    public SelenideElement getDownloadBtn() {
        return getDownloadBtnLocator();
    }
}
