package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ArchiveInspectionsPage {

    private SelenideElement getSearchToolbarLocator() { return $(".ui-search-toolbar"); }
    private SelenideElement getTableHeaderLocator() { return $(".ui-jqgrid-labels"); }
    private SelenideElement getDeleteButtonLocator() { return $(".big-common-btn"); }

    public SelenideElement getSearchToolbar() {
        return getSearchToolbarLocator();
    }

    public SelenideElement getTableHeader() {
        return getTableHeaderLocator();
    }

    public SelenideElement getDeleteButton() {
        return getDeleteButtonLocator();
    }
}
