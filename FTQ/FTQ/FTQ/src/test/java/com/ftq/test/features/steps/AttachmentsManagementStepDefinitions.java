package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.AttachmentsManagementPage;
import com.ftq.test.pages.DeficienciesArchivePage;
import com.ftq.test.pages.UserBasePage;
import cucumber.api.java.en.Then;

import java.util.Arrays;
import java.util.function.Consumer;

import static com.codeborne.selenide.Selenide.page;

public class AttachmentsManagementStepDefinitions {

    @Then("^I 'should' see '/Settings/Attachments' page$")
    public void iShouldSeeAttachmentsPage() {
        AttachmentsManagementPage attachmentsManagementPage = page(AttachmentsManagementPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);
        UserBasePage userBasePage = page(UserBasePage.class);

        String[] columnTitles = {"Preview", "Size (kB)", "Date", "Insp. ID", "Chkpt. ID", "Proj.-Phase/Equip./Location/Ref#",
                "Checklist", "Inspector"};
        String[] dropdownTitles = {"Newest 100", "All Projects", "All Phases", "All Checklists"};

        Consumer<String> columnTitlesStream = s -> deficienciesArchivePage.getColumnTitle(s).shouldBe(Condition.visible);
        Consumer<String> dropdownTitlesStream = s ->  attachmentsManagementPage.getDropdown(s).shouldBe(Condition.visible);

        Arrays.stream(columnTitles).forEach(columnTitlesStream);
        Arrays.stream(dropdownTitles).forEach(dropdownTitlesStream);

        attachmentsManagementPage.getTitle().shouldBe(Condition.visible);
        attachmentsManagementPage.getFileLocator().shouldBe(Condition.visible);
        userBasePage.getSearchField().shouldBe(Condition.visible);
    }
}
