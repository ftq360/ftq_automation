package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ReportQueriesPage {

    private ElementsCollection getTitleLocator() { return $$(By.xpath("//h1[text()='Queries']")); }
    private ElementsCollection getQueriesListLocator() { return $$(".option-list"); }
    private ElementsCollection getQueryIdInputFieldLocator() { return $$("#queryId"); }
    private ElementsCollection getRunButtonLocator() { return $$(".big-default-btn"); }

    public ElementsCollection getTitle() {
        return getTitleLocator();
    }

    public ElementsCollection getQueriesList() {
        return getQueriesListLocator();
    }

    public ElementsCollection getQueryIdInputField() {
        return getQueryIdInputFieldLocator();
    }

    public ElementsCollection getRunButton() {
        return getRunButtonLocator();
    }
}
