package com.ftq.test.api;

import com.ftq.test.utils.Common;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class RequestSender {

    protected static String __RequestVerificationToken;

    private static String appUrl = Common.getConfigValue("app.url");


    protected static HttpResponse sendPostSetToken(String path, String params) {

        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(appUrl + path);
        HttpResponse response = null;

        try {
            httppost.setEntity(new StringEntity(params, "UTF-8"));

            response = httpclient.execute(httppost);
            Common.pause(500);

            System.out.println(response.getStatusLine());
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    protected String sendPost(String path, String params) {

        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(appUrl + path);
        httppost.setHeader("Cookie", "__RequestVerificationToken=" + __RequestVerificationToken);
        httppost.setHeader("Content-Type", "application/json");
        HttpResponse response;

        try {
            httppost.setEntity(new StringEntity(params, "UTF-8"));

            response = httpclient.execute(httppost);
            Common.pause(500);

            System.out.println(response.getStatusLine());
            if (response.getEntity() != null) {
                return EntityUtils.toString(response.getEntity());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
