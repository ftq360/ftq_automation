package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.objects.CommonObject;
import com.ftq.test.objects.Section;
import com.ftq.test.pages.CommonPage;
import com.ftq.test.pages.InspectionPage;
import com.ftq.test.pages.UserBasePage;
import com.ftq.test.utils.Common;
import com.ftq.test.utils.GmailInbox;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;
import static com.codeborne.selenide.Condition.*;
import static com.ftq.test.utils.Common.*;
import static java.lang.Math.toIntExact;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class CommonStepDefinitions {

    private String sectionName, column, button;
    public static int yOffsetCoodinate;
    private ArrayList<String> savedTitles = new ArrayList<>();
    private ArrayList<String> refreshedTitles = new ArrayList<>();

    @Then("^I '(.*)' see '(.*)' notification$")
    public void iConditionSeeNotification(String condition, String notification) {
        UserBasePage userBasePage = page(UserBasePage.class);
        Condition should = getVisibleCondition(condition);

        userBasePage.getNotification(notification).waitUntil(should, 60000);
        userBasePage.getNotification(notification).shouldBe(should);
    }

    @When("^I enter '(.*)' in the search on (?:community setup|checklist setup|create inspection|inspections|user setup|users|sync control panel|reports|responsible party) page$")
    public void iEnterXXXInSearchOnCommunitySetupPage(String value) {
        Common.printCurrentTime();
        UserBasePage userBasePage = page(UserBasePage.class);
        userBasePage.getSearchField().waitUntil(visible, 60000);
        userBasePage.getSearchField().click();

        if (value.equals("inspection id")) {
            userBasePage.getSearchField().setValue(InspectionPage.inspectionId);
        } else if (value.equals("guid")) {
            userBasePage.getSearchField().setValue(CommonPage.guid);
        } else {
            userBasePage.getSearchField().setValue(getUniqueValue(value));
        }
        Common.pause(5000);
    }
    @When("^I enter admin username in the search on community setup page$")
    public void iEnterAdminUsernameInSearchOnCommunitySetupPage(){
        UserBasePage userBasePage = page(UserBasePage.class);
        userBasePage.getSearchField().waitUntil(visible, 60000);
        userBasePage.getSearchField().click();

        userBasePage.getSearchField().setValue(Common.getConfigValue("username"));

        Common.pause(5000);
    }

    @When("^I click X icon in search field$")
    public void iClickXIconInSearchField() {
        UserBasePage userBasePage = page(UserBasePage.class);

        userBasePage.getXIconInSearchField().waitUntil(visible, 20000);
        userBasePage.getXIconInSearchField().click();
    }

    @When("^I enter '(.*)' email in the search on responsible party page$")
    public void iEnterEmailXXXInSearchOnCommunitySetupPage(String value) {
        Common.printCurrentTime();
        UserBasePage userBasePage = page(UserBasePage.class);

        userBasePage.getSearchField().waitUntil(exist, 60000);
        userBasePage.getSearchField().waitUntil(visible, 60000);
        userBasePage.getSearchField().click();

        Common.pause(1000);
        userBasePage.getSearchField().setValue(getUniqueEmail(value));
        Common.pause(1000);
    }

    @When("I open '(.*)' inspection url")
    public void iOpenXXXInspectionUrl(String url) {
        if (url.equalsIgnoreCase("guid")) {
            open(CommonPage.url);
        }
    }

    @Then("^I should not see ready to save item$")
    public void iShouldNotSeeReadyToSaveItem() {
        UserBasePage userBasePage = page(UserBasePage.class);
        userBasePage.getItemToSave().waitUntil(hidden, 60000);
    }

    @Then("^I should see '(.*)' Save button$")
    public void iShouldSeeXXXSaveButton(String buttonColor) {
        UserBasePage userBasePage = page(UserBasePage.class);
        if (buttonColor.equalsIgnoreCase("blue")) {
            userBasePage.getBlueSaveButton().waitUntil(visible, 60000);
            userBasePage.getBlueSaveButton().shouldBe(visible);
        } else {
            userBasePage.getGreySaveButton().waitUntil(visible, 90000);
            userBasePage.getGreySaveButton().shouldBe(visible);
        }
    }

    @Then("^I '(.*)' see '(.*)' toast message$")
    public void iShouldSeeXXXToastMessage(String condition, String notification) {
        UserBasePage userBasePage = page(UserBasePage.class);
        Condition should = getVisibleCondition(condition);
        if (notification.equalsIgnoreCase("Save")) {
            userBasePage.getSaveNotification().waitUntil(should, 60000);
            userBasePage.getSaveNotification().shouldBe(should);
        } else {
            userBasePage.getToastNotification().waitUntil(should, 60000);
            userBasePage.getToastNotification().shouldBe(should);
        }
    }

    @Then("^I '(.*)' see '(.*)' header$")
    public void iConditionSeeXXXHeader(String condition, String headerName) {
        CommonPage commonPage = page(CommonPage.class);
        Condition should = getVisibleCondition(condition);
        if (headerName.equalsIgnoreCase("ITP Plan Completion Dashboard")) {
            commonPage.getDashboardHeader(headerName).shouldBe(should);
        }
    }

    @Then("^I '(.*)' see '(.*)' section on (?:open item dashboard|inspection dashboard|open itp dashboard|responsible party performance dashboard)$")
    public void iConditionSeeXXXSectionDashboard(String condition, String sectionName) {
        CommonPage commonPage = page(CommonPage.class);
        Condition should = getVisibleCondition(condition);
        commonPage.getSectionName(sectionName).shouldBe(should);
    }

    @Then("^I '(.*)' see following sections on (?:open item dashboard|inspection dashboard|open itp dashboard|responsible party performance dashboard):$")
    public void iConditionSeeFollowingSectionOnDashboard(String condition, List<Section> sections) {
        CommonPage commonPage = page(CommonPage.class);
        Condition should = getVisibleCondition(condition);

        sections.stream().forEach(s -> commonPage.getSectionName(s.getSectionName()).shouldBe(should));
    }

    @When("^I save inspection GUID$")
    public void iSaveInspectionGUID() {
        final Pattern patter = Pattern.compile("(?<=guid=)(.*)(?=&)");
        final Matcher matcher = patter.matcher(url());

        if(CommonPage.guids.size() > 2){
            CommonPage.guids.clear();
        }
        CommonPage.guid = null;
        matcher.find();
        CommonPage.guid = matcher.group(1);
        CommonPage.url = url();
        CommonPage.guids.add(matcher.group(1));
        for(String guid : CommonPage.guids) {
            System.out.println(guid);
        }
    }

    @When("^I open following url '(.*)'$")
    public void iOpenFollowingUrlXXX(String url) {
        if (url.substring(0, 1).equals("/")) {
            String fullUrl = Common.getConfigValue("app.url") + url;
            open(fullUrl);
        } else {
            open(url);
        }

    }

    @When("^I select '(\\d+)' item radio button$")
    public void iSelectItemRadioButton(int itemNumber) {
        CommonPage commonPage = page(CommonPage.class);
        commonPage.getRadioButtonsList().get(itemNumber).waitUntil(visible, 30000);
        commonPage.getRadioButtonsList().get(itemNumber).click();
    }


    @Then("^I 'should' see following (columns|buttons):$")
    public void iShouldSeeFollowingColumns(String itemName, List<CommonObject> commonObjects) {
        CommonPage commonPage = page(CommonPage.class);

        for (CommonObject commonObject : commonObjects) {

            if (itemName.equalsIgnoreCase("columns")) {
                column = commonObject.getColumn();

                if (StringUtils.isNotEmpty(column)) {
                    if (column.equalsIgnoreCase("Value")) {
                        commonPage.getSpecificColumnTitle(column).waitUntil(Condition.visible, 60000);
                        commonPage.getSpecificColumnTitle(column).shouldBe(Condition.visible);
                    } else {
                        commonPage.getColumnTitle(column).waitUntil(Condition.visible, 60000);
                        commonPage.getColumnTitle(column).shouldBe(Condition.visible);
                    }
                }
            } else if (itemName.equalsIgnoreCase("buttons")) {
                button = commonObject.getButton();

                if (StringUtils.isNotEmpty(button)) {
                    commonPage.getButton(button).shouldBe(Condition.visible);
                }
            }
        }
    }

    @When("^I refresh page$")
    public void iRefreshPage() {
        pause(2000);
        refreshPage();
        pause(4000);
    }

    @When("^I wait '(.*)' seconds$")
    public void iWaitSeconds(int second) {
        pause(second*1000);
    }

    @When("^I delete all mails$")
    public void deleteAllMails() {
        GmailInbox gmailInbox = new GmailInbox();
        gmailInbox.deleteAllEmails();
    }

    @Then("^I 'should' see mail with subject '(.*)'$")
    public void iShouldSeeMailWithSubject(String subject) {
        GmailInbox gmailInbox = new GmailInbox();

        HashMap data = gmailInbox.read();

        assertThat(data.get("subject"), equalTo(subject));
    }

    @When("^I click on refresh button$")
    public void iClickOnRefreshButton() {
        CommonPage commonPage = page(CommonPage.class);
        commonPage.getRefreshButton().scrollTo().click();
    }

    @When("^I save all titles from page$")
    public void iSaveAllTitlesFromPage() {
        CommonPage commonPage = page(CommonPage.class);
        savedTitles.clear();
        for (int i = 0; i < commonPage.getTitles().size(); i++) {
            savedTitles.add(commonPage.getTitles().get(i).getText());
        }
    }

    @Then("^I should see saved titles$")
    public void iShouldSeeSavedTitles() {
        CommonPage commonPage = page(CommonPage.class);
        refreshedTitles.clear();
        for (int i = 0; i < commonPage.getTitles().size(); i++) {
            refreshedTitles.add(commonPage.getTitles().get(i).getText());
        }

        for (int i = 0; i < savedTitles.size(); i++) {
            for (int j = 0; j < refreshedTitles.size(); j++) {
                if (i == j) {
                    continue;
                }
            }
        }
    }

    @When("^I save YOffset coordinate$")
    public void iSaveYOffsetCoordinate() {
        InspectionPage inspectionPage = page(InspectionPage.class);
        yOffsetCoodinate = toIntExact(executeJavaScript("return window.pageYOffset")) - inspectionPage.getTopBorderSize();
    }


    @Then("^I should see following notification:$")
    public void IShouldSeeFollowingNotification(String message) {
        CommonPage commonPage = page(CommonPage.class);

        commonPage.getGlobalNotification().shouldHave(text(message));
        if(message.equalsIgnoreCase("Could not save. User with that username already exists on the FTQ360 system.")) {
            commonPage.getGlobalNotification().click();

        }
    }

    @When("^I click on '(.*)' button on confirmation modal$")
    public void iClickOnXXXButtonOnDeleteModal(String buttonName){
        CommonPage commonPage = page(CommonPage.class);
        commonPage.getActionModalButton(buttonName).click();
    }

    @When("^I wait until loading spinner is disappear$")
    public void iWaitUntilLoadingSpinnerIsDisappear() {
        CommonPage commonPage = page(CommonPage.class);
        commonPage.getLoadingSpinner().waitUntil(Condition.disappear, 400000);
    }
}
