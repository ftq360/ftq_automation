package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class ITPSchedulerPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='ITP Scheduling Timeline']")); }
    private SelenideElement getAssignBtnLocator() { return $(By.cssSelector(".big-common-btn")); }
    private SelenideElement getStatusDropdownLocator() { return $(By.xpath("//*[contains(@id, 'mode_selector')][contains(@class, 'container-single')]")); }
    private SelenideElement getProjectDropdownLocator() { return $(By.xpath("//*[contains(@id, 'project_selector')][contains(@class, 'container-single')]")); }
    private SelenideElement getSearchFieldLocator() { return $(By.cssSelector("#select-menu-filter")); }
    private SelenideElement getMainTableHeaderLocator(String name, String locator) { return $(By.xpath(String.format("//*[@data-column-id='%s'][text()='%s']", locator, name))); }
    private SelenideElement getTableWithDataLocator() { return $(By.cssSelector(".gantt_data_area")); }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getAssignBtn() {
        return getAssignBtnLocator();
    }

    public SelenideElement getStatusDropdown() {
        return getStatusDropdownLocator();
    }

    public SelenideElement getProjectDropdown() {
        return getProjectDropdownLocator();
    }

    public SelenideElement getSearchField() {
        return getSearchFieldLocator();
    }

    public SelenideElement getMainTableHeader(String name, String locator) {
        return getMainTableHeaderLocator(name, locator);
    }

    public SelenideElement getTableWithData() {
        return getTableWithDataLocator();
    }

}
