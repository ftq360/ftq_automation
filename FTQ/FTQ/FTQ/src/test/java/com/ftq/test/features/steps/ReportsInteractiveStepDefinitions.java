package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ReportsInteractivePage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Selenide.page;

public class ReportsInteractiveStepDefinitions {

    @Then("^I 'should' see '/Reports/Interactive' page$")
    public void iShouldSeeReportsInteractivePage() {
        ReportsInteractivePage reportsInteractivePage = page(ReportsInteractivePage.class);

        reportsInteractivePage.getTitle().shouldBe(Condition.visible);
        reportsInteractivePage.getReportsOptionList().shouldBe(Condition.visible);
    }

    @When("^I select report by '(.*)' on interactive reports page$")
    public void iSelectReportByXXXOnInteractiveReportsPage(String reportSelection) {
        ReportsInteractivePage reportsInteractivePage = page(ReportsInteractivePage.class);

        reportsInteractivePage.getReportBy(reportSelection).click();
    }

    @When("^I click on '(.*)' button on interactive reports page$")
    public void iClickOnXXXButtonOnInteractiveReportsPage(String button) {
        ReportsInteractivePage reportsInteractivePage = page(ReportsInteractivePage.class);

        if(button.equalsIgnoreCase("Select All")) {
            reportsInteractivePage.getSelectAllButton().click();
        }
    }
}
