package com.ftq.test.objects;

public class OptionCondition {

    private String precondition;
    private String condition;

    public String getPreCondition() { return precondition; }
    public String getCondition() { return condition; }
}
