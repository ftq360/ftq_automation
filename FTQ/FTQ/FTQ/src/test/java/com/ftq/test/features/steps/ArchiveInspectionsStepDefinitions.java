package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.ArchiveInspectionsPage;
import com.ftq.test.pages.DeficienciesArchivePage;
import com.ftq.test.pages.ViewRecentInspectionsPage;
import cucumber.api.java.en.Then;

import static com.codeborne.selenide.Selenide.page;

public class ArchiveInspectionsStepDefinitions {

    @Then("^I 'should' see '/Inspections/Archive' page$")
    public void iShouldSeeInspectionsArchivePage() {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        ArchiveInspectionsPage archiveInspectionsPage = page(ArchiveInspectionsPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);

        viewRecentInspectionsPage.getTitle().shouldBe(Condition.visible);
        viewRecentInspectionsPage.getShowDropdown().shouldBe(Condition.visible);
        archiveInspectionsPage.getDeleteButton().shouldBe(Condition.visible);
        archiveInspectionsPage.getSearchToolbar().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. ID").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Last Activity Date").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. Date").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Communication/Notes").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Proj").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Phase/Location/Ref#").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Equip.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Resp. Party/Crew").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Checklist").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("First Insp.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Last Insp.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. Type").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Status").shouldBe(Condition.visible);
    }
}
