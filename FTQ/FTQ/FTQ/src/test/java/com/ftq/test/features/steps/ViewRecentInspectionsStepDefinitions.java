package com.ftq.test.features.steps;

import com.codeborne.selenide.Condition;
import com.ftq.test.pages.DeficienciesArchivePage;
import com.ftq.test.pages.InspectionPage;
import com.ftq.test.pages.ViewRecentInspectionsPage;
import com.ftq.test.utils.Common;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Selenide.page;
import static com.ftq.test.pages.UserBasePage.getVisibleCondition;

public class ViewRecentInspectionsStepDefinitions {

    @When("^I select searched item on view recent inspections page$")
    public void iSelectSearchedInspectionOnViewRecentInspectionsPage() {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        viewRecentInspectionsPage.selectSearchedInspection();
    }

    @When("^I open searched item on view recent inspections page$")
    public void iOpenSearchedItemInViewRecentInspectionsPage() {
        Common.printCurrentTime();
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        viewRecentInspectionsPage.getSearchedInspection().waitUntil(Condition.exist, 5000);
        viewRecentInspectionsPage.getSearchedInspection().click();
    }

    @When("^I open inspection by remembered (?:id|guid) on create inspections page$")
    public void iOpenInspectionByRememberedXXXOnCreateInspectionPage() {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        viewRecentInspectionsPage.getInspectionLink(InspectionPage.inspectionId).waitUntil(Condition.exist, 5000);
        viewRecentInspectionsPage.getInspectionLink(InspectionPage.inspectionId).click();
    }

    @When("^I select (.*) item on (?:view recent inspections|sync control panel) page$")
    public void iSelectSearchedItemInViewRecentInspectionsPage(String value) {
        Common.printCurrentTime();

        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        viewRecentInspectionsPage.getInspectionCheckbox(InspectionPage.inspectionId).waitUntil(Condition.exist, 5000);
        viewRecentInspectionsPage.getInspectionCheckbox(InspectionPage.inspectionId).click();
    }

    @When("^I click on '(.*)' button on view recent inspections page$")
    public void iClickXXXButtonOnViewRecentInspectionsPage(String buttonName) {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);

        if(buttonName.equalsIgnoreCase("delete")) {
            viewRecentInspectionsPage.getDeleteButton().click();
        }
    }

    @Then("^I '(.*)' see recently created inspection on (?:view recent inspections|deficiencies) page$")
    public void iConditionSeeRecentlyCreatedInspectionOnViewRecentInspectionPage(String condition) {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        Condition should = getVisibleCondition(condition);

        viewRecentInspectionsPage.getSearchedInspection().waitUntil(should, 10000);
        viewRecentInspectionsPage.getSearchedInspection().shouldBe(should);
    }

    @Then("^I '(.*)' see '(.*)' dropdown on view recent inspections page$")
    public void iConditionSeeXXXDropdownOnViewRecentInspectionPage(String condition, String dropdownName) {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        Condition should = getVisibleCondition(condition);

        if(dropdownName.equalsIgnoreCase("show")) {
            viewRecentInspectionsPage.getShowDropdown().shouldBe(should);
        } else if(dropdownName.equalsIgnoreCase("for project")) {
            viewRecentInspectionsPage.getForProjectsDropdown().shouldBe(should);
        }
    }

    @Then("^I '(.*)' see '(.*)' button on view recent inspections page$")
    public void iConditionSeeXXXButtonOnViewRecentInspectionsPage(String condition, String buttonName) {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        Condition should = getVisibleCondition(condition);

        if(buttonName.equalsIgnoreCase("delete")) {
            viewRecentInspectionsPage.getDeleteButton().shouldBe(should);
        }
    }

    @Then("^I '(.*)' see header of table on view recent inspections page$")
    public void iConditionSeeXXXHeaderOfTableOnViewRecentInspectionsPage(String condition) {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        Condition should = getVisibleCondition(condition);

        viewRecentInspectionsPage.getHeaderOfTable().shouldBe(should);
    }

    @Then("^I 'should' see '/Inspections' page$")
    public void iShouldSeeViewRecentInspectionsPage() {
        ViewRecentInspectionsPage viewRecentInspectionsPage = page(ViewRecentInspectionsPage.class);
        DeficienciesArchivePage deficienciesArchivePage = page(DeficienciesArchivePage.class);

        viewRecentInspectionsPage.getTitle().shouldBe(Condition.visible);
        viewRecentInspectionsPage.getSearchField().shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. ID").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Last Activity").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Insp. Date").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Status").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Checklist").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Communication/Notes").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Resp. Party/Crew").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Proj./Phase/Equip.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("First Insp.").shouldBe(Condition.visible);
        deficienciesArchivePage.getColumnTitle("Last Insp.").shouldBe(Condition.visible);
        viewRecentInspectionsPage.getDeleteButton().shouldBe(Condition.visible);
        viewRecentInspectionsPage.getForProjectsDropdown().shouldBe(Condition.visible);
        viewRecentInspectionsPage.getShowDropdown().shouldBe(Condition.visible);
    }
}
