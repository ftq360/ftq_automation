package com.ftq.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ResponsiblePartySetupPage {

    private ElementsCollection getResponsiblePartiesLocator() { return $$("[data-bind*='VendorCode']"); }
    private ElementsCollection getRadioButtonsLocator() { return $$("[type='radio']"); }
    private SelenideElement getUsernameLocator() { return $("span[data-bind*='UserName']");}
    private SelenideElement getFirstNameLocator() { return $(By.xpath("//*[contains(@data-bind,'FirstName')]"));}
    private SelenideElement getLastNameLocator() { return $(By.xpath("//*[contains(@data-bind,'LastName')]"));}
    private SelenideElement getEmailLocator() { return $("textarea[data-bind*='Email']");}
    private SelenideElement getNoListedUsersTextLocator() { return $("[data-bind*='emptyText']"); }
    private SelenideElement getInspTypeLocator() { return $(By.xpath("//select[contains(@data-bind,'InspectorType')]/..//span")); }

    public void selectResposibleParty(String partyName) {
        for(int i = 0; i < getResponsiblePartiesLocator().size(); i++) {
            if(getResponsiblePartiesLocator().get(i).val().equalsIgnoreCase(partyName)) {
                getRadioButtonsLocator().get(i).click();
            }
        }
    }

    public SelenideElement getUsernameField() {
        return getUsernameLocator();
    }

    public SelenideElement getFirstNameField() {
        return getFirstNameLocator();
    }

    public SelenideElement getLastNameField() {
        return getLastNameLocator();
    }

    public SelenideElement getEmailField() {
        return getEmailLocator();
    }

    public SelenideElement getNoListedUsersText() {
        return getNoListedUsersTextLocator();
    }

    public SelenideElement getInspType() { return getInspTypeLocator(); }
}
