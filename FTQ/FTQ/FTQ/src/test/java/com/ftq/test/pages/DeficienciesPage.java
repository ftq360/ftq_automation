package com.ftq.test.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DeficienciesPage {

    private SelenideElement getTitleLocator() { return $(By.xpath("//h1[text()='Recent Deficiency List']")); }
    private SelenideElement getColumnTitleLocator(String title) { return $(By.xpath(String.format("//th[contains(.,'%s')]", title)));}
    private SelenideElement getDeficiencyIdLocator() { return $("[data-bind*='DataCollScoreChkPntID']"); }
    private SelenideElement getInspectionIdLocator() { return $("[data-bind*='text:$parent.DataRecordID']"); }

    public SelenideElement getColumnTitle(String title) {
        return getColumnTitleLocator(title);
    }

    public SelenideElement getTitle() {
        return getTitleLocator();
    }

    public SelenideElement getDeficiencyId() {
        return getDeficiencyIdLocator();
    }

    public SelenideElement getInspectionId() {
        return getInspectionIdLocator();
    }
}
