@P2
Feature: P2
  Scenario: Dynamic checkpoints
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I should see 'Concrete-Checkpoint Test' inspection on inspection page
    When I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Enter Punchlist Item' button on inspection page
    Then I wait '3' seconds
    Then I 'should' see 'Punch intem deficiency:' checkpoint on Inspection page
    When I click on 'Undo for Enter Punchlist Item' button on inspection page
    Then I wait '3' seconds
    Then I 'should not' see 'Punch intem deficiency:' checkpoint on Inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Icon colors
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    And I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Pass' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'green' icon for 'Checklist' on the Inspections page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Incomplete' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'grey' icon for 'Checklist' on the Inspections page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Fail' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'red' icon for 'Checklist' on the Inspections page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Draft' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'red' icon for 'Checklist' on the Inspections page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Complete (w/Open)' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'red2' icon for 'Checklist' on the Inspections page
    And I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Report Complete' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I should see 'green2' icon for 'Checklist' on the Inspections page
    When I click on 'FTQ360' button in top menu
    And I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text        | category |
      | Checklist2  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist2' in the search on inspections page
    Then I should see 'blue' icon for 'Checklist2' on the Inspections page
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist2' in the search on checklist setup page
    And I select 'Checklist2' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal


  Scenario: Icon color for last created inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text                   | category |
      | TestProdWithAllCommon  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I uncheck 'Plan mode' if it checked
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'TestProdWithAllCommon' in the search on inspections page
    And I select 'TestProdWithAllCommon' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Pass' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'TestProdWithAllCommon' in the search on inspections page
    Then I should see 'green' icon for 'TestProdWithAllCommon' on the Inspections page
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text                    | category |
      | TestProdWithAllCommon1  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'TestProdWithAllCommon1' in the search on inspections page
    And I select 'TestProdWithAllCommon1' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Fail (final)' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Saved to device' notification appears on inspection page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'TestProdWithAllCommon1' in the search on inspections page
    Then I should see 'red' icon for 'TestProdWithAllCommon1' on the Inspections page


  Scenario: Attachment notes
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I wait '1' seconds
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '10' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I click on 'Show All' button on inspection page
    And I comment each attachment with 'comment' text on inspection page
    And I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '20' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I comment each attachment with 'comment' text on inspection page
    And I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '30' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I comment each attachment with 'comment' text on inspection page
    And I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '40' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I comment each attachment with 'comment' text on inspection page
    And I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '50' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I comment each attachment with 'comment' text on inspection page
    And I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint                  |
      | test_image | Safety vests are being worn |
    Then I should see '60' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I comment each attachment with 'comment' text on inspection page
    And I refresh page
    Then I should see '60' attachments in 'Safety vests are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Dynamic matrix
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-test4444444' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'dynamic matrix' button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value   |
      | 1           | value1  |
      | 2           | value2  |
    Then I should see 'Synced to server' text instead of save button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I clear Dynamic Matrix fields on inspection page:
      | fieldNumber |
      | 1           |
      | 2           |
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value   |
      | 2           | value2  |
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I refresh page
    And I wait '5' seconds
    Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value   |
      | 2           | value2  |
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Plan mode checkbox filters the selection Checklists ITP
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code           | description    |
      | itpProjectCode | itptestProject |
    And I enter 'itptestProject' in the search on community setup page
    And I wait '5' seconds
    And I click on 'Save' button on community setup page
    And I wait '10' seconds
    When I select searched item on community setup page
    And I select 'All ITP' in process menu
    And I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code        | checklist            |
      | itpPlanCode | Concrete-test4444444 |
    And I click on 'Save' button on community setup page
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I enter 'itptestProject' in the search on create inspection page
    And I select 'itptestProject' division on create inspection page
    And I check 'Plan mode' checkbox on create inspection page
    Then I should see itp plan item with following data on create inspection page:
      | code        | checklist            |
      | itpPlanCode | Concrete-test4444444 |
    When I open following url '/Settings/CommunitySetup'
    And I select 'Project' in process menu
    And I enter 'itptestProject' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description    |
      | itptestProject |
    When I delete searched project on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Ok' button on delete modal
    Then I should see 'grey' Save button


  Scenario: Inspection copy
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I uncheck 'Plan mode' if it checked
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Hardhats are being worn |
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Copy' button on inspection page
    And I click 'Yes' on Copy inspection modal
      Then I should see 'Synced to server' text instead of save button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    And  I should see Notes field filled with 'note' text on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    And I should see checked 'OPEN' checkbox for 'Safety vests are being worn' checkpoint on inspection page
    And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Back to original' button on inspection page
    Then I should see Notes field filled with 'note' text on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    And I should see checked 'OPEN' checkbox for 'Safety vests are being worn' checkpoint on inspection page
    And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    And I open searched item on view recent inspections page
    Then I should see Notes field filled with 'note' text on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Restricted User can login and inspect
    Given I login as restricted user
    Then I should wait until 'Loading data' modal will be closed
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup ifElseCheckpoint' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I wait '10' seconds
    And I click on 'FTQ' checkbox in 'Test1' checkpoint on inspection page
    Then I 'should' see 'Test2' checkpoint on Inspection page
    And I click on 'OPN' checkbox in 'Test1' checkpoint on inspection page
    Then I 'should not' see 'Test2' checkpoint on Inspection page
    And I fill 'Test1' checkpoint with 'pre-setup test3' text on inspection page
    Then I 'should' see 'Test3' checkpoint on Inspection page
    And I fill 'Test1' checkpoint with 'pre-setup test4' text on inspection page
    Then I 'should not' see 'Test3' checkpoint on Inspection page
    And I fill 'Notes' field with 'note' text on inspection page
      And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
      And I should see 'Synced to server' text instead of save button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup MatrixFormula' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'Round' button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value           |
      | 1           | pre-setup 1.217 |
    Then I should see 'Synced to server' text instead of save button on inspection page
    Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value           |
      | 2           | pre-setup 1     |
      | 3           | pre-setup 1     |
      | 4           | pre-setup 1.22  |
      | 5           | pre-setup 1.217 |
    And I click on 'Floor' button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value           |
      | 6           | pre-setup 2.919 |
    Then I should see 'Synced to server' text instead of save button on inspection page
    Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value           |
      | 7           | pre-setup 2     |
      | 8           | pre-setup 2     |
      | 9           | pre-setup 2.91  |
      | 10          | pre-setup 2.919 |
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I click on 'Max' button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value         |
      | 11          | pre-setup  11 |
    Then I should see 'Synced to server' text instead of save button on inspection page
    Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value        |
      | 12          | pre-setup 11 |
      | 13          | pre-setup 11 |
      | 14          | pre-setup 11 |
      | 15          | pre-setup 20 |
    Then I should see 'Synced to server' text instead of save button on inspection page
#    When I click on 'Delete' button on inspection page
#    And I click on 'Yes' button on delete modal


  Scenario: References
    Given I login as admin user using url '/Settings/TaskSetup'
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I enter 'Checklist' in the search on checklist setup page
    And I add following attachments on the TaskSetup page:
      | attachLink                 | attachNote | file       |
      | https://devtest.ftq360.net | test note  | test_image |
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code            | description | email                 |
      | testProjectCode | testProject | test_project@mail.com |
    And I enter 'testProject' in the search on community setup page
    And I click on 'Save' button on community setup page
    And I add following attachments on the TaskSetup page:
      | attachLink                 | attachNote | file       |
      | https://devtest.ftq360.net | test note  | test_image |
    Then I should not see ready to save item
    And I 'should' see next project in he list on community setup page:
      | description | email                 |
      | testProject | test_project@mail.com |
    When I select searched item on community setup page
    And I select 'Phase' in process menu
    And I click on 'Add' button on community setup page
    And I fill new phase form with next data on community setup page:
      | code          | phaseName |
      | testPhaseCode | testPhase |
    And I enter 'testPhase' in the search on community setup page
    And I click on 'Save' button on community setup page
    And I add following attachments on the TaskSetup page:
      | attachLink                 | attachNote | file       |
      | https://devtest.ftq360.net | test note  | test_image |
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Yes' button on confirmation modal
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I enter 'testProject' in the search on inspections page
    And I select 'testProject' project on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I 'should' see 'Checklist' project on create inspection page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click 'Checklist References' on Inspection page
    Then I should see '3' as amount of attachments
    When I click 'Project References' on Inspection page
    Then I should see '6' as amount of attachments
    When I click 'Phase References' on Inspection page
    Then I should see '9' as amount of attachments
    When I open following url '/Settings/CommunitySetup'
    And I select 'Project' in process menu
    And I enter 'testProject' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description |
      | testProject |
    When I delete searched project on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Ok' button on delete modal
    Then I should see 'grey' Save button
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist' in the search on checklist setup page
    And I select 'Checklist' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal

  Scenario: Autosave in offline
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
      Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup Clean Project' project on create inspection page
      And I select 'pre-setup Buiding One' phase on create inspection page
      And I uncheck 'Plan mode' if it checked
      And I select 'pre-setup Location323' location on create inspection page
      And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
      And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
      And I fill 'Location' field with 'Location 22222 (1-2)' text on inspection page
      And I fill 'Notes' field with 'note' text on inspection page
      And I select override checkbox for status on the 'Inspection' page
      And I type 'current plus 2' Due Date on the 'Inspection' page
      And I select 'Pass' status on the 'Inspection' page
      And I click on 'FAILED' checkbox in 'Hardhats are being worn' checkpoint on inspection page
      And I attach file to checkpoint on inspection page:
        | fileName   | checkpoint              |
        | test_image | Hardhats are being worn |
      And I fill following fields for 'Hardhats are being worn' checkpoint with following data on inspection page:
        | fieldName               | value                    |
        | Enter Observation       | observation              |
        | Corrective Action Notes | actionNote               |
        | Ready for review        | true                     |
        | Responsible Party       | pre-setup test rp        |
        | Due Date                | first day of next month  |
      And I wait '21' seconds
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I refresh page
    Then I should wait until 'Loading data' modal will be closed
    Then I should see following data in 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Ready for review        | true                     |
      | Corrective Action Notes | actionNote               |
      | Responsible Party       | pre-setup test rp        |
      | Due Date                | first day of next month  |
      And I should see Location field filled with 'Location 22222 (1-2)' text on inspection page
      And I should see Notes field filled with 'note' text on inspection page
      And I should see 'Pass' status on inspection page
      And I should see checked 'FAILED' checkbox for 'Hardhats are being worn' checkpoint on inspection page
      And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Signal icon' top menu item
      And I select 'Offline' item in appeared top menu dropdown
      And I fill 'Location' field with 'Location323 (1-1)' text on inspection page
      And I fill 'Notes' field with 'note test' text on inspection page
      And I select override checkbox for status on the 'Inspection' page
      And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
      And I click 'Yes' on Change checkpoint status modal
      And I attach file to checkpoint on inspection page:
        | fileName   | checkpoint              |
        | test_image | Hardhats are being worn |
      And I fill following fields for 'Hardhats are being worn' checkpoint with following data on inspection page:
        | fieldName               | value                        |
        | Enter Observation       | observation test             |
        | Corrective Action Notes | actionNote test              |
        | Responsible Party       | pre-setup description01 (V8) |
        | Due Date                | first day of next month      |
      And I wait '21' seconds
    Then I should wait until 'Synced to device' notification appears on inspection page
    When I refresh page
    Then I should see following data in 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               | value                          |
      | Enter Observation       | observation test               |
      | Ready for review        | true                           |
      | Corrective Action Notes | actionNote test                |
      | Responsible Party       | pre-setup description01 (V8)   |
      | Due Date                | first day of second next month |
      And I should see Location field filled with 'Location323 (1-1)' text on inspection page
      And I should see Notes field filled with 'note test' text on inspection page
      And I should see 'Incomplete' status on inspection page
      And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
      And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page


  Scenario: Attachment to dynamic checkpoint
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I uncheck 'Plan mode' if it checked
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'Enter Punchlist Item' button on inspection page
    Then I 'should' see 'Punch intem deficiency:' checkpoint on Inspection page
    When I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Punch intem deficiency: |
    And I wait '21' seconds
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I refresh page
    Then I should see attached file to 'Punch intem deficiency:' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Checklist status override
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist  | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I uncheck 'Plan mode' if it checked
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    And I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Pass' status on the 'Inspection' page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I refresh page
    Then I should see 'Pass' status on inspection page
    When I click on 'OPEN' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see 'Pass' status on inspection page
    When I select override checkbox for status on the 'Inspection' page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I refresh page
    Then I should see 'Incomplete (w/Open)' status on inspection page
    When I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I click 'Yes' on Change checkpoint status modal
    Then I should see 'Complete (w/Open)' status on inspection page
    When I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click 'Yes' on Change checkpoint status modal
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I refresh page
    Then I should see 'Pass' status on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I wait '22' seconds
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist' in the search on checklist setup page
    And I select 'Checklist' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal


  Scenario: Deleting Inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I uncheck 'Plan mode' if it checked
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should not' see recently created inspection on view recent inspections page
    When I click on 'FTQ360' button in top menu
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I select inspection id item on view recent inspections page
    And I click on 'delete' button on view recent inspections page
    And I click on 'Ok' button on delete modal
    Then I 'should not' see recently created inspection on view recent inspections page

  Scenario: Risk factor
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
      | fieldName         | value       |
      | Risk Factor       | empty       |
      | Enter Observation | observation |
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName   | value  |
      | Risk Factor | 1      |
    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
      | fieldName         | value       |
      | Risk Factor       | -1          |
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 1     |
    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
      | fieldName         | value       |
      | Risk Factor       | 42          |
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 42    |
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I click 'Yes' on Change checkpoint status modal
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I refresh page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 42    |

  Scenario: Offline Location Selection
    Given I login as admin user using url '/Settings/CommunitySetup'
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code            | description | email                 |
      | testProjectCode | testProject | test_project@mail.com |
    And I enter 'testProject' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should not see ready to save item
    And I 'should' see next project in he list on community setup page:
      | description | email                 |
      | testProject | test_project@mail.com |
    When I select searched item on community setup page
    And I select 'All Locations' in process menu
    And I click on 'Add' button on community setup page
    And I fill new location form with next data on community setup page:
      | code             | locationName |
      | testLocationCode | testLocation |
    And I enter 'testLocation' in the search on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Add' button on community setup page
    And I fill new location form with next data on community setup page:
      | code               | locationName       |
      | testSecondLocation | testSecondLocation |
    And I enter 'testPhase' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    Then I should not see ready to save item
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'testProject' project on create inspection page
    And I select 'testSecondLocation' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I should see Location field filled with 'testSecondLocation' text on inspection page

  Scenario: Offline Equipment Selection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I wait '5' seconds
    And I select 'Project' in process menu
    And I wait '5' seconds
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code            | description      | email                 |
      | testProjectCode | testProjectEquip | test_project@mail.com |
    And I enter 'testProjectEquip' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should not see ready to save item
    And I 'should' see next project in he list on community setup page:
      | description      | email                 |
      | testProjectEquip | test_project@mail.com |
    When I select searched item on community setup page
    And I select 'All Equipment' in process menu
    And I click on 'Add' button on community setup page
    And I fill new equipment form with next data on community setup page:
      | code             | equipmentName    |
      | testOneEquipment | testOneEquipment |
    And I enter 'testSecondEquipment' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    Then I should not see ready to save item
    When I click on 'Add' button on community setup page
    And I fill new equipment form with next data on community setup page:
      | code                | equipmentName       |
      | testSecondEquipment | testSecondEquipment |
    And I enter 'testSecondEquipment' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    Then I should not see ready to save item
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text            | category | type                |
      | Checklist Equip | Concrete | Equipment Checklist |
    And I click 'Save' button on the TaskSetup page
    Then I should see 'grey' Save button
    When I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I select 'pre-setup (Undefined)' division on create inspection page
      And I wait '5' seconds
    And I select 'testProjectEquip' project on create inspection page
    And I select 'Checklist Equip' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select 'testSecondEquipment' equipment on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    Then I should wait until 'Synced to device' notification appears on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Online' item in appeared top menu dropdown
    And I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Common checkpoints
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup TestWithoutCommon' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see 'Test Checkpoint' checkpoint on Inspection page
    Then I 'should not' see 'Hardhats are being worn' checkpoint on Inspection page
    Then I 'should not' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'Add New' for "Communications" action
    And I click on 'Add' button in Communication modal window
    And I click on "Communications" dropdown under inspection headers
    And I click on "Inspection Id" in Communications opened dropdown
    Then I 'should' see 'Test Checkpoint Communication' checkpoint on Inspection page
    Then I 'should not' see 'Hardhats are being worn' checkpoint on Inspection page
    Then I 'should not' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup TestWithCommon' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see 'Test Checkpoint' checkpoint on Inspection page
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    Then I 'should' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'Add New' for "Communications" action
    And I click on 'Add' button in Communication modal window
    And I click on "Communications" dropdown under inspection headers
    And I click on "Inspection Id" in Communications opened dropdown
    Then I 'should' see 'Test Checkpoint Communication' checkpoint on Inspection page
    Then I 'should not' see 'Hardhats are being worn' checkpoint on Inspection page
    Then I 'should not' see 'Safety vests are being worn' checkpoint on Inspection page


  Scenario: Limit bulk file size
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    When I attach 15 files to checkpoint on inspection page:
      | fileName   | checkpoint    |
      | test_image | Safety Header |
      Then I should see 'Upload limit exceeded' header and 'Up to 10 files can be uploaded at one time. Each file may be up to 50 MB. Please try again.' text in appeared error modal
      When I click on 'Ok' in upload error modal
    When I attach 10 files to checkpoint on inspection page:
      | fileName   | checkpoint    |
      | test_image | Safety Header |
    Then I should see '10' attachments in 'Safety Header' checkpoint on inspection page


  Scenario: User should see calculation in matrix via formulas on Inspection page
     Given I login as admin user
     Then I should wait until 'Loading data' modal will be closed
     When I select 'pre-setup (Undefined)' division on create inspection page
     And I select 'pre-setup Clean Project' project on create inspection page
     And I select 'pre-setup Buiding One' phase on create inspection page
     And I select 'pre-setup Location323' location on create inspection page
     And I select 'pre-setup MatrixFormula' inspection on create inspection page
     And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
     And I click on 'Round' button on inspection page
     And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value           |
      | 1           | pre-setup 1.217 |
     Then I should see 'Synced to server' text instead of save button on inspection page
     Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value           |
      | 2           | pre-setup 1     |
      | 3           | pre-setup 1     |
      | 4           | pre-setup 1.22  |
      | 5           | pre-setup 1.217 |
     And I click on 'Floor' button on inspection page
     And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value           |
      | 6           | pre-setup 2.919 |
     Then I should see 'Synced to server' text instead of save button on inspection page
     Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value           |
      | 7           | pre-setup 2     |
      | 8           | pre-setup 2     |
      | 9           | pre-setup 2.91  |
      | 10          | pre-setup 2.919 |
     Then I should see 'Synced to server' text instead of save button on inspection page
     And I click on 'Max' button on inspection page
     And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value         |
      | 11          | pre-setup  11 |
     Then I should see 'Synced to server' text instead of save button on inspection page
     Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value        |
      | 12          | pre-setup 11 |
      | 13          | pre-setup 11 |
      | 14          | pre-setup 11 |
      | 15          | pre-setup 20 |
     Then I should see 'Synced to server' text instead of save button on inspection page
     When I click on 'Delete' button on inspection page
     And I click on 'Yes' button on delete modal


  Scenario: User should be able to hide and show checkpoints with different If-Else rules
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup ifElseCheckpoint' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I wait '10' seconds
    And I click on 'FTQ' checkbox in 'Test1' checkpoint on inspection page
    Then I 'should' see 'Test2' checkpoint on Inspection page
    And I click on 'OPN' checkbox in 'Test1' checkpoint on inspection page
    Then I 'should not' see 'Test2' checkpoint on Inspection page
    And I fill 'Test1' checkpoint with 'pre-setup test3' text on inspection page
    Then I 'should' see 'Test3' checkpoint on Inspection page
    And I fill 'Test1' checkpoint with 'pre-setup test4' text on inspection page
    Then I 'should not' see 'Test3' checkpoint on Inspection page

  @devfix
  Scenario: Dynamic checkpoint
    Given I login as admin user using url '/Settings/TaskSetup'
    When I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text        | category |
      | ChecklistDC | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I enter 'ChecklistDC' in the search on checklist setup page
    And I select 'ChecklistDC' checklist on TaskSetup page
    And I select 'Checkpoint' in process menu
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checkpoint on the TaskSetup page:
      | text       |
      | pre-setup TestIfElse |
    And I click 'Save' button on the TaskSetup page
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checkpoint on the TaskSetup page:
      | text                 | typeCheckpoint     | typeCheckpointRow |
      | pre-setup DynamicBig | Dynamic Checkpoint | 1                 |
    And I click 'Save' button on the TaskSetup page
    And I click 'Options...' for '2' checkpoint in row on the TaskSetup page
    And I set If-Else condition for checkpoint on the TaskSetup page:
      | precondition | condition |
      | Status       | FTQ       |
    And I add sub-checkpoint with data:
      | text                       | type                       | row |
      | testData                   | Data                       | 3   |
      | testHalfData               | Half-width Data            | 3   |
      | testHeader                 | Header                     | 3   |
#      | testInspectorInstructions  | Inspector Instructions     | 3   |
#      | testQuality                | Quality                    | 3   |
#      | testQualityDeficiency      | Quality Deficiency         | 3   |
#      | testQualityHotspot         | Quality Hotspot            | 3   |
#      | testSafety                 | Safety                     | 3   |
#      | testSafetyHeader           | Safety Header              | 3   |
#      | testSafetyHotspot          | Safety Hotspot             | 3   |
#      | testSafetyViolation        | Safety Violation           | 3   |
#      | testScoreBudget            | Score - Budget             | 3   |
#      | testScoreCleanliness       | Score - Cleanliness        | 3   |
#      | testScoreProductivity      | Score - Productivity       | 3   |
#      | testScoreProjectManagement | Score - Project Management | 3   |
#      | testQuality                | Score - Quality            | 3   |
#      | testSafety                 | Score - Safety             | 3   |
#      | testSchedule               | Score - Schedule           | 3   |
#      | testTeamwork               | Score - Teamwork           | 3   |
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    When I select 'ChecklistDC' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I wait '10' seconds
    Then I click on 'FTQ' checkbox in 'TestIfElse' checkpoint on inspection page
    And I click on 'DynamicBig' button on inspection page
    And I wait '10' seconds
    Then I 'should' see 'testData' checkpoint on Inspection page
    Then I 'should' see 'testHalfData' checkpoint on Inspection page
    Then I 'should' see 'testHeader' checkpoint title section on Inspection page
#    Then I 'should' see 'testInspectorInstructions' checkpoint on Inspection page
#    Then I 'should' see 'testQuality' checkpoint on Inspection page
#    Then I 'should' see 'testQualityDeficiency' checkpoint on Inspection page
#    Then I 'should' see 'testQualityHotspot' checkpoint on Inspection page
#    Then I 'should' see 'testSafety' checkpoint on Inspection page
#    Then I 'should' see 'testSafetyHeader' checkpoint title section on Inspection page
#    Then I 'should' see 'testSafetyHotspot' checkpoint on Inspection page
#    Then I 'should' see 'testSafetyViolation' checkpoint on Inspection page
#    Then I 'should' see 'testScoreBudget' checkpoint on Inspection page
#    Then I 'should' see 'testScoreCleanliness' checkpoint on Inspection page
#    Then I 'should' see 'testScoreProductivity' checkpoint on Inspection page
#    Then I 'should' see 'testScoreProjectManagement' checkpoint on Inspection page
#    Then I 'should' see 'testQuality' checkpoint on Inspection page
#    Then I 'should' see 'testSafety' checkpoint on Inspection page
#    Then I 'should' see 'testSchedule' checkpoint on Inspection page
#    Then I 'should' see 'testTeamwork' checkpoint on Inspection page
    And I click on 'NA' checkbox in 'TestIfElse' checkpoint on inspection page
    Then I 'should not' see 'testData' checkpoint title section on Inspection page
    Then I 'should not' see 'testHalfData' checkpoint on Inspection page
    Then I 'should not' see 'testHeader' checkpoint title section on Inspection page
#    Then I 'should not' see 'testInspectorInstructions' checkpoint on Inspection page
#    Then I 'should not' see 'testQuality' checkpoint on Inspection page
#    Then I 'should not' see 'testQualityDeficiency' checkpoint on Inspection page
#    Then I 'should not' see 'testQualityHotspot' checkpoint on Inspection page
#    Then I 'should not' see 'testSafety' checkpoint on Inspection page
#    Then I 'should not' see 'testSafetyHeader' checkpoint title section on Inspection page
#    Then I 'should not' see 'testSafetyHotspot' checkpoint on Inspection page
#    Then I 'should not' see 'testSafetyViolation' checkpoint on Inspection page
#    Then I 'should not' see 'testScoreBudget' checkpoint on Inspection page
#    Then I 'should not' see 'testScoreCleanliness' checkpoint on Inspection page
#    Then I 'should not' see 'testScoreProductivity' checkpoint on Inspection page
#    Then I 'should not' see 'testScoreProjectManagement' checkpoint on Inspection page
#    Then I 'should not' see 'testQuality' checkpoint on Inspection page
#    Then I 'should not' see 'testSafety' checkpoint on Inspection page
#    Then I 'should not' see 'testSchedule' checkpoint on Inspection page
#    Then I 'should not' see 'testTeamwork' checkpoint on Inspection page











