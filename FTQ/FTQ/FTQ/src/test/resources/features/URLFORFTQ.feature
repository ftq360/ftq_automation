@URLFORFTQ
Feature: URL for FTQ

  Scenario: Verify /Inspections/Create
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Inspections/Create'
    Then I 'should' see '/Inspections/Create' page


  Scenario: Verify /ChecklistLibrary
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/ChecklistLibrary'
    Then I 'should' see '/ChecklistLibrary' page


  Scenario: Verify /Dashboards/InspectionDashboard
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Dashboards/InspectionDashboard'
    Then I 'should' see '/Dashboards/InspectionDashboard' page


  Scenario: Verify /Checklists/InspectionPlan
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Checklists/InspectionPlan'
    Then I 'should' see '/Checklists/InspectionPlan' page


  Scenario: Verify /Dashboards/itpdashboard
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Dashboards/itpdashboard'
    Then I 'should' see '/Dashboards/itpdashboard' page


  Scenario: Verify /Dashboards/OpenItemDashboard
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Dashboards/OpenItemDashboard'
    Then I 'should' see '/Dashboards/OpenItemDashboard' page


  Scenario: Verify /Dashboards/VendorPerformanceDashboard
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Dashboards/VendorPerformanceDashboard'
    Then I 'should' see '/Dashboards/VendorPerformanceDashboard' page


  Scenario: Verify /Deficiencies
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Deficiencies'
    Then I 'should' see '/Deficiencies' page


  Scenario: Verify /Deficiencies/Archive
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Deficiencies/Archive'
    Then I 'should' see '/Deficiencies/Archive' page


  Scenario: Verify http://support.ftq360.com/
    When I open following url 'http://support.ftq360.com/'
    Then I 'should' see 'support.ftq360' page


  Scenario: Verify https://support.ftq360.com/hc/en-us/articles/115007622047-What-s-New-in-2018
    When I open following url 'https://support.ftq360.com/hc/en-us/articles/115007622047-What-s-New-in-2018'
    Then I 'should' see 'Whats New' page


  Scenario: Verify https://support.ftq360.com/hc/en-us/requests/new
    When I open following url 'https://support.ftq360.com/hc/en-us/requests/new'
    Then I 'should' see 'Submit a request' page


  Scenario: Verify https://support.ftq360.com/hc/en-us/sections/115002546848-Download-Apps
    When I open following url 'https://support.ftq360.com/hc/en-us/sections/115002546848-Download-Apps'
    Then I 'should' see 'Download Apps' page


  Scenario: Verify https://support.ftq360.com/hc/en-us/sections/115002547788-Videos
    When I open following url 'https://support.ftq360.com/hc/en-us/sections/115002547788-Videos'
    Then I 'should see 'Videos' page


  Scenario: Verify /Inspections
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Inspections'
    Then I 'should' see '/Inspections' page


  Scenario: Verify /Inspections/Archive
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Inspections/Archive'
    Then I 'should' see '/Inspections/Archive' page


  Scenario: Verify /Home/ErrorReport
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Home/ErrorReport'
    Then I 'should' see '/Home/ErrorReport' page


  Scenario: Verify /Library/UserMgmt/LicenseView.aspx
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Library/UserMgmt/LicenseView.aspx'
    Then I 'should' see '/Library/UserMgmt/LicenseView.aspx' page


  Scenario: Verify /Reports
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports'
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Report      |
      | Description |
    When I select '1' item radio button
    And I select 'Projects' in process menu
    Then I 'should' see following columns:
      | column        |
      | Code          |
      | Description   |
      | Division      |
      | Information   |
      | Private Notes |
    When I select 'Phases' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Description |
      | Notes       |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Responsible Parties' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Name        |
      | Description |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Checklists' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Name        |
      | Type        |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Inspection Statuses' in process menu
    Then I 'should' see following columns:
      | column      |
      | Description |
      | Closed      |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Checkpoints Types' in process menu
    Then I 'should' see following columns:
      | column      |
      | Description |
      | Notes       |
      | Attachments |
      | Checkboxes  |
      | Score       |
      | Notes       |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Inspector Types' in process menu
    Then I 'should' see following columns:
      | column      |
      | Description |
    And I 'should' see following buttons:
      | button     |
      | Select All |
      | Continue   |
    When I select 'Dates' in process menu
    Then I 'should' see following buttons:
      | button |
      | Select |
    When I select 'Run Report' in process menu
    Then I 'should' see following buttons:
      | button     |
      | Run report |


  Scenario: Verify /Reports/History
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/History'
    Then I 'should' see '/Reports/History' page


  Scenario: Verify /Reports/Interactive
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/Interactive'
    Then I 'should' see '/Reports/Interactive' page


  Scenario: Verify /Settings/Attachments
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/Attachments'
    Then I 'should' see '/Settings/Attachments' page


  Scenario: Verify /Settings/BaseSetup
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/BaseSetup'
    Then I 'should' see '/Settings/BaseSetup' page
    And I 'should' see following columns:
      | column      |
      | Code        |
      | Notes       |
      | Description |
      | Active      |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
    When I select 'Common Checkpoints' in process menu
    Then I 'should' see following columns:
      | column       |
      | Code         |
      | Seq.         |
      | Type         |
      | Text         |
      | Def. Note    |
      | Auto Options |
      | Project      |
      | R            |
      | Category     |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
    When I select 'Reason Codes' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Seq.        |
      | Category    |
      | Description |
      | Notes       |
      | Active      |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
    When I select 'Corrective Action Codes' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Seq.        |
      | Category    |
      | Description |
      | Notes       |
      | Active      |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |

  Scenario: Verify /Settings/CommunitySetup
    Given I login as admin user using url '/Settings/CommunitySetup'
    Then I should wait until 'Loading data' modal will be closed
    And I select 'Project' in process menu
    Then I 'should' see '/Settings/CommunitySetup' page
    When I enter 'pre-setup Clean Project' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description             | email                 |
      | pre-setup Clean Project |                       |
    When I select searched item on community setup page
    And I select 'Phase' in process menu
    Then I 'should' see following columns:
      | column        |
      | Code          |
      | Seq.          |
      | Phase         |
      | Email         |
      | Private Notes |
      | Plan          |
      | Attach        |
      | Active        |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    And I enter 'pre-setup Buiding One' in the search on community setup page
    And I select searched item on community setup page
    And I select 'Phase ITP' in process menu
    Then I 'should' see following columns:
      | column            |
      | Code              |
      | Seq.              |
      | Dependency        |
      | Due               |
      | Checklist         |
      | phase             |
      | Location          |
      | Responsible Party |
      | Equipment         |
      | Insp. Affiliation |
      | Insp. Type        |
      | Qty               |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Phase Locations' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Seq.        |
      | Description |
      | Notes       |
      | Phase       |
      | Active      |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Phase Equipment' in process menu
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Seq.        |
      | Type        |
      | Description |
      | phase       |
      | Location    |
      | Attach      |
      | Active      |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Project Details' in process menu
    Then I 'should' see following columns:
      | column     |
      | Field Name |
      | Value      |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Phase Checklists' in process menu
    Then I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Phase Responsible Parties' in process menu
    Then I 'should' see following columns:
      | column             |
      | Responsible Party  |
      | Responsible Person |
      | Selected           |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
    When I select 'Phase Inspectors' in process menu
    Then I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |


  Scenario: Verify /Settings/PlanSetup
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/PlanSetup'
    Then I 'should' see '/Settings/PlanSetup' page
    When I select '1' item radio button
    Then I 'should' see following columns:
      | column      |
      | Code        |
      | Name        |
      | Description |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
      | Next >     |
    When I select 'Plan Items' in process menu
    Then I 'should' see following columns:
      | column            |
      | Code              |
      | Seq.              |
      | Offset            |
      | Checklist         |
      | Responsible Party |
      | Insp. Affiliation |
      | Insp. Type        |
      | Min               |
      | Notes             |
      | Dependency        |
      | Active	          |
    And I 'should' see following buttons:
      | button     |
      | < Previous |
      | Save       |
      | Cancel     |
      | Next >     |


  Scenario: Verify /Settings/Preferences
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/Preferences'
    Then I 'should' see '/Settings/Preferences' page


  Scenario: Verify /Settings/ReportRunnerSetup
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/ReportRunnerSetup'
    Then I 'should' see '/Settings/ReportRunnerSetup' page
    And I 'should' see following columns:
      | column          |
      | Code            |
      | Report          |
      | Description     |
      | Show Online     |
      | Run on Schedule |
      | Schedule        |
      | Loop            |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Favorite Reports & Scheduling' in process menu
    Then I 'should' see following columns:
      | column                  |
      | Favorite Report Name    |
      | Report                  |
      | Available to Run Online |
      | Run on Schedule         |
      | Schedule                |
      | Recipients              |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Interactive Reports' in process menu
    Then I 'should' see following columns:
      | column  |
      | Report  |
      | Active  |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Data Queries' in process menu
    Then I 'should' see following columns:
      | column  |
      | QueryID |
      | Query   |
      | Active  |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Email Customization & Additional Settings' in process menu
    Then I 'should' see following columns:
      | column     |
      | Field Name |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |



  Scenario: Verify /Settings/TaskSetup
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/TaskSetup'
    Then I 'should' see '/Settings/TaskSetup' page
    When I click X icon in search field
    And I select 'Checklist' in process menu
    Then I 'should' see following columns:
      | column        |
      | Code          |
      | Seq.          |
      | Category      |
      | Type          |
      | Text          |
      | Attach        |
      | Active        |
      | Common Chkpt. |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
      | Next > |
    When I select '1' item radio button
    And I select 'Checkpoint' in process menu
    Then I 'should' see following columns:
      | column       |
      | Code         |
      | Seq.         |
      | Type         |
      | Text         |
      | Def. Note    |
      | Auto Options |
      | Project      |
      | R            |
      | Category     |
      | JR           |
      | Attach       |
      | Active       |
    And I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
      | Next >     |
      | < Previous |
    When I select 'Projects' in process menu
    Then I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
      | Next >     |
      | < Previous |
    When I select 'Responsible Party' in process menu
    Then I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
      | Next >     |
      | < Previous |
    When I select 'Inspectors' in process menu
    Then I 'should' see following buttons:
      | button     |
      | Save       |
      | Cancel     |
      | Next >     |
      | < Previous |


  Scenario: Verify /Settings/UserSetup
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Settings/UserSetup'
    Then I 'should' see '/Settings/UserSetup' page
    And I 'should' see following columns:
      | column           |
      | Username         |
      | First Name       |
      | Last Name        |
      | Password         |
      | Confirm Password |
      | Email            |
      | Insp. type       |
      | Restr.           |
      | Active	         |
    And I 'should' see following buttons:
      | button          |
      | Save            |
      | Cancel          |
      | Send Invitation |
    When I click X icon in search field
    And I select '1' item radio button
    And I select 'Checklists' in process menu
    Then I 'should' see following buttons:
      | button          |
      | Save            |
      | Cancel          |
    When I select 'Proj. Access' in process menu
    Then I 'should' see following buttons:
      | button          |
      | Save            |
      | Cancel          |
    When I select 'Permissions' in process menu
    Then I 'should' see following columns:
      | column                 |
      | Username               |
      | First Name             |
      | Last Name              |
      | Inspections            |
      | View                   |
      | Edit                   |
      | Create                 |
      | Plan Mode Required     |
      | Restricted	           |
      | Self Insp. Only	       |
      | Run Rpts.	           |
      | Setup Data	           |
      | Data In/Out	           |
      | Data Overwrite/ Delete |
      | User Mgmt.             |
      | Acc. Mgmt              |
    And I 'should' see following buttons:
      | button          |
      | Save            |
      | Cancel          |
    When I select 'Linked Accounts' in process menu
    Then I 'should' see following buttons:
      | button          |
      | Save            |
      | Cancel          |

  Scenario: Verify /Settings/VendorTasksSetup
  Given I login as admin user using url '/Settings/VendorTasksSetup'
  Then I should wait until 'Loading data' modal will be closed
    Then I 'should' see '/Settings/VendorTasksSetup' page
    When I select '1' item radio button
    And I select 'Crews' in process menu
    Then I 'should' see following columns:
      | column     |
      | Code       |
      | Insp. Type |
      | First Name |
      | Last Name  |
      | Full Name  |
      | Email      |
      | Notes      |
      | Username   |
      | Attach     |
      | Active     |
    And I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Checklists' in process menu
    Then I 'should' see following buttons:
      | button |
      | Save   |
      | Cancel |
    When I select 'Projects' in process menu
    Then I 'should' see following columns:
      | column             |
      | Selected             |
      | Project            |
      | Responsible Person |
    And I 'should' see following buttons:
      | button       |
      | Save         |
      | Cancel       |


  Scenario: Verify /Reports/Queries
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/Queries'
    Then I 'should' see '/Reports/Queries' page


  Scenario: Verify /Reports/Queries
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/Queries'
    Then I 'should' see '/Reports/Queries' page


  Scenario: Verify /Checklists/ItpList
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Checklists/ItpList'
    Then I 'should' see '/Checklists/ItpList' page


  Scenario: Verify /Checklists/ItpScheduler
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Checklists/ItpScheduler'
    Then I 'should' see '/Checklists/ItpScheduler' page