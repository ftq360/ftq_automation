#@P1
#Feature: Login
#
#  Scenario: Rejected log in with bad password
#    Given I login as admin user
#    Then I should wait until 'Loading data' modal will be closed
#    When I click on 'Setup' top menu item
#    And I select 'Administration' dropdown in appeared top menu dropdown
#    And I select 'User Setup Process' item in appeared top menu dropdown
#    And I click on 'Add' button on user setup page
#    And I fill new user form with next data on user setup page:
#      | username | firstName | lastName | password | confirmPassword | email               | inspectionType |
#      | test     | Auto      | Test     | Great123 | Great123        | test_user@mail.com  | QA Dept.       |
#    And I click on 'Save' button on user setup page
#    Then I should see 'grey' Save button
#    When I click on 'User Icon' top menu item
#    And I select 'Logout' item in appeared top menu dropdown
#    And login with following credentials:
#      | username | password  |
#      | test     | Great123  |
#    And I should check checkbox on license agree page
#    And I click on 'Accept' button on license agree page
#    Then I should wait until 'Loading data' modal will be closed
#    When I click on 'User Icon' top menu item
#    And I select 'Logout' item in appeared top menu dropdown
#    And I login as admin user
#    Then I should wait until 'Loading data' modal will be closed
#    When I click on 'Setup' top menu item
#    And I select 'Administration' dropdown in appeared top menu dropdown
#    And I select 'User Setup Process' item in appeared top menu dropdown
#    And I enter 'test' in the search on user setup page
#    When I select searched user on user setup page
#    And I fill new user form with next data on user setup page:
#     | password  | confirmPassword |
#     | Great1234 | Great1234       |
#    And I click on 'Save' button on user setup page
#    And I click on 'User Icon' top menu item
#    And I select 'Logout' item in appeared top menu dropdown
#    And login with following credentials:
#      | username | password  |
#      | test     | Great123  |
#    Then I should see login error on login page
#    When I login as admin user
#    Then I should wait until 'Loading data' modal will be closed
#    When I click on 'Setup' top menu item
#    And I select 'Administration' dropdown in appeared top menu dropdown
#    And I select 'User Setup Process' item in appeared top menu dropdown
#    And I enter 'test' in the search on user setup page
#    When I select searched user on user setup page
#    And I click on delete button for selected user on user setup page
#    And I click on 'Save' button on user setup page
#    And I click on 'Ok' button on delete modal