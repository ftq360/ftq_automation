@P1
Feature: P1

  Scenario: Listing Grid offline loads
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    When I save inspection GUID
    And I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    When I save inspection GUID
    And I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I click on 'Show Recent Inspections' button on Create Inspection page
    Then I should see inspections with saved guids on Create Inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Sync Control Panel' item in appeared top menu dropdown
    Then I should see inspections with saved guids on Sync Control Panel page

  Scenario: Creating new project (basic view)
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code            | description | email                 |
      | testProjectCode | testProject | test_project@mail.com |
    And I enter 'testProject' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should not see ready to save item
    And I 'should' see next project in he list on community setup page:
      | description | email                 |
      | testProject | test_project@mail.com |
    When I select searched item on community setup page
    And I select 'Phase' in process menu
    And I click on 'Add' button on community setup page
    And I fill new phase form with next data on community setup page:
      | code          | phaseName |
      | testPhaseCode | testPhase |
    And I enter 'testPhase' in the search on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    Then I should not see ready to save item
    And I 'should' see next phase in he list on community setup page:
      | phaseName |
      | testPhase |
    When I select searched item on community setup page
    And I select 'Phase Checklists' in process menu
    And I select '1' checklists on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I select 'Phase Responsible Parties' in process menu
    And I select '2' subcontractors on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I select 'Phase Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I enter 'testProject' in the search on create inspection page
    Then I 'should' see 'testProject' project on create inspection page
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I enter 'testProject' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description | email                 |
      | testProject | test_project@mail.com |
    When I delete searched project on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Ok' button on delete modal
    Then I should see 'grey' Save button
    And I 'should not' see next project in he list on community setup page:
      | description |
      | testProject |
    When I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I enter 'testProject' in the search on create inspection page
    Then I 'should not' see 'testProject' project on create inspection page

  Scenario: Creating new inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Hardhats are being worn |
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page
    Then I should see Notes field filled with 'note' text on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    And I should see checked 'OPEN' checkbox for 'Safety vests are being worn' checkpoint on inspection page
    And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'delete' button on inspection page
    And I click on 'Yes' button on delete modal
    Then I 'should not' see recently created inspection on view recent inspections page
    When I enter 'inspection id' in the search on inspections page
    Then I 'should not' see recently created inspection on view recent inspections page


  Scenario: Users from the same group should see inspections of each others
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I should see 'Concrete-Checkpoint Test' inspection on inspection page
    And I wait '5' seconds
    When I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username                | password   |
      | pre-setup dantheman5502 | caldeira44 |
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page
    Then I should see valid Inspection ID on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Creating new user
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test     | Auto      | Test     | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test     | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    And I wait '5' seconds
    And I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test' in the search on user setup page
    Then I 'should' see user with following data on user setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |
    When I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal
    Then I 'should not' see user with following data on user setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test     | Great123 |
    Then I should see login error on login page


  Scenario: Change user password
    Given I login as admin user using url '/Settings/UserSetup'
    Then I select 'Users' step on user setup page
    And I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test     | Auto      | Test     | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test     | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    When I refresh page
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test' in the search on user setup page
    Then I 'should' see user with following data on user setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |
    And I edit searched user with next data on user setup page:
      | password  | confirmPassword |
      | Fishki123 | Fishki123       |
    Then I should see 'blue' Save button
    When I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test     | Great123 |
    Then I should see login error on login page
    When I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test' in the search on user setup page
    Then I 'should' see user with following data on user setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |
    When I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    Then I should see 'blue' Save button
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal
    Then I 'should not' see user with following data on user setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |

  Scenario: Inspection created successfully - simplest basic steps
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Hardhats are being worn |
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    Then I should wait until 'Saved to device' notification appears on inspection page


  Scenario: Inspections auto-sync to server successfully
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Hardhats are being worn |
    Then I should wait until 'Synced to server' notification appears on inspection page
    And I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page
    Then I should see Notes field filled with 'note' text on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    And I should see checked 'OPEN' checkbox for 'Safety vests are being worn' checkpoint on inspection page
    And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: View Recent inspections - does the page load?
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    Then I 'should' see header of table on view recent inspections page
    And I 'should' see 'show' dropdown on view recent inspections page
    And I 'should' see 'for project' dropdown on view recent inspections page
    And I 'should' see 'delete' button on view recent inspections page


  Scenario: Dashboards - does the page load?
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Dashboards' top menu item
    And I select 'ITP Inspection Progress Dashboard' item in appeared top menu dropdown
    Then I 'should' see 'ITP Inspection Progress Dashboard' header
    And I 'should' see following sections on open item dashboard:
      | sectionName             |
      | Project                 |
      | ITP Inspection Activity |
      | Status                  |
      | Checklist Category      |
      | Checklist               |
      | Responsible Party       |
      | Inspector Type          |
    When I click on 'Dashboards' top menu item
    And I select 'Inspections Activity Dashboard' item in appeared top menu dropdown
    Then I 'should' see 'Inspections Dashboard' header
    And I 'should' see following sections on open item dashboard:
      | sectionName       |
      | Time Period       |
      | Inspections       |
      | Stats             |
      | Project           |
      | Responsible Party |
      | Inspector         |
      | Checklist         |
      | Inspector Type    |
      | Status            |
    When I click on 'Dashboards' top menu item
    And I select 'ITP Inspection Progress Dashboard' item in appeared top menu dropdown
    Then I 'should' see 'ITP Inspection Progress Dashboard' header
    And I 'should' see following sections on open itp dashboard:
      | sectionName             |
      | Project                 |
      | ITP Inspection Activity |
      | Status                  |
      | Checklist Category      |
      | Checklist               |
      | Responsible Party       |
      | Inspector Type          |
    When I click on 'Dashboards' top menu item
    And I select 'Responsible Party Performance Dashboard' item in appeared top menu dropdown
    Then I 'should' see 'Responsible Party Performance Dashboard' header
    And I 'should' see following sections on responsible party performance dashboard:
      | sectionName                            |
      | % FTQ Trend                            |
      | % FTQ Checkpoints By Category          |
      | % FTQ Checkpoints By Responsible Party |
      | # Deficiencies Trend                   |
      | # Deficiencies By Responsible Party    |
      | Avg. Days To R4R By Responsible Party  |
      | Recurring Deficiencies By Checkpoint   |
      | Total R Exposure By Responsible Party  |
      | Stats                                  |


  Scenario: Creating new checklist (basic view)
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text      | category |
      | Checklist | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I select 'Checklist' checklist on TaskSetup page
    And I select 'Checkpoint' in process menu
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checkpoint on the TaskSetup page:
      | text           |
      | Dynamic matrix |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I 'should' see 'Checklist' project on create inspection page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see inspection with following data on inspection page:
      | inspectionName | projectName   | phaseName   | checkpointName |
      | Checklist      | Clean Project | Buiding One | Dynamic matrix |
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I wait '3' seconds
    And I open following url '/Settings/TaskSetup'
    And I wait '3' seconds
    And I select 'Checklist' in process menu
    And I enter 'Checklist' in the search on checklist setup page
    And I select 'Checklist' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal

  Scenario: Creating new Responsible Party (basic view)
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Responsible Party Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the VendorTaskSetup page
    And I add following data for new responsible party on VendorTaskSetup page:
      | description | email         | isActive |
      | description | test@mail.com | true     |
    And I wait '5' seconds
    And I click 'Save' button on the VendorTaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I enter 'description' in the search on create inspection page
    Then I 'should' see 'description' responsible party on create inspection page
    When I select 'description' inspection on create inspection page
    Then I 'should' see inspection with following data on inspection page:
      | inspectionName                     | projectName   | phaseName   |
      | pre-setup Concrete-Checkpoint Test | Clean Project | Buiding One |
    When I click on 'FAILED' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see 'description' responsibility party on 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see 'description' responsibility party on 'Safety vests are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Save in offline
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see inspection with following data on inspection page:
      | inspectionName                     | projectName   | phaseName   |
      | pre-setup Concrete-Checkpoint Test | Clean Project | Buiding One |
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I attach file to checkpoint on inspection page:
      | fileName   | checkpoint              |
      | test_image | Hardhats are being worn |
    And I click on 'Save' button on inspection page
    And I should wait until 'Saved to device' notification appears on inspection page
    And I save inspection GUID
    When I click on 'Signal icon' top menu item
    And I select 'Sync Control Panel' item in appeared top menu dropdown
    Then I 'should' see 'guid' inspection on sync control panel page
    When I click on 'Signal icon' top menu item
    And I select 'Online' item in appeared top menu dropdown
    And I wait '5' seconds
    And I refresh page
    Then I 'should not' see 'guid' inspection on sync control panel page
    When I open 'guid' inspection url
    Then I should wait until Inspection ID value appears on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    And I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Signature
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see inspection with following data on inspection page:
      | inspectionName                     | projectName   | phaseName   |
      | pre-setup Concrete-Checkpoint Test | Clean Project | Buiding One |
    And I click on camera button in 'Hardhats are being worn' checkpoint on inspection page
    And I select 'Signature' option in camera dropdown on inspection page
    Then I should see drawing board
    When I draw signature on drawing board
    And I click on 'Save' button on drawing board
    Then I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I refresh page
    Then I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Photo
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see inspection with following data on inspection page:
      | inspectionName                     | projectName   | phaseName   |
      | pre-setup Concrete-Checkpoint Test | Clean Project | Buiding One |
    And I click on camera button in 'Hardhats are being worn' checkpoint on inspection page
    And I select 'Photo' option in camera dropdown on inspection page
    And I click on capture video on inspection page
    And I click on 'Save' button on drawing board
    Then I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I refresh page
    Then I should see attached file to 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Run Online Report
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/History'
    Then I 'should' see '/Reports/History' page
    When I delete all reports on reports history page
    And I click on 'Reports' top menu item
    And I select 'Run Reports Online' item in appeared top menu dropdown
    And I enter 'pre-setup FTQ-324b' in the search on reports page
    Then I should see 'pre-setup FTQ-324b' report on reports page
    When I select 'pre-setup FTQ-324b' report on reports page
    And I select 'Dates' in process menu
    And I select 'Yesterday' date range on reports page
    And I click on 'Select' button on reports page
    And I click on 'Run Report(online)' button on reports page
    Then I should see following report on reports history page:
      | reportName                                                        | dateRange |
      | pre-setup 324b WCC-324 FTQ Performance by Project (last 12 weeks) | Yesterday |


  Scenario: Emergency Dictionary Refresh button
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I save all titles from page
    And I click on refresh button
    Then I should wait until 'Loading data' modal will be closed
    And I should see saved titles
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I save all titles from page
    And I click on refresh button
    Then I should wait until 'Loading data' modal will be closed
    And I should see saved titles
    When I select 'pre-setup Clean Project' project on create inspection page
    And I save all titles from page
    And I click on refresh button
    Then I should wait until 'Loading data' modal will be closed
    And I should see saved titles
    When I select 'pre-setup Buiding One' phase on create inspection page
    And I save all titles from page
    And I click on refresh button
    Then I should wait until 'Loading data' modal will be closed
    And I should see saved titles

  Scenario: Run Interactive Report
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/History'
    Then I 'should' see '/Reports/History' page
    When I delete all reports on reports history page
    And I click on 'Reports' top menu item
    And I select 'Interactive Reports' item in appeared top menu dropdown
    And I select report by 'Inspector, Checklist, Project' on interactive reports page
    And I click on 'Select All' button on interactive reports page
    And I click on 'Select All' button on interactive reports page
    And I click on 'Select All' button on interactive reports page
    And I click on 'Run Report(interactive)' button on reports page
    Then I should see following report on reports history page:
      | reportName                                                                   |
      | pre-setup 401ax Inspection Report (New Style, inspection checklist printout) |


  Scenario: Inspection/Deficiency ID links work
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I save YOffset coordinate
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    And I should see 'Synced to server' text instead of save button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Deficiencies (1,000)' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on deficiencies page
    When I click on deficiency id on deficiencies page
    And I should see 'Synced to server' text instead of save button on inspection page
    Then I should see page scrolled to checkpoint on inspection page
    When I wait '3' seconds
    And I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Inspection ID link works
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Deficiencies (1,000)' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on deficiencies page
    When I click on inspection id on deficiencies page
    Then I should see valid Inspection ID on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Correct text state shown
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    When I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    And I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page

  Scenario: Inspection attachments and data all correct even after server refresh
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    Then I should wait until data will be loaded
    When I click '+Add' button on the TaskSetup page
    And I enter ' ' in the search on checklist setup page
    And I wait '10' seconds
    And I add following data for new checklist on the TaskSetup page:
      | text      | category |
      | Checklist | Concrete |
    And I click 'Save' button on the TaskSetup page
    Then I should wait until data will be loaded
    When I add following attachments on the TaskSetup page:
      | attachLink                 | attachNote | file       |
      | https://devtest.ftq360.net | test note  | test_image |
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    Then I 'should' see 'Checklist' project on create inspection page
    When I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click 'Checklist References' on Inspection page
    Then I 'should' see following attachments on Checklist References section:
      | note      | link                        |
      | test note | https://devtest.ftq360.net/ |
    When I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I refresh page
    And I click 'Checklist References' on Inspection page
    Then I 'should' see following attachments on Checklist References section:
      | note      | link                        |
      | test note | https://devtest.ftq360.net/ |
    When I click on 'Copy' button on inspection page
    And I click 'Yes' on Copy inspection modal
    Then I should wait until Inspection ID value appears on inspection page
    When I refresh page
    And I click 'Checklist References' on Inspection page
    Then I 'should' see following attachments on Checklist References section:
      | note      | link                        |
      | test note | https://devtest.ftq360.net/ |
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Create user
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I click on 'Add' button on user setup page
    Then I should see '1' new line added on User Setup page
    When I click on 'Save' button on user setup page
    Then I should see following notification:
      """
      Could not save. Please check highlighed fields in changed records
      """
    When I click on 'Cancel' button on user setup page
    Then I should see '0' new line added on User Setup page
    When I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test1    | Auto      | Test     | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    When I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test1    | Auto      | Test     | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    Then I should see following notification:
      """
      Could not save. User with that username already exists on the FTQ360 system.
      """
    When I fill new user form with next data on user setup page:
      | username |
      | test2    |
    And I click on 'Save' button on user setup page
    When I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test3    | Auto      | Test3    | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    When I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType |
      | test4    | Auto      | Test4    | Great123 | Great123        | test_user@mail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    When I select 'Permissions' in process menu
    And I enter 'test1' in the search on user setup page
    Then I 'should' see user with following data on User Setup Permissions page:
      | username | inspect | editInsp |
      | test1    | checked | checked  |
    When I select 'Permissions' in process menu
    And I enter 'test2' in the search on user setup page
    Then I 'should' see user with following data on User Setup Permissions page:
      | username | inspect | editInsp |
      | test2    | checked | checked  |
    When I select 'Permissions' in process menu
    And I enter 'test3' in the search on user setup page
    Then I 'should' see user with following data on User Setup Permissions page:
      | username | inspect | editInsp |
      | test3    | checked | checked  |
    When I select 'Permissions' in process menu
    And I enter 'test4' in the search on user setup page
    Then I 'should' see user with following data on User Setup Permissions page:
      | username | inspect | editInsp |
      | test4    | checked | checked  |
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test1    | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test2    | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test3    | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test4    | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And I login as admin user
    And I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test1' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal
    And I enter 'test2' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal
    And I enter 'test3' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal
    And I enter 'test4' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal

  Scenario: Show recent inspections
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I 'should' see appeared QC fields for checkpoint on inspection page
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    And I click on 'Inspections' top menu item
    When I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I click 'Show Recent Inspections' button on Create Inspection page
    And I get data from first inspection in list on Create Inspection page
    And I click first inspection in list on Create Inspection page
    Then I should see same data on Inspection page as data from first inspection in list on Create Inspection page
    And I click on 'Inspections' top menu item
    When I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I click 'Show Recent Inspections' button on Create Inspection page
    And I get data from first inspection in list on Create Inspection page
    And I click first inspection in list on Create Inspection page
    Then I should see same data on Inspection page as data from first inspection in list on Create Inspection page
    And I click on 'Inspections' top menu item
    When I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I click 'Show Recent Inspections' button on Create Inspection page
    And I get data from first inspection in list on Create Inspection page
    And I click first inspection in list on Create Inspection page
    Then I should see same data on Inspection page as data from first inspection in list on Create Inspection page
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I click 'Show Recent Inspections' button on Create Inspection page
    And I get data from first inspection in list on Create Inspection page
    And I click first inspection in list on Create Inspection page
    Then I should see same data on Inspection page as data from first inspection in list on Create Inspection page
    And I click on 'Inspections' top menu item
    When I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I click 'Show Recent Inspections' button on Create Inspection page
    And I get data from first inspection in list on Create Inspection page
    And I click first inspection in list on Create Inspection page
    Then I should see same data on Inspection page as data from first inspection in list on Create Inspection page

  Scenario: Create non restricted user
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I select 'Users' in process menu
    And I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email              | inspectionType | affiliation |
      | test     | Auto      | Test     | Great123 | Great123        | test_user@mail.com | QA Dept.       | (V190)      |
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I open following url '/Settings/VendorTasksSetup'
    And I select 'Responsible Parties/Departments' in process menu
    And I enter 'pre-setup V190' in the search on responsible party page
    And I select 'V190' responsible party
    And I select 'Crews' in process menu
    And I select 'All' in top right dropdown
    And I enter 'test_user@mail.com' email in the search on responsible party page
    Then I 'should' see user with following data on 'Crew' tab on 'Responsible Party' setup page:
      | username | firstName | lastName | email              | inspectionType |
      | test     | Auto      | Test     | test_user@mail.com | QA Dept.       |
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal


  Scenario: Create new ITP inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page


  Scenario: Auto-update inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I should wait until Inspection ID value appears on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I click on 'Signal icon' top menu item
    And I select 'Sync Control Panel' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on sync control panel
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Sync Control Panel' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I click on 'Signal icon' top menu item
    And I select 'Online' item in appeared top menu dropdown
    Then I should wait until inspection with 'inspection id' id is synchronized on sync control panel
    When I open searched item on sync control panel
    And I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page
    Then I should wait until Inspection ID value appears on inspection page
    And I should see Notes field filled with 'note' text on inspection page
    And I should see checked 'SAFE' checkbox for 'Hardhats are being worn' checkpoint on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Checkpoint status
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text      | category |
      | Checklist | Concrete |
    And I click 'Save' button on the TaskSetup page
    And I click on 'FTQ360' button in top menu
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I enter 'Checklist' in the search on inspections page
    And I select 'Checklist' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see 'Pass' status on inspection page
    And I should see 'Synced to server' text instead of save button on inspection page
    When I select override checkbox for status on the 'Inspection' page
    And I select 'Pass' status on the 'Inspection' page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I refresh page
    Then I should see 'Pass' status on inspection page
    When I select 'Complete (w/Open)' status on the 'Inspection' page
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I refresh page
    Then I should see 'Complete (w/Open)' status on inspection page
    When I click on 'OPEN' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I select override checkbox for status on the 'Inspection' page
    Then I should see 'Complete (w/Open)' status on inspection page
    And I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I wait '3' seconds
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist' in the search on checklist setup page
    And I select 'Checklist' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal


  Scenario: OPN/QC checkpoints work as expected
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'FAILED' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see following fields for 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               |
      | Enter Observation       |
      | Corrective Action Notes |
      | Ready for review        |
      | Responsible Party       |
      | Location                |
      | Due Date                |
      | Risk Factor             |
    When I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see following fields for 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName               |
      | Enter Observation       |
      | Corrective Action Notes |
      | Ready for review        |
      | Responsible Party       |
      | Location                |
      | Due Date                |
      | Risk Factor             |
    When I fill following fields for 'Hardhats are being worn' checkpoint with following data on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    And I click on 'Save' button on inspection page
    And I refresh page
    Then I should see following data in 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal

  Scenario: Inspection with previously used checklist will show 'Select Option' in ITP Plan field
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist2 | Concrete |
    And I click 'Save' button on the TaskSetup page
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code | description    |
      | test | itptestProject |
    And I wait '10' seconds
    And I enter 'itptestProject' in the search on community setup page
    And I wait '10' seconds
    And I click on 'Save' button on community setup page
    And I enter 'itptestProject' in the search on community setup page
    And I select searched item on community setup page
    And I select 'All ITP' in process menu
    And I wait '2' seconds
    And I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeOne | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeTwo | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'itptestProject' project on create inspection page
    And I check 'Plan mode' checkbox on create inspection page
    And I enter 'Checklist2' in the search on inspections page
    And I select 'Checklist2' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click ITP dropdown on Inspection page
    Then I 'should' see following code into ITP drop down on Inspection page:
      | code           |
      | itpPlanCodeOne |
      | itpPlanCodeTwo |
    When I open following url '/Settings/CommunitySetup'
    And I select 'Project' in process menu
    And I enter 'itptestProject' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description    |
      | itptestProject |
    When I delete searched project on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Ok' button on delete modal
    Then I should see 'grey' Save button
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist2' in the search on checklist setup page
    And I select 'Checklist2' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal

  Scenario: Verify that each plan will have proper data in ITP Plan field
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist2 | Concrete |
    And I click 'Save' button on the TaskSetup page
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | code | description    |
      | test | itptestProject |
    And I wait '10' seconds
    And I enter 'itptestProject' in the search on community setup page
    And I wait '10' seconds
    And I click on 'Save' button on community setup page
    And I wait '1' seconds
    When I select searched item on community setup page
    And I wait '2' seconds
    And I select 'All ITP' in process menu
    And I wait '2' seconds
    And I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeOne | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeTwo | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    And I wait '5' seconds
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'itptestProject' project on create inspection page
    And I check 'Plan mode' checkbox on create inspection page
    Then I should see itp plan item with following data on create inspection page:
      | code           | checklist          |
      | itpPlanCodeOne | Concrete-Checklist |
      | itpPlanCodeTwo | Concrete-Checklist |
    When I click 'itpPlanCodeTwo' checklist on Create Inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see following code into ITP drop down on Inspection page:
      | code           |
      | itpPlanCodeOne |
      | itpPlanCodeTwo |
    When I open following url '/Settings/CommunitySetup'
    And I select 'Project' in process menu
    And I enter 'itptestProject' in the search on community setup page
    Then I 'should' see next project in he list on community setup page:
      | description    |
      | itptestProject |
    When I delete searched project on community setup page
    And I click on 'Save' button on community setup page
    And I click on 'Ok' button on delete modal
    Then I should see 'grey' Save button
    When I open following url '/Settings/TaskSetup'
    And I enter 'Checklist2' in the search on checklist setup page
    And I select 'Checklist2' checklist on TaskSetup page
    And I delete searched checklist on checklist setup page
    And I click 'Save' button on the TaskSetup page
    And I click on 'Ok' button on delete modal

  Scenario: Verify that ITP Progress have created ITP
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
    And I click '+Add' button on the TaskSetup page
    And I add following data for new checklist on the TaskSetup page:
      | text       | category |
      | Checklist2 | Concrete |
    And I click 'Save' button on the TaskSetup page
    When I click on 'Setup' top menu item
    And I select 'Project Setup Process' item in appeared top menu dropdown
    And I select 'Project' in process menu
    And I click on 'Add' button on community setup page
    And I fill new project form with next data on community setup page:
      | description    |
      | itptestProject |
    And I wait '10' seconds
    And I enter 'itptestProject' in the search on community setup page
    And I wait '10' seconds
    And I click on 'Save' button on community setup page
    And I wait '1' seconds
    When I select searched item on community setup page
    And I wait '2' seconds
    And I select 'All ITP' in process menu
    And I wait '2' seconds
    And I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeOne | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Add' button on community setup page
    And I fill ITP plan item data on community setup page:
      | code           | checklist  |
      | itpPlanCodeTwo | Checklist2 |
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I select 'Project Inspectors' in process menu
    And I enter admin username in the search on community setup page
    And I click on 'create' checkbox on community setup page
    And I click on 'Save' button on community setup page
    Then I should see 'grey' Save button
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'itptestProject' project on create inspection page
    And I check 'Plan mode' checkbox on create inspection page
    And I click 'itpPlanCodeTwo' checklist on Create Inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'SAFE' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'itptestProject' project on create inspection page
    And I click 'itpPlanCodeOne' checklist on Create Inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I select override checkbox for status on the 'Inspection' page
    And I select 'Fail (final)' status on the 'Inspection' page
    And I click on 'Save' button on inspection page
    Then I should wait until 'Synced to server' notification appears on inspection page
    And I click on 'Inspections' top menu item
    And I select 'ITP Progress' item in appeared top menu dropdown
    And I search 'itptestProject' in project dropdown
    And I click on arrow for 'itpPlanCodeOne' plan to see inspection list
    Then I should see plan info row section:
      | status       | plan           | icon | inspection_status |
      | FAIL (FINAL) | itpPlanCodeOne | red  | Fail (final)      |
    And I click on arrow for 'itpPlanCodeTwo' plan to see inspection list
    Then I should see plan info row section:
      | status | plan           | icon  | inspection_status |
      | PASS   | itpPlanCodeTwo | green | Pass              |


  Scenario: Auto-update inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should wait until Inspection ID value appears on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Sync Control Panel' item in appeared top menu dropdown
    And I select 'Ready to Save to Device' in top dropdown menu on 'Sync Control Panel' page
    And I enter 'inspection id' in the search on sync control panel page
    When I select inspection id item on sync control panel page
    And I click on 'Save to Device' button on 'Sync Control Panel' page
    And  I click on 'Ok' button in 'Save to Device' modal window
    And I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I click on 'Show Recent Inspections' button on Create Inspection page
    And I open inspection by remembered id on create inspections page
    And I fill 'Notes' field with 'performance' text on inspection page
    And I fill 'Hardhats are being worn' checkpoint with 'test12' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Online' item in appeared top menu dropdown
    And I should wait until 'Synced to server' notification appears on inspection page
    And I should see Notes field filled with 'performance' text on inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Matrix navigation
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'dynamic matrix' button '2' times to add rows
    And I fill '1' cells with 'value' value in Dynamic Matrix on inspection page
    And I click on '2' cell and press 'Tab' key '8' times in Dynamic Matrix on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value  |
      | 9           | value3 |
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I fill 'Hardhats are being worn' checkpoint with 'test' text on inspection page
    Then I should see Dynamic Matrix with following data on inspection page:
      | fieldNumber | value  |
      | 9           | value3 |
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Inspection status filters
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    And I 'should' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'OPEN' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    And I click on 'Any Status' in top Inspection bar
    And I click on 'Open' row status in status filter
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    And I 'should' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'NA' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'Open' in top Inspection bar
    And I click on 'NA' row status in status filter
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    And I 'should not' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'SAFE' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I click on 'NA' in top Inspection bar
    And I click on 'FTQ' row status in status filter
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    And I 'should not' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'OPEN' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see 'Synced to server' text instead of save button on inspection page
    When I click on 'FTQ' in top Inspection bar
    And I click on 'Open' row status in status filter
    Then I 'should' see 'Hardhats are being worn' checkpoint on Inspection page
    And I 'should' see 'Safety vests are being worn' checkpoint on Inspection page
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Run a query
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I open following url '/Reports/History'
    Then I 'should' see '/Reports/History' page
    When I delete all reports on reports history page
    And I click on 'Reports' top menu item
    And I select 'Run Reports Online' item in appeared top menu dropdown
    And I enter 'pre-setup Q1009' in the search on reports page
    Then I should see 'pre-setup Q1009' report on reports page
    When I select 'pre-setup Q1009' report on reports page
    And I select 'Dates' in process menu
    And I select 'Yesterday' date range on reports page
    And I click on 'Select' button on reports page
    And I click on 'Run Report(online)' button on reports page
    Then I should see following report on reports history page:
      | reportName                  | dateRange |
      | pre-setup Q1009 Query #1009 | Yesterday |


  Scenario: Inspection creation date change
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email               | inspectionType |
      | test1    | Auto      | Test     | Great123 | Great123        | test_user1@mail.com | QA Dept.       |
    And I wait '5' seconds
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I select 'Permissions' in process menu
    And I enter 'test1' in the search on user setup page
    Then I 'should' see user with following data on User Setup Permissions page:
      | username | inspect | editInsp |
      | test1    | checked | checked  |
    When I click on 'Data Overwrite or Delete' checkbox for 'test1' user on User Setup Permissions page
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test1    | Great123 |
    And I should check checkbox on license agree page
    And I click on 'Accept' button on license agree page
    And I wait '5' seconds
    Then I should wait until 'Loading data' modal will be closed
    And I wait '30' seconds
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    Then I should wait until 'Loading data' modal will be closed
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should' see 'Inspection created date' section is editable
    When I click on 'Inspection created date' section
    And I wait '5' seconds
    When I select '10' date in calendar modal window
    Then I should see '10' inspection creation date
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And I login as admin user
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    When I select 'Permissions' in process menu
    And I enter 'test1' in the search on user setup page
    When I click on 'Data Overwrite or Delete' checkbox for 'test1' user on User Setup Permissions page
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And login with following credentials:
      | username | password |
      | test1    | Great123 |
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    Then I 'should not' see 'Inspection created date' section is editable
    When I click on 'User Icon' top menu item
    And I select 'Logout' item in appeared top menu dropdown
    And I login as admin user
    And I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I enter 'test1' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal


  Scenario: Send invitation to user
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Setup' top menu item
    And I select 'Administration' dropdown in appeared top menu dropdown
    And I select 'User Setup Process' item in appeared top menu dropdown
    And I click on 'Add' button on user setup page
    And I fill new user form with next data on user setup page:
      | username | firstName | lastName | password | confirmPassword | email                  | inspectionType |
      | test     | Auto      | Test     | Great123 | Great123        | testftqemail@gmail.com | QA Dept.       |
    And I click on 'Save' button on user setup page
    Then I should see 'grey' Save button
    When I enter 'test' in the search on user setup page
    And I select searched item on community setup page
    And I click on 'Send Invitation' button on user setup page
    And I click on 'Ok' button on confirmation modal
    Then I should see following notification:
      """
      Invitations were sent to 1 recipient(s).
      """
    And I enter 'test' in the search on user setup page
    And I select searched user on user setup page
    And I click on delete button for selected user on user setup page
    And I click on 'Save' button on user setup page
    And I click on 'Ok' button on delete modal


  Scenario: Forgot password
    Then I should see 'Forgot password' link under Login form
    When I click on 'Forgot password' link under Login form
    Then I should see 'Restore password' text in Forgot Password modal
    And I should see 'Enter your FTQ360 user name. We'll look for your account and send you a password reset email.' text in Forgot Password modal
    And I type 'pre-setup dantheman5454' in 'User name' field
    And I click on 'Reset' button in Forgot Password form
    Then I should see in forgot password form following text:
      """
      We just sent a password reset email.
      """
    Then I should see in forgot password form following text:
      """
      When you receive the email, click on the link inside to reset your password.
      """
    Then I should see in forgot password form following text:
      """
      If you don't see the email after a few minutes, check spam folder.
      """
  Scenario: Key metric categories
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup ChecklistKeyMetric1' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'OPN' checkbox in 'Checkpoint1Insp (Field Management)' checkpoint on inspection page
    Then I should see following data in 'Checkpoint1Insp (Field Management)' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 1     |
      | TestR3      | 3     |
    And I 'should not' see 'TestR2' risk factor in 'Checkpoint1Insp (Field Management)' checkpoint
    When I click on 'OPN' checkbox in 'Checkpoint2' checkpoint on inspection page
    Then I should see following data in 'Checkpoint2' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 1     |
      | TestR2      | 2     |
    And I 'should not' see 'TestR3' risk factor in 'Checkpoint2' checkpoint
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal
    And I click on 'Inspections' top menu item
    And I select 'Enter New Inspection' item in appeared top menu dropdown
    And I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup ChecklistKeyMetric2' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I click on 'OPN' checkbox in 'Checkpoint1Con (Concrete)' checkpoint on inspection page
    Then I should see following data in 'Checkpoint1Con (Concrete)' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 1     |
      | TestR2      | 2     |
    And I 'should not' see 'TestR3' risk factor in 'Checkpoint1Con (Concrete)' checkpoint
    When I click on 'OPN' checkbox in 'Checkpoint2' checkpoint on inspection page
    Then I should see following data in 'Checkpoint2' checkpoint on inspection page:
      | fieldName   | value |
      | Risk Factor | 1     |
      | TestR3      | 3     |
    And I 'should not' see 'TestR2' risk factor in 'Checkpoint2' checkpoint
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: OPN/QC checkpoints work as expected
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'FAILED' checkbox in 'Hardhats are being worn' checkpoint on inspection page
    Then I should see following fields for 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               |
      | Enter Observation       |
      | Corrective Action Notes |
      | Ready for review        |
      | Responsible Party       |
      | Location                |
      | Due Date                |
      | Risk Factor             |
    When I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
    Then I should see following fields for 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName               |
      | Enter Observation       |
      | Corrective Action Notes |
      | Ready for review        |
      | Responsible Party       |
      | Location                |
      | Due Date                |
      | Risk Factor             |
    When I fill following fields for 'Hardhats are being worn' checkpoint with following data on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    And I click on 'Save' button on inspection page
    And I refresh page
    Then I should see following data in 'Hardhats are being worn' checkpoint on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    Then I should see following data in 'Safety vests are being worn' checkpoint on inspection page:
      | fieldName               | value                    |
      | Enter Observation       | observation              |
      | Corrective Action Notes | actionNote               |
      | Ready for review        | true                     |
      | Responsible Party       | pre-setup test rp        |
      | Location                | pre-setup Location 22222 |
      | Due Date                | first day of next month  |
      | Risk Factor             | 5                        |
    When I click on 'Delete' button on inspection page
    And I click on 'Yes' button on delete modal


  Scenario: Hidden character in inspection
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Location' field with 'test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test ' text on inspection page
    And I fill 'Notes' field with 'test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test ' text on inspection page
   # And I fill 'Hardhats are being worn' checkpoint with 'test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test ' text on inspection page
    And I click on 'OPEN' checkbox in 'Safety vests are being worn' checkpoint on inspection page
#    And I fill following fields for 'Safety vests are being worn' checkpoint with following data on inspection page:
#      | fieldName               | value                                                                       |
#      | Enter Observation       | test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test  |
#      | Corrective Action Notes | test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test  |
#      | Risk Factor             | test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test  |
    And I click on 'dynamic matrix' button on inspection page
    And I fill Dynamic Matrix fields on inspection page:
      | fieldNumber | value                                                                     |
      | 1           | test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test |
      | 2           | test000#001#test002#test003#test004#test005#test006# 007# 008# 011 test |
    Then I should wait until 'Synced to server' notification appears on inspection page
    And I should wait until Inspection ID value appears on inspection page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page


  Scenario: Check sync of existing not synced Inspections
    Given I login as admin user
    Then I should wait until 'Loading data' modal will be closed
    When I click on 'Signal icon' top menu item
    And I select 'Offline' item in appeared top menu dropdown
    Then I 'should' see 'Offline' toast message
    When I select 'pre-setup (Undefined)' division on create inspection page
    And I select 'pre-setup Clean Project' project on create inspection page
    And I select 'pre-setup Buiding One' phase on create inspection page
    And I select 'pre-setup Location323' location on create inspection page
    And I select 'pre-setup Concrete-Checkpoint Test' inspection on create inspection page
    And I select 'pre-setup Adams Fuel' responsibility party on create inspection page
    And I fill 'Notes' field with 'note' text on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Saved to device' text instead of save button on inspection page
    When I click on 'Signal icon' top menu item
    And I select 'Online' item in appeared top menu dropdown
    Then I should see 'Synced to server' text instead of save button on inspection page
    And I should wait until Inspection ID value appears on inspection page
    And I refresh page
    When I click on 'Inspections' top menu item
    And I select 'View Recent Inspections' item in appeared top menu dropdown
    And I enter 'inspection id' in the search on inspections page
    Then I 'should' see recently created inspection on view recent inspections page
    When I open searched item on view recent inspections page
    Then I should see Notes field filled with 'note' text on inspection page