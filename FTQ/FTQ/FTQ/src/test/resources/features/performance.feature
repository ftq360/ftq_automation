#@PERFORMANCE
#Feature: Performance
#
#  Scenario: Performance scenario
#    Given I login as performance user
#    Then I should wait until 'Loading data' modal will be closed
#    When I click on 'Inspections' top menu item
#    And I select 'All Inspections List (Archive)' item in appeared top menu dropdown
#    Then I should see inspection ids are loaded
#    When I click on 'Inspections' top menu item
#    And I select 'All Deficiencies List (Archive)' item in appeared top menu dropdown
#    Then I should see deficiency ids are loaded
#    When I click on 'Reports' top menu item
#    And I select 'Run Reports Online' item in appeared top menu dropdown
#    Then I should see favorite reports are loaded
#    When I click on 'Setup' top menu item
#    And I select 'Checklist Form Setup Process' item in appeared top menu dropdown
#    Then I should see checklists are loaded
#    When I click on 'Setup' top menu item
#    And I select 'Project Setup Process' item in appeared top menu dropdown
#    Then I should see projects are loaded
#    When I click on 'Setup' top menu item
#    And I select 'Responsible Party Setup Process' item in appeared top menu dropdown
#    Then I should see rps are loaded
#    When I click on 'Setup' top menu item
#    And I select 'Administration' dropdown in appeared top menu dropdown
#    And I select 'User Setup Process' item in appeared top menu dropdown
#    Then I should see users are loaded
#    When I click on 'Inspections' top menu item
#    And I select 'View Recent Inspections' item in appeared top menu dropdown
#    And I enter 'pre-setup 1525539' in the search on inspections page
#    And I open searched item on view recent inspections page
#    And I fill 'Notes' field with 'performance' text on inspection page
#    And I click on 'OPN' checkbox in '123' checkpoint on inspection page
#    And I click on 'Save' button on inspection page
#    Then I should see 'Synced to server' text instead of save button on inspection page
#    When I fill 'Notes' field with 'performance2' text on inspection page
#    And I click on 'NA' checkbox in '123' checkpoint on inspection page
#    And I click on 'Save' button on inspection page
#    Then I should see 'Synced to server' text instead of save button on inspection page